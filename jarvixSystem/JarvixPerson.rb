require 'rubygems'
require 'net/http'		#for http connections
require 'yaml'
require 'active_record'
require 'date'
require '/usr/src/jarvixServer/jarvixSystem/jarvix_salute.rb'
require '/usr/src/jarvixServer/jarvixSystem/JarvixPersonality.rb'
require '/usr/src/jarvixServer/jarvixSystem/Jarvix_Abstract_Object.rb'
require '/usr/src/jarvixServer/jarvixSystem/Jarvix_Custom_Object.rb'
require '/usr/src/jarvixServer/jarvixSystem/Jarvix_Custom_Property.rb'
require '/usr/src/jarvixServer/db/model/sys_user.rb'
require '/usr/src/jarvixServer/db/model/sys_user_phone.rb'
require '/usr/src/jarvixServer/jarvixSystem/IQiwo.rb'


class JarvixPerson
	attr_accessor:ontosData
	attr_accessor:ontosDataByPhone
	attr_accessor:currentPersonalPronoun
	attr_accessor:isThird
	attr_accessor:ontosEquals
	attr_accessor:ontosEqualsArray	
	attr_accessor:qiwo

	def initialize(ontosNameOrPhoneNumber,loadOntosRelated=true)
		# db_config = YAML::load( File.open("#{PROJECT_HOME}/config/database.yml"))
		# ActiveRecord::Base.establish_connection( db_config["JarvixBrain"])
		# #deactivate logger on DEBUG
		# ActiveRecord::Base.logger.level = 1
		#se comenta la siguiente linea debido a que al iniciar es muy probable que iniciemos la conversacion usando
		#el numero de telefono
		#
		@ontosEquals=[]
		@isThird=false
		@qiwo=IQiwo.new
		if is_numeric(ontosNameOrPhoneNumber) == true
			#compare length if the length is 10 is a phoneNumber, else a user id
			if ontosNameOrPhoneNumber.to_s().length>5
				loadByPhoneNumber(ontosNameOrPhoneNumber)
			else
				loadPersonalityByID(ontosNameOrPhoneNumber)
			end
		else
			loadPersonality(ontosNameOrPhoneNumber)	
		end
		#una vez cargada la personalidad principal hay que buscar todas las personalidades que se relacionan con esta
		@ontosEqualsArray=[]
		@ontosEqualsArray.push(@ontosData.id)
		if loadOntosRelated==true
			loadOntosEquals=Jarvix_Custom_Property.where(:property_name=>"__equals__",:property_value=>@ontosData.id)
			loadOntosEquals.each do |singleEquals|
				@ontosEqualsArray.push(singleEquals.id_user)
				@ontosEquals.push(JarvixPerson.new(singleEquals.id_user,false))
			end
			puts loadOntosEquals.inspect
		end
						
	end

	def loadPersonalityByID(tID)
		# puts "----loading by personalityID----"
		#@ontosData=Jarvix_Personality.find_by_id(tID)
		#ontosDataByID=Jarvix_Personality.find_by_sql("select * from jarvix_personalities inner join jarvix_custom_properties on jarvix_custom_properties.id_user = jarvix_personalities.id where jarvix_personalities.id='#{tID}'")
		ontosDataByID=Jarvix_Personality.find_by_id(tID)
		@ontosData=ontosDataByID
		
		if ontosDataByID==nil
			#to do crear usuario
			puts @ontosData.inspect
			
		end
		@ontosData.save
		# @ontosData.last_login=Time.now.to_s(:db)
		#puts @ontosData.to_json
		# if @ontosData.sex=="m"
		# 	@ontosData.my_noun="he"
		# elsif @ontosData.sex=="f"
		# 	@ontosData.my_noun="she"
		# end
		@currentPersonalPronoun=@ontosData.my_noun
		# puts "----end by personalityID----"
		#if it is being loaded by id, it can be a third person, in which case, lets change the sex/personal pronoun

	end
	def loadPersonality(ontosName)
		puts "----loading by personality----"
		@ontosData=Jarvix_Personality.find_by_my_name(ontosName)
		#puts @ontosData.to_json
		# @ontosData.last_login=Time.now.to_s(:db)
		# @ontosData.save
		@currentPersonalPronoun=@ontosData.my_noun
		puts "----end by personality----"
	end
	def loadByPhoneNumber(pnumber)
		puts "----loading by phoneNumber----"
		if pnumber!=1234567890
			puts "_JP1_"
			@ontosDataByPhone=Jarvix_Personality.find_by_sql("select * from jarvix_personalities inner join jarvix_custom_properties on jarvix_custom_properties.id_user = jarvix_personalities.id where jarvix_custom_properties.property_name = 'number' and jarvix_custom_properties.property_value = '#{pnumber}'")
		end
		#recuperamos el id del usuario y cargamos el usuario en base a su id

		
		if @ontosDataByPhone == nil or @ontosDataByPhone.length==0
			#crear usuario
			puts "_JP1.1_"
			createGenericUser(pnumber)
			return
		else
			puts "_JP1.2_"
			@ontosDataByPhone=Jarvix_Personality.find_by_id(@ontosDataByPhone[0].id_user.to_i)
		end
		# if @ontosDataByPhone.length==0
		# 	#crear usuario
		# 	createGenericUser(pnumber)
		# 	return
		# end
		
		# @ontosDataByPhone.last_login=Time.now.to_s(:db)
		
		# @ontosDataByPhone.save
		@ontosData=@ontosDataByPhone
		#el resultado fes un array, hay que verificar que no traiga más de un usuario usando el mismo numero de telefono
		puts "----end by phoneNumber----"
		@currentPersonalPronoun=@ontosData.my_noun
	end
	def thirdPersonalPronoun
		if @ontosData.sex=="m"
			return "he"
		elsif @ontosData.sex=="f"
			return "she"
		end
	end
	def createGenericUser(pNumber)
		#debemos ir por el nombre real del usuario, para eso, necesitamos ir a lineAccess y regresar un sys_user
		genericUser=Jarvix_Personality.new(:my_name=>"Strange",:my_noun=>"I")
		genericUser.save
		
		#se creo el usuario generico para esta sesion con telefono desconocido
		#ahora se crea el campo de propiedad del telefono
		genericPhone=Jarvix_Custom_Object.new(:id_abstract_object=>1,:is_active=>1,:id_owner=>genericUser.id)
		genericPhone.save
		#ahora se guarda el numero, el ID 2 de abstract_object es email, el 1 es phone
		genericPhoneNumber=Jarvix_Custom_Property.new(:property_name=>"number",:property_value=>pNumber,:id_user=>genericUser.id,:id_custom_object=>genericPhone.id)
		genericPhoneNumber.save
		accessLineUser=jarvixToLineAccessUser(genericUser.id)
		if accessLineUser!=nil
			#esta personalidad no se creo con jarvix
			genericUser.my_name=accessLineUser.name.downcase
			genericUser.save
			#ahora guardar el mail, asi ya tenemos mas datos
			genericEmail=Jarvix_Custom_Object.new(:id_abstract_object=>2,:is_active=>1,:id_owner=>genericUser.id)
			genericEmail.save
			#se usa extra por que no se especifica una propiedad especifica del objedsdsdto.
			genericEmailAddress=Jarvix_Custom_Property.new(:property_name=>"extra",:property_value=>accessLineUser.email,:id_user=>genericUser.id,:id_custom_object=>genericEmail.id)
			genericEmailAddress.save
		end
		#ahora cargar usuario
		@ontosDataByPhone=Jarvix_Personality.find_by_sql("select * from jarvix_personalities inner join jarvix_custom_properties on jarvix_custom_properties.id_user = jarvix_personalities.id where jarvix_custom_properties.property_name = 'number' and jarvix_custom_properties.property_value = '#{pNumber}'")
		# puts "_JP_2"
		# puts @ontosDataByPhone.inspect
		# puts "_JP_2.1"
		loadPersonalityByID(genericUser.id)
	end
	def createGenericUserByName(userName)
		genericUser=Jarvix_Personality.new(:my_name=>userName,:my_noun=>"I")
		genericUser.save
		#se creo el usuario generico para esta sesion con telefono desconocido
		#ahora se crea el campo de propiedad del telefono
		genericPhone=Jarvix_Custom_Object.new(:id_abstract_object=>1,:is_active=>1,:id_owner=>genericUser.id)
		genericPhone.save
		#ahora se guarda el numero
		genericPhoneNumber=Jarvix_Custom_Property.new(:property_name=>"number",:property_value=>1234567890,:id_user=>genericUser.id,:id_custom_object=>genericPhone.id)
		genericPhoneNumber.save
		loadPersonalityByID(genericUser.id)
	end
	def status(requestType,allRequests)
		#getting status 
		#puts "getting STATUS"
		arrayAllRequests=allRequests.split(",")
		arrayAllRequests.each do |incomingData|
			if incomingData==nil
				next
			end
			#dataSplited=String.new(incomingData).split(">")
		
			if requestType=="GETTER"
				wordToSave=Jarvix_Word.find_by_word(allRequests)
				if wordToSave.word_type!=6
					#es una descripcion
					return "STATUS WITH OBJECT!"
				end
				if allRequests==@ontosData.my_status
					return "yes"
				else
					return "no"
				end
			else
				#ver la palabra y ver si es una igualacion o es un status
				wordToSave=Jarvix_Word.find_by_word(allRequests)

				statusToSave=allRequests
				if allRequests[allRequests.length-1]==">"
					puts "deleting symbols"
					statusToSave=allRequests[0..allRequests.length-2]
				end
				if wordToSave.word_type!=6
					puts "__equals__"
					personalDirectProperty=Jarvix_Custom_Property.new
					# if allRequests.index("|")!=nil
					# 	#dividir propiedad
					# 	extractedObject=allRequests.split("|")
					# 	personalDirectProperty.property_name=extractedObject[0]
					# 	personalDirectProperty.property_value=extractedObject[1]
					# 	personalDirectProperty.is_enabled=1
					# 	personalDirectProperty.id_user=@ontosData.id
					# 	personalDirectProperty.id_custom_object=-1
					# 	personalDirectProperty.save
					# end
				else
					@ontosData.my_status=statusToSave
					@ontosData.save
				end
				return "ok"
			end
		end
		puts "final data"
		return @ontosData.my_status	
		
	end
	def startDate(requestType,allRequests)
		#puts "executing startDate"
		return "TODAY"
	end
	def description(requestType,params)
		puts "ODESC"
		if requestType=="SETTER"
			puts "ODESC1"
			if params.index("|")!=nil
				puts "ODESC2"
				#dividir propiedad
				personalDirectProperty=Jarvix_Custom_Property.new
				extractedObject=params.split("|")
				personalDirectProperty.property_name=extractedObject[0]
				personalDirectProperty.property_value=extractedObject[1]
				personalDirectProperty.is_enabled=1
				personalDirectProperty.id_user=@ontosData.id
				# personalDirectProperty.id_custom_object=133
				personalDirectProperty.save
				return "ok"
			end
		elsif requestType=="GETTER"
			puts "ODESC3"
			if params.index("|")!=nil
				puts "ODESC4"
				#dividir propiedad
				extractedObject=params.split("|")
				personalDirectProperty=searchPropertyInOtherPersonalities(extractedObject[0])#Jarvix_Custom_Property.where(:property_name=>extractedObject[0],:id_user=>@ontosData.id,:id_custom_object=>nil).last
				puts personalDirectProperty.inspect
				if personalDirectProperty!=nil
					puts "ODESC5"
					extractedObject=params.split("|")
					if personalDirectProperty.property_value==extractedObject[1]
						return "yes"
					else
						return "no"
					end
					# personalDirectProperty.property_name=extractedObject[0]
					# personalDirectProperty.property_value=extractedObject[1]
					# personalDirectProperty.is_enabled=1
					# personalDirectProperty.id_user=@ontosData.id
					# personalDirectProperty.id_custom_object=-1
					# personalDirectProperty.save	
				else
					return "no"
				end
			else
				personalDirectProperty=searchPropertyInOtherPersonalities(params)#Jarvix_Custom_Property.where(:property_name=>params,:id_user=>@ontosData.id,:id_custom_object=>nil).last
				puts personalDirectProperty
				puts "testing description"
				#en muchos casos la descripcion es extra, es decir es la descripcion de un objeto, entonces buscar extra
				personalExtra=getObjectProperty(params)
				if personalExtra!=nil
					return personalExtra
				end
				puts "testing description 2"
				#ahora buscar
				objectDirectProperty=Jarvix_Custom_Property.where(:property_name=>params,:id_user=>@ontosEqualsArray,:id_custom_object=>nil).last
				puts "testing description 3"
				if personalDirectProperty!=nil
					return personalDirectProperty.property_value
				else
					return "i do not have that answer"
				end
			end
		end

	end
	def getObjectProperty(objecto)
		#buscar primero el objeto
		objectDirectObject=Jarvix_Abstract_Object.find_by_object_name(objecto)
		if objectDirectObject!=nil
			#ahora buscar el custom del objeto de todas las personalidades igualadas
			objectDirectCustomObject=Jarvix_Custom_Object.where(:id_abstract_object=>objectDirectObject.id,:id_owner=>@ontosEqualsArray)

			#
			if objectDirectCustomObject.length>0
				#encontramos el objeto, ahora determinar si tiene propiedades extra
				#las propiedades extra tienen mas prioridad sobre las propiedades unicas
				objectExtraDirectProperty=Jarvix_Custom_Property.where(:property_name=>"extra",:id_custom_object=>getIDArrayOfObjects(objectDirectCustomObject),:id_user=>@ontosEqualsArray).last
				if objectExtraDirectProperty!=nil
					return objectExtraDirectProperty.property_value
				else
					#no hay propiedad extra, entonces ver si el objeto tiene 1 sola propiedad
					#si tiene una sola propiedad entonces regresar esa
					objectNonExtraDirectProperty=Jarvix_Custom_Property.where(:id_custom_object=>getIDArrayOfObjects(objectDirectCustomObject),:id_user=>@ontosEqualsArray)
					if objectNonExtraDirectProperty.length==1
						reg=objectNonExtraDirectProperty.last.property_value.to_s
						return reg
					end
					#en caso de que no pues notificamos que no existe propiedad
					return nil
				end
			end
			return nil
		end
		return nil
	end
	def getIDArrayOfObjects(activeRecordResults)
		resultArray=[]
		activeRecordResults.each do |singleActiveRecordResult|
			resultArray.push(singleActiveRecordResult.id)
		end
		return resultArray
	end
	def posessions(requestType,allRequests)
		puts "getting into Posessions"
		objectsSeparated=[]
		answerToReturn="ok"
		 if String.new(allRequests).index(",")!=nil
		 	objectsSeparated=String.new(allRequests).split(",")
		 else
		 	objectsSeparated.push(String.new(allRequests).strip)
		 	#puts objectsSeparated.length
		 	#puts objectsSeparated
		 end
		#if requestType=="SETTER"
			#separar todos los objetos
		 extractedObject=nil
		 extractedProperty=nil
		 puts "from:posessions:"+objectsSeparated.inspect
		 objectsSeparated.each do |singleObjectSeparated|
		 	#buscar el objeto
		 	if singleObjectSeparated==nil
		 		next
		 	end
		 	if singleObjectSeparated.index(">")==nil
		 		#es solo un objeto aunque tambien puede ser por ejemplo el nombre o el mail
		 		#asi que buscamos el metodo aqui y tratamos de separar los parametros en caso de existir parametros
		 		extractedObject=singleObjectSeparated
 				if singleObjectSeparated.index("|")!=nil
 					extractedObject=singleObjectSeparated.split("|")[0]
 				end
		 		isInternalMethod=methodOrObject(requestType,singleObjectSeparated,extractedObject)
		 		if isInternalMethod!="NOT_METHOD"
		 			return isInternalMethod
		 		end
		 		puts "posessions::1"
		 		#searchedWords=assignObjects(extractedObject)
		 		#si llegamos a este punto es por que no es un objeto es una propiedad
		 		return description(requestType,allRequests)
		 		puts "posessions::1.1"

		 		next
		 	else

		 		#si entra aqui es altamente probable que sea un objeto
		 		currentPointedObject=singleObjectSeparated.split(">")[0]
		 		puts "posessions::2"
		 		currentAssignObjects=assignObjects(currentPointedObject)
		 		if singleObjectSeparated.split(">")[1]==nil
		 			puts "NO_PROPERTIES"
		 			next
		 		end
		 		currentAssignObjects.each do |singleCurrentObject|
		 			if singleCurrentObject==nil
		 				next
		 			end
		 		#si tiene propiedades entonces veamos si necesitamos extraer o setear
			 		propertyAndValue=singleObjectSeparated.split(">")[1].split("|")
			 		if requestType=="GETTER"
			 			#vamos y buscamos la propiedad de este objeto
			 			puts "BUSCANDO PROPIEDAD:"+propertyAndValue[0]
			 			#customProperty=Jarvix_Custom_Property.where("id_custom_object="+singleCurrentObject.id+" and id_")
			 			newArrayCustomProperty=Jarvix_Custom_Property.where(:id_custom_object=>singleCurrentObject.id.to_s(),:id_user=>@ontosEqualsArray,:property_name=>propertyAndValue[0]).last
			 			if newArrayCustomProperty!=nil
			 				#si existe y regresar valor
			 				#verificamos si el getter trae valor, en tal caso significa que nos estan preguntando si coincide
			 				if propertyAndValue[1]!=nil
			 					if newArrayCustomProperty.property_value==propertyAndValue[1]
			 						puts "YES!, both values are EQUAL"
			 						answerToReturn="yes. It is"
			 					else
			 						puts "NO, the value is different"
			 						answerToReturn="No. It is not"
			 					end
			 				else
			 					#regresar el valor default de la propiedad
			 					puts "the answer is:"+newArrayCustomProperty.property_value
                                #prevenir dar el numero de telefono fake que usamos para crear nuevos usuarios
                                if newArrayCustomProperty.property_value!="1234567890"
			 					   answerToReturn=newArrayCustomProperty.property_value
                                end
			 				end
			 			else
			 				#no existe regresar un error
			 				puts "I do not have that answer<<<<<<<<<<<"
			 				answerToReturn="I do not have that answer. Sorry"
			 			end
			 			#en caso de tener valor, lo comparamos y regresamos un afirmativo
			 		elsif requestType=="SETTER"
			 			puts "GRABANDO PROPIEDAD"
			 			#primero buscamos si ya existe, en caso de existir modificamos
			 			newCustomProperty=nil
			 			#antes de prosceder con la busqueda hay que identificar si el usuario esta pidiendo la respuesta anterior que sea enviada a su mail
			 			#o algun otro metodo de guardado por lo tanto necesitamos buscar si trae el -newIntention- 
			 			puts "metodo de guardado:"+currentPointedObject
			 			puts "tipo de intencion:"+propertyAndValue[0]
			 			
			 			if propertyAndValue[0]!="newIntention"
			 				#si no es una -newIntention- entonces se esta guardando un objeto normal,
			 				if propertyAndValue[1]!=nil
		 						newCustomProperty=Jarvix_Custom_Property.new(:id_custom_object=>singleCurrentObject.id,:id_user=>@ontosData.id,:property_name=>propertyAndValue[0],:property_value=>propertyAndValue[1],:is_enabled=>1)
		 					else
		 						newCustomProperty=Jarvix_Custom_Property.new(:id_custom_object=>singleCurrentObject.id,:id_user=>@ontosData.id,:property_name=>"extra",:property_value=>propertyAndValue[0],:is_enabled=>1)
		 					end
		 					newCustomProperty.save
			 			else
			 				#tenemos que alguien quiere ejecutar una intencion sobre algo que se dijo, por lo que
			 				#tenemos que ver como ejecutarla, hasta este punto es para enviar cosas que dijo el sistema
			 				#a una ubicacion diferente, por ejemplo correo, twitter, facebook y evernote
			 				return "SENT_TO_CONTAINER"
			 			end
		 				
			 		end
		 		end
		 	end
		 end
		
		#recibimos las posesiones para modificar el objeto
		# return "POSESSION RECEIVED>>"+allRequests.strip
		return answerToReturn
	end
	def assignObjects(extractedObject)
		wordExists=Jarvix_Word.find_all_by_word(extractedObject)
 		abstractObjectExists=Jarvix_Abstract_Object.find_all_by_object_name(extractedObject)
 		newWord=nil
 		newAbstract=nil
 		newObjectRelated=nil
 		if wordExists.length<1
 			newWord=Jarvix_Word.new(:word=>extractedObject,:is_question=>0,:is_sentence=>"X",:result_is=>"X:"+extractedObject,:word_type=>13,:is_breakline=>0,:is_method=>0)
 			newWord.save
 			#se creo el registro nuevo, ahora a crear el objeto abstracto con la propiedad y setear la propiedad customizada en el usuario
 		end
 		if abstractObjectExists.length<1
 			newAbstract=Jarvix_Abstract_Object.new(:object_name=>extractedObject)
 			newAbstract.save
 		end
 		newAbstract=Jarvix_Abstract_Object.find_by_object_name(extractedObject)
 		#aqui se necesita algo de inteligencia
 		#si por ejemplo digo "i have a car" tengo un carro
 		#buscamos todos los carros del usuario y los regresamos
 		#eventualmente se filtraran por las mismas propiedades
 		puts "starting search"
 		puts @ontosData.to_json
 		wholeUserList=Jarvix_Custom_Object.where(:id_abstract_object=>newAbstract.id,:id_owner=>@ontosEqualsArray)
 		if wholeUserList.length==0
 			puts "creating object"
 			#entonces si creamos el objeto por que no lo encontramos
 			newObjectRelated=Jarvix_Custom_Object.new(:id_abstract_object=>newAbstract.id,:id_owner=>@ontosData.id)
			newObjectRelated.save
			wholeUserList.push(newObjectRelated)
 		end
 		puts "returning objects"
 		puts wholeUserList.to_json
		return wholeUserList
	end
	def microPosessionUpdate(objeto)
		
	end
	def methodOrObject(requestType,singleObjectSeparated,extractedObject)
		
 		puts "testing method:"+extractedObject
 		insertionParameters=singleObjectSeparated.split("|")[1]
		if insertionParameters==nil
			insertionParameters=singleObjectSeparated.split("|")[0]
		end
		puts "testing method1:"+extractedObject
 		if JarvixPerson.method_defined?(extractedObject)==true
 			puts "methodOrObject::extractedObject"
 			
 			#hay dudas aqui, por que comparar vs tipo 6????
 			#al parecer pueden pasar tanto descripciones (cualquier palabra o adjetivo) como metodos reales
 			#probar mejor con metodos que si existan en esta clase
 			puts extractedObject+'("'+requestType+'","'+insertionParameters+'")'	
 			return eval extractedObject+'("'+requestType+'","'+insertionParameters+'")'
 		end
 		puts "testing method2:"+extractedObject
		wordToTest=Jarvix_Word.find_by_word(insertionParameters)
		if wordToTest.word_type!=6
			puts 'JP::description("'+requestType+'","'+insertionParameters+'")'	
			return eval 'description("'+requestType+'","'+insertionParameters+'")'
		end
 			

 			#return self[extractedObject](requestType,singleObjectSeparated.split("|")[1])
 		return "NOT_METHOD"
	end
	def location(requestType,allRequests)
		#verificar si tenemos verbos, en caso de que sí, entonces es una busqueda
		semillasDeBusqueda=allRequests.split("|")
		finalSemillasDeBusqueda=[]
		isQiwoSearch=false
		semillasDeBusqueda.each do | singleSemillaDeBusqueda|
			palabra=Jarvix_Word.find_by_word(singleSemillaDeBusqueda)
			#estamos buscando palabras que no sean verbos (3), por ejemplo comprar,comer,vender esto permite aislar resultados
			#en lugar de buscar "comer ensalada" buscamos solo ensalada
			if palabra.word_type!=3
				finalSemillasDeBusqueda.push(palabra.es_word)
				isQiwoSearch=true
			end
		end
		#finalmente pasamos la busqueda
		if isQiwoSearch==true
			return @qiwo.performSearch(finalSemillasDeBusqueda.join(" "))
		end
		if requestType=="GETTER"
			#verificar si esta vacio allRequests entonces pregunta donde esta la persona
			if allRequests==""
				#buscar ultima ubicacion
				#el usuario quiere saber la última ibicación
				lastLocation=Jarvix_Custom_Property.where(:property_name=>"__location__",:id_user=>@ontosEqualsArray).last
				return lastLocation.property_value
			else
				#comparar ultima ubicacion
				lastLocation=Jarvix_Custom_Property.where(:property_name=>"__location__",:id_user=>@ontosEqualsArray).last
				if lastLocation==nil
					return "actually, I do not know the location."
				end
				if lastLocation.property_value.index(allRequests.downcase) or allRequests.downcase.index(lastLocation.property_value)
					return "yes, in that location"
				else
					return "no, it looks it is in another location"
				end
			end
		else
			#si es setter entonces esta diciendo donde esta
			Jarvix_Custom_Property.new(:property_name=>"__location__",:property_value=>allRequests.downcase,:id_user=>@ontosData.id).save
			return "OK"
		end
		return "YOU ARE SOMEWHERE"
	end
	def idPersonality
		if @ontosData.attributes.has_key? :id_user
			return @ontosData.id_user
		else
			return @ontosData.id
		end
	end
	def name(requestType,allRequests)
		if requestType == "GETTER"
			#hay que verificar si este usuario tiene igualaciones, de ser asi se busca la
			#que tenga la propiedad más reciente, en el caso del nombre que no sea "strange"
			if @ontosData.my_name!="Strange"
				return @ontosData.my_name
			else
				#buscar en las otras personalidades
				return selectNewestPersonality().ontosData.my_name
			end
		elsif requestType =="SETTER"
			ontosDataTmp=Jarvix_Personality.find_by_id(@ontosData.id)
			ontosDataTmp.my_name=allRequests
			ontosDataTmp.save
			newName=Jarvix_Word.find_by_word(allRequests.downcase)
			if newName==nil
				newName=Jarvix_Word.new
				newName.word=allRequests.downcase
				newName.word_type=16
				newName.is_question="X"
				newName.result_is="X:"+allRequests.downcase
				newName.is_breakline=0
				newName.is_method=0
				newName.save
			end
		end
		return "GIVING NAME"
	end
	def relative(requestType,allRequests)
		return "GIVING RELATIVE"
	end

	def extractPersonality(textLine)
		if textLine!=nil and textLine!=""
			if textLine.index("con")!=nil or textLine.index("with")!=nil

			end
		end
	end

	#este metodo sirve para saber si la variable es numero, de forma que sepamos cuando buscar telefono o por nombre
	def is_numeric(obj) 
   		obj.to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/) == nil ? false : true
	end

	# def newIntention(requestType,allRequests,peopleInvolved)
	# 	if requestType=="SETTER"

	# 	end
	# 	return "No application could be executed",
	# end

	def selectNewestPersonality
		currentSelectedPersonality=self
		pointDate=DateTime.new(2000,11,20)
		@ontosEquals.each do |singleOntos|
			#
			puts "CP_S"
			puts pointDate.inspect+"==="+singleOntos.ontosData.updated_at.inspect
			puts "CP_E"
			if pointDate<singleOntos.ontosData.updated_at
				currentSelectedPersonality=singleOntos
				pointDate=singleOntos.ontosData.updated_at
			end
		end
		return currentSelectedPersonality
	end
	def searchPropertyInOtherPersonalities(propertyName,idCustomObject=nil)
		#construir consulta
		
		
		puts "siop1 propertyName:"+propertyName+"  id_user:"+@ontosEqualsArray.join(",")+"   id_custom_object:"+idCustomObject.inspect
		if idCustomObject==nil
			return Jarvix_Custom_Property.where(:property_name=>propertyName,:id_user=>@ontosEqualsArray,:id_custom_object=>nil).last
		elsif idCustomObject=="*"
			return Jarvix_Custom_Property.where(:property_name=>propertyName,:id_user=>@ontosEqualsArray).last
		else
			return Jarvix_Custom_Property.where(:property_name=>propertyName,:id_user=>@ontosEqualsArray,:id_custom_object=>idCustomObject).last
		end
		
	end
	def jarvixToLineAccessUser(userID)
		#con el telefono podemos buscar el usuario.
		lineAccessPhone=userPhone(userID)
		if lineAccessPhone==nil
			#no se encontro el usuario en lineAccess, es probable que sea una personalidad
			#creada por jarvix
			return nil
		end
		lineAccessUser=Sys_User.find_by_id(lineAccessPhone.sys_user)
	end

	def userPhone(userID)
		person=JarvixPerson.new(userID)
		#buscar telefono
		personPhone=Jarvix_Custom_Object.where(:id_abstract_object=>1,:id_owner=>person.ontosData.id).last
		realPhone=Jarvix_Custom_Property.where(:property_name=>"number",:id_user=>person.ontosData.id,:id_custom_object=>personPhone).last
		#con el telefono podemos buscar el usuario
		lineAccessPhone=Sys_User_Phone.where(:number=>realPhone.property_value,:sys_phone_type=>1).last
	end
end