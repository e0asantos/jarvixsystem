require '/usr/src/jarvixServer/db/model/anet_device.rb'
require '/usr/src/jarvixServer/db/model/anet_command.rb'
require '/usr/src/jarvixServer/db/model/sys_user.rb'
require '/usr/src/jarvixServer/db/model/api_method.rb'
require '/usr/src/jarvixServer/db/model/api_interface.rb'
# require '/usr/src/jarvixServer/lib/google/JMail.rb'
require '/usr/src/jarvixServer/lib/twitter/JTwitter.rb'
require '/usr/src/jarvixServer/lib/facebook/JFacebook.rb'


class ILineAccess

	attr_accessor:currentApp
	attr_accessor:currentAppParams
	attr_accessor:currentAppMethod
	#esta variable es nil cuando no espera parametros, es -person cuando espera el ID de una personality
	#es -text cuando espera cualquier otro texto y es -option cuando espera que se escoja de una opcion
	attr_accessor:waitingParam
	#esta variable no deberia estar vacia al realizar un waitinParam=-option, esto permite escoger
	#una opcion de una lista
	attr_accessor:waitingSelectionList
	#esta variable almacena momentaneamente el numero de opcion que escogio el usuario
	attr_accessor:waitingSelectedIndex
	attr_accessor:waitingConfirmation
	attr_accessor:currentUserID

	def initialize(userID)
		@currentUserID=userID
		@waitingParam=nil
		@waitingConfirmation=false
		@currentAppParams=[]
		@waitingSelectedIndex=-1
	end

	#USO
	#cuando recien queremos ejecutar una app
	#runApp('email.send','NONE')
	#
	#cuando hizo falta un parametro
	#addNewParam('hola k ace')
	#runApp(instancia.currentApp.name,instancia.currentAppParams)
	def runApp(appName,appParams)
		appMethod=appName.split(".")
		#verifica si existe esa app, por ejemplo facebook.chat
		#en caso que exista NONE, significa que no se especifico la app
		if appMethod[0]=="NONE"
			#buscar el metodo y dar los posible metodos expuestos
			puts "ILA1"
			metodos=findAppMethod(appMethod[1])
			if metodos.length>0
				puts "ILA1.1"
				#si existen metodos pero no tenemos cual de todas esas interfaces es la que desea ejecutar
				return "there are a couple of applications with that "+appMethod[1]+" you should tell me the application"
			end
		else
			#parece que tenemos app y metodo, buscar primero app
			findApp(appMethod[0])
			#
			
			if @currentApp!=nil
				
				#ahora buscar si el metodo lo tiene la app
				if appContainsMethod(appMethod[0],appMethod[1])!=nil
					
					#existe el metodo, ahora revisar el numero de parametros
					@currentAppParams=appParams.split(",")
					
					realAppParams=@currentAppMethod.params.split(",")
					
					if appParams=="NONE"
						@currentAppParams=[]
					end
					puts "currentAppParams:"+@currentAppParams.inspect+"    realAppParams:"+realAppParams.inspect
					if @currentAppParams.length<realAppParams.length
						#faltan parametros, buscar nombre del parametro siguiente que falta
						@waitingParam=realAppParams[@currentAppParams.length]

						return realAppParams[@currentAppParams.length]
					else
						#quizas hay mas parametros pero solo tomamos los necesarios
						####aqui corre la app#####
						@waitingParam=nil
						puts "RUNNING APP!"
						#crear objeto
						mainAppEx=eval @currentApp.command
						if mainAppEx!=nil
							 
								mainAppEx.fromIdUser=@currentUserID
								if @waitingSelectedIndex!=-1
									mainAppEx.optionsList=@waitingSelectionList
									mainAppEx.waitingSelectedIndex=@waitingSelectedIndex
								end
								if @waitingParam=="-option"
									@waitingSelectionList=nil
									@waitingSelectedIndex=nil
									@waitingParam=nil
								end
								mainAppExResult=mainAppEx.send("_"+@currentAppMethod.name,*@currentAppParams)
								#verificar si espera opciones de una lista
								if mainAppExResult=="-option"
									@waitingParam="-option"
									@waitingSelectionList=mainAppEx.optionsList
								end
								return mainAppExResult	
							 
							
						end
						return "that application is not installed yet"
					end
				else
					return "sorry i cannot use that command; it is not installed yet"
				end
			else
                puts "appName:"+appName.inspect+"    appParams:"+appParams.inspect
                #si llegamos aqui es por que no tenemos aplicaciones con el nombre requerido, asi que hay que verificar->
                #si esta queriendo llamar un arduino/raspberry, para eso necesitamos buscar un ".", el punto permite conocer->
                #si la peticion tiene verbo, en caso de tener verbo entonces es muy probable que sea un comando de arduino
                
                if appName.index(".")!=nil
                    #probablemente si es un comando de arduino
                    #buscar el arduino
                    arduinoDevice=appName.split(".")[0].split(";")
                    commandDevice=appName.split(".")[1].split(";").join(" ").downcase
                    if arduinoDevice.length==1
                    	#si no tenemos mas que una posicion en el dispositivo, significa que entonces
                    	#no tenemos ya sea la ubicacion o el nombre del objeto, y no es posible ejecutar
                    	#sin alguno de los dos
                    	return "NOT_PROCESSED"
                    end
                    #un pequeño fix si se encuentra -turn- solo quizas era -turn on-
                    puts "arduinoDevice:"+arduinoDevice.inspect+"   commandDevice:"+commandDevice.inspect
                    if commandDevice=='turn'
                       #agregar -on-
                        commandDevice='turn on'
                    end
                    lineAccessUser=jarvixToLineAccessUser(@currentUserID)
                    anetDevice=AnetDevice.where("location like '%"+arduinoDevice[0]+"%' and object like '%"+arduinoDevice[1]+"%' and id_owner like '%"+lineAccessUser.id.to_s+"%'").last
                    #quizas esta al reves la ubicacion
                    if anetDevice==nil
#                        anetDevice=AnetDevice.where(:location=>arduinoDevice[1],:object=>arduinoDevice[0],:id_owner=>lineAccessUser.id).last
                        anetDevice=AnetDevice.where("location like '%"+arduinoDevice[1]+"%' and object like '%"+arduinoDevice[0]+"%' and id_owner like '%"+lineAccessUser.id.to_s+"%'").last
                    end
                    puts anetDevice.inspect
                    if anetDevice!=nil
                        #probar si tiene el metodo
                        anetDeviceCommand=AnetCommand.where(:arduino_id=>anetDevice.id,:command=>commandDevice).last
                        puts anetDeviceCommand.inspect
                        if anetDeviceCommand!=nil
                           #encontramos un comando y dispositivo tratar de enviar el comando
                             s = TCPSocket.new 'localhost', 3000
                            while line = s.gets # Read lines from socket
                                puts line.inspect
                                if line.index("TOKEN?")!=nil
                                    puts "enviando"
                                   s.puts "025621de/d822339f" 
                                elsif line.index("WLCME")!=nil
                                    puts "WLCME"
                                    s.puts "sendto "+anetDevice.token+" "+anetDeviceCommand.command_data
                                elsif line.index("OK")!=nil
                                    return "the instructions were succesfully executed on the device"
                                elsif line.index("NOT_ONLINE")!=nil
                                    return "the device is not currently connected to the cloud"
                                elsif line.index("CMD_ERROR")!=nil
                                    return "the command was not found on the list for the device"
                                end
                            end
                            s.close
                        else
                            return "the device could not recognize the command you said"
                        end
                    else 
                        return "no devices were found in that location with that name"        
                    end
                else
                    return "no application with that name was found"
                end
				
				waitingParam=nil
			end

		end
	end

	def findApp(appName)
		#puts "FA1:"+appName
		@currentApp=Api_Interface.find_by_name(appName)
		#puts @currentApp.inspect
		return @currentApp
	end

	def findAppMethod(method)
		metodos=Api_Method.find_all_by_name(method)
		return metodos
	end
	def listPossibleApps(method)

	end
	def appContainsMethod(appName,method)
		findApp(appName)
		metodos=findAppMethod(method)
		metodos.each do |singleMethod|
			if singleMethod.api_interface==@currentApp.id
				@currentAppMethod=singleMethod
				return singleMethod
			end
		end
		return nil
	end

	def newIntent(appName,appParams)
		
	end

	def addNewParam(param)
		if @currentAppParams==nil
			@currentAppParams=[]
		end
		@currentAppParams.push(param)
	end

	def classFactoryBuilder(appName)

	end
    def jarvixToLineAccessUser(userID)
		#con el telefono podemos buscar el usuario
		lineAccessPhone=userPhone(userID)
		lineAccessUser=Sys_User.find_by_id(lineAccessPhone.sys_user)
	end

	def userPhone(userID)
		person=JarvixPerson.new(userID)
		#buscar telefono
		personPhone=Jarvix_Custom_Object.where(:id_abstract_object=>1,:id_owner=>person.ontosEqualsArray)
		personPhoneIds=idFromObjects(personPhone)
		realPhone=Jarvix_Custom_Property.where(:property_name=>"number",:id_user=>person.ontosEqualsArray,:id_custom_object=>personPhoneIds).where("property_value!=1234567890").last
		#con el telefono podemos buscar el usuario
		lineAccessPhone=Sys_User_Phone.where(:number=>realPhone.property_value,:sys_phone_type=>1).last
		return lineAccessPhone
	end
    def idFromObjects(objetos)
		returnIds=[]
		objetos.each do |objetoid|
			returnIds.push(objetoid.id)
		end
		return returnIds
	end
end
