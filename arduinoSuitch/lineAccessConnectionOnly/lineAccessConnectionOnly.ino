/*
  Telnet client
 
 This sketch connects to a a telnet server (http://www.google.com)
 using an Arduino Wiznet Ethernet shield.  You'll need a telnet server 
 to test this with.
 Processing's ChatServer example (part of the network library) works well, 
 running on port 10002. It can be found as part of the examples
 in the Processing application, available at 
 http://processing.org/
 
 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13
 
 created 14 Sep 2010
 modified 9 Apr 2012
 by Tom Igoe
 
 */

#include <SPI.h>
#include <Ethernet.h>

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {  
  0xDB, 0x1B, 0xD5, 0x85, 0x4A, 0xE6 };

IPAddress ip(10,0,1,21);


int reset=0;
int waitingCharacter=0;
String incomingString;
char incomingCharacter;
int contadorA=0;
int contadorB=0;



// Enter the IP address of the server you're connecting to:
IPAddress server(209,177,156,249); 

// Initialize the Ethernet client library
// with the IP address and port of the server 
// that you want to connect to (port 23 is default for telnet;
// if you're using Processing's ChatServer, use  port 10002):
EthernetClient client;

void setup() {
  // start the Ethernet connection:
  Ethernet.begin(mac,ip);
  incomingString=String("X");
 // Open serial communications and wait for port to open:
  Serial.begin(9600);
   while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }


  // give the Ethernet shield a second to initialize:
  delay(1000);
  Serial.println("connectando...");

  // if you get a connection, report back via serial:
  if (client.connect(server, 3000)) {
    Serial.println("connected");
  } 
  else {
    // if you didn't get a connection to the server:
    Serial.println("connection failed");
  }
}

void loop()
{
  // if there are incoming bytes available 
  // from the server, read them and print them:
  if (client.available()) {
    incomingCharacter=client.read();
    Serial.print(incomingCharacter);
    if (incomingCharacter == '\n' || incomingCharacter== '\r') {
      if(incomingString=="XTOKEN?"){
       client.println("XXXXXXX/YYYYYYY");
       incomingString="X";
      } else if(incomingString=="XWLCME"){
        //quiere decir que entramos bien
        Serial.println("Connected to a-Network");
      }
    } else {
     incomingString=incomingString+incomingCharacter; 
    }
  }

  // as long as there are bytes in the serial queue,
  // read them and send them out the socket if it's open:
  while (Serial.available() > 0) {
    char inChar = Serial.read();
    if (client.connected()) {
      client.print(inChar); 
    }
  }

  // if the server's disconnected, stop the client:
  if (!client.connected()) {
    Serial.println();
    Serial.println("disconnectando.");
    client.stop();
    // do nothing:
    while(true);
  }
}




