//We always have to include the library
#include "LedControl.h"
#include <EEPROM.h>
/*
 Now we need a LedControl to work with.
 ***** These pin numbers will probably not work with your hardware *****
 pin 12 is connected to the DataIn 
 pin 11 is connected to the CLK 
 pin 10 is connected to LOAD 
 We have only a single MAX72XX.
 */
LedControl lc=LedControl(12,11,10,1);

/* we always wait a bit between updates of the display */
unsigned long delaytime=2000;
signed long isInResetTimeOut=0;//timeout = millis() + 30000;
int brillo=0;
//estas dos variables controlan el flujo de los mensajes cuando llegan por UART
String incomingString="X";
//este es el string que guarda el token para enviarlo junto
String tokenString="";
int askForStatus=0;
int loginStatus=0;
char incomingCharacter;
int eeAddress=0;
byte resetSymbol[8]={0x55,0xFE,0x7F,0xFE,0x7F,0xFE,0x7F,0xAA};
byte tickSymbol[8]={0x04,0x06,0x03,0x03,0x06,0x0C,0x18,0x30};
/*
esta variable controla el tipo de dispositivo que es, de forma que podemos crear los
tokens basandonos en el tipo para controlar las versiones
*/
int versionSync=0;
void setup() {
  /*
   The MAX72XX is in power-saving mode on startup,
   we have to do a wakeup call
   */
  delay(1000);
  //iniciamos comunicacion serial al baud-rate del wifi
  Serial.begin(115200);
  
  lc.shutdown(0,false);
  /* Set the brightness to a medium values */
  lc.setIntensity(0,1);
  /* and clear the display */
  lc.clearDisplay(0);
  isInResetTimeOut=millis() + 26000;
}
int airSignalStep=0;
signed long airSignalStepTimer=0;//timeout = millis() + 30000;
void airSignal(){
  byte fase1[8]={0x00,0x00,0x00,0x03,0x03,0x00,0x00,0x00};
  byte fase2[8]={0x00,0x04,0x08,0x0B,0x0B,0x08,0x04,0x00};
  byte fase3[8]={0x10,0x24,0x48,0x4B,0x4B,0x48,0x24,0x10};
  if(airSignalStepTimer<millis()){
    airSignalStepTimer=millis()+300;
      if(airSignalStep==0){
        lc.setRow(0,0,fase1[7]);
        lc.setRow(0,1,fase1[6]);
        lc.setRow(0,2,fase1[5]);
        lc.setRow(0,3,fase1[4]);
        lc.setRow(0,4,fase1[3]);
        lc.setRow(0,5,fase1[2]);
        lc.setRow(0,6,fase1[1]);
        lc.setRow(0,7,fase1[0]);
        airSignalStep=1;
      } else if(airSignalStep==1){
        lc.setRow(0,0,fase2[7]);
        lc.setRow(0,1,fase2[6]);
        lc.setRow(0,2,fase2[5]);
        lc.setRow(0,3,fase2[4]);
        lc.setRow(0,4,fase2[3]);
        lc.setRow(0,5,fase2[2]);
        lc.setRow(0,6,fase2[1]);
        lc.setRow(0,7,fase2[0]);
        airSignalStep=2;
      } else if(airSignalStep==2){
        lc.setRow(0,0,fase3[7]);
        lc.setRow(0,1,fase3[6]);
        lc.setRow(0,2,fase3[5]);
        lc.setRow(0,3,fase3[4]);
        lc.setRow(0,4,fase3[3]);
        lc.setRow(0,5,fase3[2]);
        lc.setRow(0,6,fase3[1]);
        lc.setRow(0,7,fase3[0]);
        airSignalStep=3;
      } else if(airSignalStep==3){
        lc.setRow(0,0,fase3[7]);
        lc.setRow(0,1,fase3[6]);
        lc.setRow(0,2,fase3[5]);
        lc.setRow(0,3,fase3[4]);
        lc.setRow(0,4,fase3[3]);
        lc.setRow(0,5,fase3[2]);
        lc.setRow(0,6,fase3[1]);
        lc.setRow(0,7,fase3[0]);
        airSignalStep=4;
      } else if(airSignalStep==4){
        lc.setRow(0,0,fase2[7]);
        lc.setRow(0,1,fase2[6]);
        lc.setRow(0,2,fase2[5]);
        lc.setRow(0,3,fase2[4]);
        lc.setRow(0,4,fase2[3]);
        lc.setRow(0,5,fase2[2]);
        lc.setRow(0,6,fase2[1]);
        lc.setRow(0,7,fase2[0]);
        airSignalStep=5;
      } else if(airSignalStep==5){
        lc.setRow(0,0,fase1[7]);
        lc.setRow(0,1,fase1[6]);
        lc.setRow(0,2,fase1[5]);
        lc.setRow(0,3,fase1[4]);
        lc.setRow(0,4,fase1[3]);
        lc.setRow(0,5,fase1[2]);
        lc.setRow(0,6,fase1[1]);
        lc.setRow(0,7,fase1[0]);
        airSignalStep=0;
      }
  }
}
/*
 This method will display the characters for the
 word "Arduino" one after the other on the matrix. 
 (you need at least 5x7 leds to see the whole chars)
 */
void writeArduinoOnMatrix() {
  /* here is the data for the characters */
  byte okwd[8]={0x1c,0x22,0x22,0x1c,0x00,0xfe,0x14,0x22};
  byte hiwd[8]={0x00,0x3e,0x08,0x3e,0x00,0x2e,0x00,0x00};
  byte fawd[8]={0x00,0x0c,0x62,0x01,0x01,0x62,0x0c,0x00};
  byte ci1[8]={0x00,0x3c,0x42,0x42,0x42,0x42,0x3c,0x00};
  byte ci2[8]={0x00,0x3c,0x7e,0x66,0x66,0x7e,0x3c,0x00};
  byte ci3[8]={0x00,0x3c,0x7e,0x7e,0x7e,0x7e,0x3c,0x00};
  byte n[5]={B00111110,B00010000,B00100000,B00100000,B00011110};
  byte o[5]={B00011100,B00100010,B00100010,B00100010,B00011100};

  /* now display them one by one with a small delay */
  lc.setRow(0,0,okwd[7]);
  lc.setRow(0,1,okwd[6]);
  lc.setRow(0,2,okwd[5]);
  lc.setRow(0,3,okwd[4]);
  lc.setRow(0,4,okwd[3]);
  lc.setRow(0,5,okwd[2]);
  lc.setRow(0,6,okwd[1]);
  lc.setRow(0,7,okwd[0]);
  delay(delaytime);
  lc.setRow(0,0,hiwd[7]);
  lc.setRow(0,1,hiwd[6]);
  lc.setRow(0,2,hiwd[5]);
  lc.setRow(0,3,hiwd[4]);
  lc.setRow(0,4,hiwd[3]);
  lc.setRow(0,5,hiwd[2]);
  lc.setRow(0,6,hiwd[1]);
  lc.setRow(0,7,hiwd[0]);
  delay(delaytime);
  lc.setRow(0,0,fawd[7]);
  lc.setRow(0,1,fawd[6]);
  lc.setRow(0,2,fawd[5]);
  lc.setRow(0,3,fawd[4]);
  lc.setRow(0,4,fawd[3]);
  lc.setRow(0,5,fawd[2]);
  lc.setRow(0,6,fawd[1]);
  lc.setRow(0,7,fawd[0]);
  delay(delaytime);
  lc.setRow(0,0,ci1[7]);
  lc.setRow(0,1,ci1[6]);
  lc.setRow(0,2,ci1[5]);
  lc.setRow(0,3,ci1[4]);
  lc.setRow(0,4,ci1[3]);
  lc.setRow(0,5,ci1[2]);
  lc.setRow(0,6,ci1[1]);
  lc.setRow(0,7,ci1[0]);
  delay(600);
  lc.setRow(0,0,ci2[7]);
  lc.setRow(0,1,ci2[6]);
  lc.setRow(0,2,ci2[5]);
  lc.setRow(0,3,ci2[4]);
  lc.setRow(0,4,ci2[3]);
  lc.setRow(0,5,ci2[2]);
  lc.setRow(0,6,ci2[1]);
  lc.setRow(0,7,ci2[0]);
  delay(600);
  lc.setRow(0,0,ci3[7]);
  lc.setRow(0,1,ci3[6]);
  lc.setRow(0,2,ci3[5]);
  lc.setRow(0,3,ci3[4]);
  lc.setRow(0,4,ci3[3]);
  lc.setRow(0,5,ci3[2]);
  lc.setRow(0,6,ci3[1]);
  lc.setRow(0,7,ci3[0]);
  delay(600);
}

/*
  This function lights up a some Leds in a row.
 The pattern will be repeated on every row.
 The pattern will blink along with the row-number.
 row number 4 (index==3) will blink 4 times etc.
 */
void rows() {
  for(int row=0;row<8;row++) {
    delay(delaytime);
    lc.setRow(0,row,B10100000);
    delay(delaytime);
    lc.setRow(0,row,(byte)0);
    for(int i=0;i<row;i++) {
      delay(delaytime);
      lc.setRow(0,row,B10100000);
      delay(delaytime);
      lc.setRow(0,row,(byte)0);
    }
  }
}

/*
  This function lights up a some Leds in a column.
 The pattern will be repeated on every column.
 The pattern will blink along with the column-number.
 column number 4 (index==3) will blink 4 times etc.
 */
void columns() {
  for(int col=0;col<8;col++) {
    delay(delaytime);
    lc.setColumn(0,col,B10100000);
    delay(delaytime);
    lc.setColumn(0,col,(byte)0);
    for(int i=0;i<col;i++) {
      delay(delaytime);
      lc.setColumn(0,col,B10100000);
      delay(delaytime);
      lc.setColumn(0,col,(byte)0);
    }
  }
}

/* 
 This function will light up every Led on the matrix.
 The led will blink along with the row-number.
 row number 4 (index==3) will blink 4 times etc.
 */
void single() {
  for(int row=0;row<8;row++) {
    for(int col=0;col<8;col++) {
      delay(delaytime);
      lc.setLed(0,row,col,true);
      delay(delaytime);
      for(int i=0;i<col;i++) {
        lc.setLed(0,row,col,false);
        delay(delaytime);
        lc.setLed(0,row,col,true);
        delay(delaytime);
      }
    }
  }
}

void printMatrix(byte figureToPrint[8]){
  lc.setRow(0,0,figureToPrint[7]);
  lc.setRow(0,1,figureToPrint[6]);
  lc.setRow(0,2,figureToPrint[5]);
  lc.setRow(0,3,figureToPrint[4]);
  lc.setRow(0,4,figureToPrint[3]);
  lc.setRow(0,5,figureToPrint[2]);
  lc.setRow(0,6,figureToPrint[1]);
  lc.setRow(0,7,figureToPrint[0]);
}

void loop() {
  
  //revisamos si podemos preguntarle si esta en modo CFG
  if(askForStatus==0){
    airSignal();
  }
  if (((signed long)(millis() - isInResetTimeOut)) > 0 && askForStatus==0) {
    Serial.println("AT+SZ_CHECKCONFIG=NETWORKTYPE");
    askForStatus=1;
    printMatrix(tickSymbol);
  } 
    
  if(Serial.available()){
  incomingCharacter=Serial.read();
   if (((signed long)(millis() - isInResetTimeOut)) > 0 && askForStatus!=3) {
     
     /*
      en esta seccion se determina si acabamos de salir de reset y el wifi esta en modo
      CFG, entonces si esta en ese modo reseteamos a configuracion de fabrica
    */
    if (incomingCharacter == '\n' || incomingCharacter== '\r') {
      //romper el renglon hasta encontrar el fin de los detalles
      //como ya llegamos al fin de linea enviamos el comando de regresar a configuracion
      //de AP
      if(askForStatus==1){
        printMatrix(resetSymbol);
        Serial.println("AT+SZ_NETWORKTYPE=AP");
        askForStatus=2;
      } else if(askForStatus==2){
       //le pedimos al wifi resetear y guardar 
       Serial.println("AT+SZ_SAVE_CONFIG=SAVE");
       askForStatus=3;
      }
      incomingString="X";
    }
   } else {
       //Serial.println(incomingCharacter);
     /*
     En caso de que no este en configuracion entonces
     se ejecuta el codigo normal como siempre
     esta seccion del codigo es para ejecutar el codigo normal de suitch
     */
     /*
     dependiendo del mensaje que recibimos por serial analizamos que es lo que deseamos
     */
     if (incomingCharacter != '\n' && incomingCharacter!= '\r') {
       incomingString=incomingString+incomingCharacter;
     }
     if (incomingCharacter != '\n' && incomingCharacter!= '\r' && loginStatus==6) {
       EEPROM.write(eeAddress, incomingCharacter);
       eeAddress+=1;
     }
     if (incomingCharacter == '\n' || incomingCharacter== '\r') {
       if(loginStatus==6){
        //estabamos en recepcion de token y al parecer ya lo recibimos asi que pasamos a loginstatus 3
       //para mostrarlo en el lcd
      loginStatus=3; 
       }
       if(incomingString=="XCLEARTOKN"){
       int internalMem=0;
        while(EEPROM.read(internalMem)!=255){
           EEPROM.write(internalMem, 255);
           internalMem+=1;
         }
         //Serial.println("DONE");
       } else if(incomingString=="XWLCME"){
         //entramos bien asi que hay que decir que estamos dentro
         loginStatus=1;
         printMatrix(tickSymbol);
         delay(2000);
         lc.clearDisplay(0);
       } else if(incomingString=="XSUON"){
         //este comando prende el relay del suitch
         //Serial.println("relay prendido");
       } else if(incomingString=="XSUOFF"){
         //este comando apaga el relay del suitch
         //Serial.println("relay apagado");
       } else if(incomingString=="XSOS"){
         //este comando apaga el relay principal y muestra que va a ver un terremoto
         //Serial.println("TERREMOTO");
       } else if(incomingString=="XTOKEN?"){
         askForStatus=3;
         //llegamos hasta esta parte entonces hay que enviarle el string de conexion al sistema
         /* 
         para enviar el string de conexion hay que ver si ya tenemos uno almacenado en la eeprom
         en la direccion 0 almacenamos la primera parte
         en la direccion 1 almacenamos la segunda
         si no tenemos token unico, entonces enviamos una peticion para crear uno con el comando
         -loremipsumdolore-
         este comando nos regresa el string de conexion, asi que lo guardamos en la eeprom
         */
         if(EEPROM.read(0)==255){
           //significa que no tiene usuario guardado, hay que invocar el comando
           if(versionSync==0){
             //cuando es cero es un suitch v1
             Serial.println("Lorem ipsum dolor sit amet");
           } else if(versionSync==1){
            //scanner NFC 
            Serial.println("Lorem ipsum dolor cfn amet");
           }
           loginStatus=6;
         } else{
           int internalMem=0;
           while(EEPROM.read(internalMem)!=255){
             tokenString=tokenString+char(EEPROM.read(internalMem));
             internalMem+=1;
           }
           Serial.println(tokenString);
         }
       } else if(incomingString=="XINVISIBLE"){
        /* 
       el status invisible es cuando el
          dispositivo no tiene propietario en el servidor
         entonces hay que desplegar su login id en la matriz
        usando el status de login 3
       */ 
         loginStatus=3;
       }
       if(loginStatus==6){
         /*
         usamos este if para saber cuando recibimos una clabe de usuario creada
         desde el servidor
         regresamos a status de login 1, osea adentro del servidor
         */
         
       }
       incomingString="X";
       
      }
     
  }
  } else {
    /***************
    si no tenemos recepcion por serial
    hay que entrar al ciclo principal de suitch
    ****************/
    if(((signed long)(millis() - isInResetTimeOut)) > 0){
      if(loginStatus==0){
        /*
        si aqui tenemos
        */
        //airSignal();
      } else if(loginStatus==3){
       //desplegar el token en matriz 
      }
      
    
    
    
    
    
    
    
    
    
    
    
    
    }
  }
}
