/*
  DigitalReadSerial
 Reads a digital input on pin 2, prints the result to the serial monitor 
 
 This example code is in the public domain.
 */
int reset=0;
int waitingCharacter=0;
String incomingString;
char incomingCharacter;
signed long timeout=0;
signed long nextPing=0;

void startConnection(){
  incomingString=String("X");
  waitingCharacter=0;
  reset=1;
  digitalWrite(12,LOW);
  delay(2000);
  digitalWrite(12,HIGH);
  digitalWrite(13,LOW);

  //Serial.println("GO");
}
// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  
  // make the pushbutton's pin an input:
  pinMode(9, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  digitalWrite(13,LOW);
  digitalWrite(9,LOW);
  delay(200);
  digitalWrite(13,HIGH);
  digitalWrite(9,HIGH);
  delay(200);
  digitalWrite(13,LOW);
  digitalWrite(9,LOW);
  delay(200);
  digitalWrite(13,HIGH);
  digitalWrite(9,HIGH);
  delay(200);
  digitalWrite(13,LOW);
  digitalWrite(9,LOW);
  
  Serial.begin(9600);
  nextPing = millis() + 20000;
  timeout = millis() + 30000;
  startConnection();
    
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input pin:
  if(Serial.available()){
  incomingCharacter=Serial.read();
  if (incomingCharacter == '\n' || incomingCharacter== '\r') {
   //testear las entradas
    if(reset==1){
     //como lo reseteamos esperamos que la palabra sea READY
     if(incomingString=="X*READY*"){
       //enviamos el comando de conexion
       incomingString="X";
       Serial.print("$$$");
     } else if(incomingString=="XCMD") {
       incomingString="X";
       Serial.println("join Unknown");       
     } else if(incomingString=="XListen on 2000"){
       //ya nos conectamos salir del modo reset
       incomingString="X";
       Serial.println("open lineaccess.mx 3000");
     } else if(incomingString=="X*OPEN*TOKEN?"){
       incomingString="X";
       reset=0;
       //this is the department vacuum
       Serial.println("709e5c87/de12bf0e");
     } else {
       incomingString="X";
    }
    } else {
     //una vez conectados enviamos token y esperamos respuesta
     if(incomingString=="XWLCME"){
      digitalWrite(13,HIGH);
      //ingresamos correctamente, enviar un tweet
       //Serial.println("twitter hello world from arduino wifi");
      // waitingCharacter=1;
      
      incomingString="X";
     } else if(incomingString=="XOK"){
       //reseteamos contadores
       waitingCharacter=0;
       incomingString="X";
       
     } else if(incomingString=="XPING"){
       incomingString="X";
       waitingCharacter=0;
     } else if(incomingString=="XON" || incomingString=="Xon"){
       incomingString="X";
       waitingCharacter=0;
       digitalWrite(9,HIGH);
     } else if(incomingString=="XOFF" || incomingString=="Xoff"){
       incomingString="X";
       waitingCharacter=0;
       digitalWrite(9,LOW);
     }
    }
  } else if(incomingCharacter!=0xFF){
   //pegar los caracteres
   incomingString=incomingString+incomingCharacter;
   //Serial.println(incomingString);
  }
 
  }
   delay(1);        // delay in between reads for stability
   if(reset==1){
      if (((signed long)(millis() - nextPing)) > 0) {
        nextPing = millis() + 40000;
        //ayuda a saber si aun no se conecta, en tal caso reconectar
        startConnection();
      }
  }
  if(reset==0 && waitingCharacter==1){
    if (((signed long)(millis() - timeout)) > 0) {
        nextPing = millis() + 40000;
        timeout = millis() + 50000;
        //ayuda a saber si aun no se conecta, en tal caso reconectar
        startConnection();
     }
  } else if(reset==0 && waitingCharacter==0){
    if (((signed long)(millis() - nextPing)) > 0) {
       nextPing = millis() + 40000;
        timeout = millis() + 50000;
        //ayuda a saber si aun no se conecta, en tal caso reconectar
        waitingCharacter=1;
        Serial.println("ping");
     }
  }
     
}



