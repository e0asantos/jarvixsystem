#!/usr/bin/env ruby
# encoding: utf-8
#Encoding.default_external = Encoding::UTF_8
#Encoding.default_internal = Encoding::UTF_8
Encoding.default_external = "utf-8"
Encoding.default_internal  = "utf-8"
require 'rubygems'
require 'savon'
require 'uri'
require 'net/http'
require 'json'


client = Savon.client(:wsdl=> "http://localhost:8000/?wsdl")
#response=client.call(:say_hello,message:{name:'94dd68dd-e2d5-404a-93f7-180be4ce1e57'})
response=client.call(:say_hello,message:{name:'55d5cbd3-cd69-4b5e-9356-25bd82f8d0f0'})
mjson=JSON.parse(response.body[:say_hello_response][:say_hello_result][:string][13..-1])
puts mjson["result"][0]["alternative"][0]["transcript"]

