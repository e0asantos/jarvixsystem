require 'eventmachine'

 module EchoServer
   def post_init
     puts "-- someone connected to the echo server!"
     EchoServer.list << self
   end

   def self.list
    @list ||= []
   end

   def receive_data data

        puts ">>>you sent: #{data}"
        EchoServer.list.each{ |c| 
            send_data "pic\n"
        }
        close_connection if data =~ /quit/i
   end

   def unbind
     puts "-- someone disconnected from the echo server!"
   end
end

# Note that this will block current thread.
EventMachine.run {
  EventMachine.start_server "0.0.0.0", 3002, EchoServer
}