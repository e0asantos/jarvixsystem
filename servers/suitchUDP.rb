require 'net/http'
require 'rubygems'
require 'eventmachine'
require 'yaml'
require 'active_record'
require 'logger'
require 'json'
require 'uuidtools'
require '/usr/src/jarvixServer/db/model/api_token_user_interface.rb'
require '/usr/src/jarvixServer/db/model/anet_log.rb'
require '/usr/src/jarvixServer/db/model/anet_command.rb'
require '/usr/src/jarvixServer/db/model/anet_value.rb'
require '/usr/src/jarvixServer/jarvixSystem/ILineAccess.rb'
require '/usr/src/jarvixServer/db/model/Jarvix_Custom_Property.rb'
require '/usr/src/jarvixServer/db/model/anet_device.rb'
require '/usr/src/jarvixServer/db/model/JarvixPersonality.rb'
require '/usr/src/jarvixServer/db/model/sys_user.rb'
require '/usr/src/jarvixServer/db/model/sys_user_phone.rb'
require '/usr/src/jarvixServer/lib/twitter/JTwitter.rb'
require '/usr/src/jarvixServer/lib/google/GtalkHelper.rb'
require '/usr/src/jarvixServer/db/model/sys_phrases.rb'
require '/usr/src/jarvixServer/db/model/qiwo_enterprise.rb'
require '/usr/src/jarvixServer/db/model/qiwo_product.rb'
require '/usr/src/jarvixServer/db/model/nfc_reader.rb'
require '/usr/src/jarvixServer/db/model/qiwo_log_kart.rb'
require "uuidtools"
require 'mail'



#Aquiring the needed models from the app



  module EchoServer
    attr_accessor :techname
    attr_accessor :name
    attr_accessor :anet_id
    attr_accessor :isclosed
    attr_accessor :incomingString
     def self.list
      @list ||= []
    end

    def self.devicesLogged
      if @devicesLogged==nil
        @devicesLogged=Hash.new
      end
      return @devicesLogged
    end
      
    def is_numeric(obj) 
      obj.to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/) == nil ? false : true
  end

    def lineAccessToJarvix(laUser)
        userPhone=SysUserPhone.where(:sys_user=>laUser,:sys_phone_type=>1)
        allPhones=[]
        puts userPhone.inspect
        userPhone.each do |singlePhone|
          allPhones.push(singlePhone.number)
        end
        loadOntosEquals=Jarvix_Custom_Property.where(:property_name=>"number",:property_value=>allPhones).first
        if loadOntosEquals==nil
          # creamos la personalidad
          personat=Jarvix_Personality.new
          personat.my_name="Stranger"
          personat.my_noun="I"
          personat.sex="m"
          personat.save
          # creamos una propiedad
          loadOntosEquals=Jarvix_Custom_Property.new
          loadOntosEquals.property_name="number"
          loadOntosEquals.property_value=allPhones[0]
          loadOntosEquals.id_user=personat.id
          loadOntosEquals.save
        end
        puts loadOntosEquals.inspect
        # puts loadOntosEquals.inspect
        return loadOntosEquals.id_user
      end
    def sendInboxToId(iDList,mensaje)
      token=Api_Token_User_Interface.where(:sys_user=>11,:api_interface=>2).last

      iDList.each do |singleList|

        direccionString=Sys_User.find_by_id(singleList)
        if direccionString!=nil
          cuerpoMensaje='Hola '+direccionString.name.to_s+', '+mensaje
          # puts cuerpoMensaje.inspect
          # puts cuerpoMensaje.inspect
          id = '-100005654102627@chat.facebook.com'
          if direccionString.user_id!=nil
              to = '-'+direccionString.user_id+'@chat.facebook.com'
            #buscar producto
              body = cuerpoMensaje
              subject = 'no titulo'
              message = Jabber::Message.new to, body
              message.subject = subject
              
              client = Jabber::Client.new Jabber::JID.new(id)
              client.connect

              client.auth_sasl(Jabber::SASL::XFacebookPlatform.new(client, '357503864375536', token.token, 'e32f8dbdee5cb477333991302a1c78bf'), nil)
              
              client.send message
              client.close
            else
              #analizar si es un telefono de telegram
            end
          end
      end

    end

    def notificateOnChange(aDevice,msgToSend)
      accessLine=ILineAccess.new(lineAccessToJarvix(EchoServer.devicesLogged[@techname].id_owner))
      # puts Sys_User.where(:id=>EchoServer.devicesLogged[@techname].id_owner).last.email
      # hay que enviar notificaciones en caso de que el dispositivo este marcado como para recibir notificaciones
      if aDevice.notify_hangout==1
        # conectar con el servidor remoto
        # uri = URI.parse("http://zorrometro-cware.rhcloud.com/hangouts.json") 
        # response = Net::HTTP.post_form(uri, {"hangout"=>{"frommail"=>"asb.studios@gmail.com", "tomail"=>"amarramizapato@gmail.com", "token"=>"ya29.LgBIa6BfQViJCBoAAACCbIhCqPPQn_QIet38vDiWKi9LYyg9SPKLhBBIA_tZPw", "message"=>"123"}})
        # puts response.inspect
        # primero cargamos el usuario del zorro para que pueda enviar notificaciones
        # hangoutToken=Api_Token_User_Interface.where("sys_user=33 and api_interface=6")[0]
        # puts hangoutToken.inspect
        # hangoutToken.forceUpdateToken()
        # objeto=Hash.new
        # objeto[:frommail]="asb.studios@gmail.com"
        # objeto[:tomail]="amarramizapato@gmail.com"
        # objeto[:token]="ya29.LgAkTHVm6RrYsBoAAABrqY7E2Xf4rIPJNy0aGpENblAuYlVI7e8xW4gE2ujveA"
        # objeto[:message]="123"

        # puts objeto.to_json
        # # response = Net::HTTP.post_form(uri, {"hangout":{"frommail": "asb.studios@gmail.com", "tomail": "amarramizapato@gmail.com", "token":"ya29.LgBIa6BfQViJCBoAAACCbIhCqPPQn_QIet38vDiWKi9LYyg9SPKLhBBIA_tZPw", "message"=>"123"}})
        # # puts response


        # # postData = Net::HTTP.post_form(URI.parse('http://localhost:3000/hangouts.json'), {:hangout=>{"frommail"=>'asb.studios@gmail.com'}})
        # JSON.load `curl -H 'Content-Type:application/json' -H 'Accept:application/json' -X POST http://zorrometro-cware.rhcloud.com/hangouts.json -d '#{objeto.to_json}'`
      end
      if aDevice.notify_fb==1
        accessLine.runApp("facebook.post","Device was connected to the network")+"\n"
      end
      if aDevice.notify_email==1
        # hay dos formas de enviar el mail, con el mail registrado del usuario
        # extraer los mail

        mandrill = Mandrill::API.new 'zC7yPLbPFNbpYqWLbxs3IA'
        message = {"html"=>'Your device just connected',
         "text"=>msgToSend,
         "subject"=>"Suitch",
         "from_email"=>"suitch@cfdi.ws",
         "from_name"=>"Suitch - Device connected",
         "to"=>
            [{"email"=>Sys_User.where(:id=>EchoServer.devicesLogged[@techname].id_owner).last.email,
                "name"=>"Suitch client",
                "type"=>"to"}],
         "headers"=>{"Reply-To"=>"hola@qiwo.mx"},
         "important"=>false,
         "track_opens"=>nil,
         "track_clicks"=>nil,
         "auto_text"=>nil,
         "auto_html"=>nil,
         "inline_css"=>nil,
         "url_strip_qs"=>nil,
         "preserve_recipients"=>nil,
         "view_content_link"=>nil,
         "tracking_domain"=>nil,
         "signing_domain"=>nil,
         "return_path_domain"=>nil,
         "merge"=>true,
         "global_merge_vars"=>[{"name"=>"merge1", "content"=>"merge1 content"}],
         "merge_vars"=>
            [{"rcpt"=>"recipient.email@example.com",
                "vars"=>[{"name"=>"merge2", "content"=>"merge2 content"}]}],
         "tags"=>["password-resets"],
         "attachments"=>
            [],
         "images"=>
            []}
        async = false
        ip_pool = "Main Pool"
        send_at = DateTime.now-2.hours
        result = mandrill.messages.send message, async, ip_pool, send_at
      end
      if aDevice.notify_twitter==1
        send_data accessLine.runApp("twitter.publish",msgToSend)+"\n"
      end
      if aDevice.notify_fbinbox==1
        sendInboxToId([EchoServer.devicesLogged[@techname].id_owner],msgToSend)
      end
    end
    def post_init (*args)
      @isclosed=0
      @techname = "anet_#{rand(99999)}"
      @name = "anet_#{rand(99999)}"
      puts "entrando:"+@techname.to_s
      #EchoServer.list.each{ |c| c.send_data "#{@name} has joined.\n" }
      EchoServer.list << self
      EchoServer.devicesLogged[@techname]="."
      # send_data "TOKEN?\n"
    end

    def unbind
        @isclosed=1
        if @anet_id!=nil and @anet_id!=1
            AnetLog.new(:arduino_id=>@anet_id,:connection_type=>0).save
        end
      puts "salio:"+@techname.to_s
      begin
        notificateOnChange(EchoServer.devicesLogged[@techname],"Device "+EchoServer.devicesLogged[@techname].object+" just disconnected at "+EchoServer.devicesLogged[@techname].location+"! :o -@suitch_")  
      rescue Exception => e
        
      end
      
      EchoServer.list.delete self
      #EchoServer.list.each{ |c| c.send_data "#{@name} has left.\n" }
    end


    def receive_data data
    
     puts "incoming data:"+data.inspect
      if (data=="\n" or data=="\r") and (@incomingString==nil or @incomingString=="")
          return
      end
      if @incomingString==nil
        @incomingString=""
      end
      @incomingString=@incomingString+data
      if @incomingString.index("\n")!=nil or @incomingString.index("\r")!=nil
          # fix para lectores nfc
          # incoming data:"Read fa"
          # incoming data:"iled 6\r\nnfcreadr CD 91 4C 78#.......................#10\r\n"
          # EMPIEZA:"Read failed 6nfcreadr CD 91 4C 78#.......................#10"
          # [#<Sys_User_Phone id: 63, sys_user: 11, sys_phone_type: 1, number: "4448573200", removed: false>, #<Sys_User_Phone id: 79, sys_user: 11, sys_phone_type: 1, number: "4448573202", removed: false>]
          # #<Jarvix_Custom_Property id: 64, created_at: "2013-09-30 14:08:46", updated_at: "2013-09-30 14:08:46", property_name: "number", property_value: "4448573202", is_enabled: nil, id_user: 25, id_custom_object: 29>
          # ["ed 6nfcreadr CD 91 4C 78", ".......................", "10"]
          # THIS LECTOR:257f5204

          # incoming data:"R"
          # incoming data:"ead failed 11\r\nnfcreadr 04 C9 79 5A 54 3C 80#io.dartl....Y#10\r\n"
          # EMPIEZA:"Read failed 11nfcreadr 04 C9 79 5A 54 3C 80#io.dartl....Y#10"
          # [#<Sys_User_Phone id: 63, sys_user: 11, sys_phone_type: 1, number: "4448573200", removed: false>, #<Sys_User_Phone id: 79, sys_user: 11, sys_phone_type: 1, number: "4448573202", removed: false>]
          # #<Jarvix_Custom_Property id: 64, created_at: "2013-09-30 14:08:46", updated_at: "2013-09-30 14:08:46", property_name: "number", property_value: "4448573202", is_enabled: nil, id_user: 25, id_custom_object: 29>

          # incoming data:"\n"
          # incoming data:"Error"
          # incoming data:". Failed read page 4\r\nnfcreadr 04 C9 79 5A 54 3C 80##10\r\n"
          # EMPIEZA:"Error. Failed read page 4nfcreadr 04 C9 79 5A 54 3C 80##10"
          if @incomingString.index("Read failed ")!=nil
            @incomingString=@incomingString.delete "Read failed "
          end
          if @incomingString.index("Error. Failed read page ")!=nil
            @incomingString=@incomingString.delete "Error. Failed read page "
          end
          #data=data[0..-2].downcase
          @incomingString=@incomingString.delete "\n"
          @incomingString=@incomingString.delete "\r"

      else
       return

      end
      puts "EMPIEZA:"+@incomingString.inspect
      # if data.index("\r")!=nil
      #     #data=data[0..-2].downcase
      #     data=data[0..-2]
      # end
      if EchoServer.devicesLogged[@techname]=="."
        #esta. recibiendo token
        #buscar si existe el dispositivo
        
        device=@incomingString.split("/")
        if device.length==2
          aDevice=AnetDevice.where(:token=>device[0],:secret_token=>device[1]).last
          puts aDevice.inspect
          if aDevice!=nil
              #buscar si ya esta loggeado, en ese caso desloggear
              alreadyLogged=false
            EchoServer.list.each{ |c| 
                if c.name == "anet_"+device[0] and c.isclosed==0
                    c.isclosed=1
                    c.close_connection
                end
            }
            if alreadyLogged==false
                @name = "anet_"+device[0]
                EchoServer.devicesLogged[@techname]=aDevice
                @anet_id=aDevice.id
                if @anet_id!=1
                   port, ip = Socket.unpack_sockaddr_in(get_peername)
                    AnetLog.new(:arduino_id=>aDevice.id,:connection_type=>1,:created_at=>DateTime.now+5.seconds,:mip=>ip).save
                
                generalLogs = Sys_Phrase.new(:phone=>EchoServer.devicesLogged[@techname].token,:interface_executed=>11,:callid=>UUIDTools::UUID.random_create.to_s())
                generalLogs.save
                generalLogs.phrase="iThing logged into system"
                generalLogs.save
                end
                
                if aDevice.id_owner==nil
                  puts "WLCMEX"
                  send_data "WLCMEX\n"  
                else
                  puts "WLCME"
                  send_data "WLCME\n"
                end
                if device[0]=="709e5c87"
                  send_data "on\n"
                end
                
                @incomingString=""
                begin
                  notificateOnChange(aDevice,"Device "+aDevice.object+" connected at "+aDevice.location+"! :) -@suitch_")  
                rescue Exception => e
                  puts "ALGUN ERROR SUCEDIO"
                end
                
            else 
              puts "BUSY"
                send_data "BUSY\n"
                close_connection
            end
          else
            puts "USR_ERROR"
            send_data "USR_ERROR\n"
            close_connection
          end  
        else
          # antes de marcarlo como erroneo
          if @incomingString.index("Lorem ipsum dolor sit amet")!=nil
            port, ip = Socket.unpack_sockaddr_in(get_peername)
            # suitch inicia el intercambio de generacion de token
            @anet_device = AnetDevice.new
            @anet_device.description="Suitch device v1.5"
            @anet_device.location="home"
            @anet_device.object="light"
            #creamos un token y un token secret
            # @anet_device.id_owner=session[:activeUser].id
            @anet_device.token=UUIDTools::UUID.random_create.to_s()
            @anet_device.mip=ip
            @anet_device.token=@anet_device.token[0..7]
            @anet_device.secret_token=UUIDTools::UUID.random_create.to_s()
            @anet_device.secret_token=@anet_device.secret_token[0..7]
            @anet_device.is_public=0
            @anet_device.save

            comandon=AnetCommand.new
            comandon.arduino_id=@anet_device.id
            comandon.command="Turn on"
            comandon.command_data="SUON"
            comandon.save
            comandon2=AnetCommand.new
            comandon2.arduino_id=@anet_device.id
            comandon2.command="Turn off"
            comandon2.command_data="SUOFF"
            comandon2.save
            puts "DEVICE CREATED"
            send_data @anet_device.token+"/"+@anet_device.secret_token
            @incomingString=""
          else
            puts "TOKEN_ERROR"
            send_data "TOKEN_ERROR\n"
            @incomingString=""
            # close_connection
          end
        end
      else
        #aqui hay que poner los comandos
        #fbwall
        #inboxfb
        #mail
        #evernote
        #twitter
        #val=algo
        #creamos un objeto generico para la interface con lineaccess
        puts "----->"
        accessLine=lineAccessToJarvix(EchoServer.devicesLogged[@techname].id_owner)
        puts "<------"+accessLine.inspect
        if @incomingString.index("=")!=nil and @incomingString.index("==")==nil
          saveAsValue=@incomingString.split("=");
          #buscar si existe el comando, sino, esta enviando pura shit
          incomingCommand=AnetCommand.where(:arduino_id=>EchoServer.devicesLogged[@techname].id,:command_data=>saveAsValue[0]).last
          puts "incomingCommand:"+incomingCommand.inspect
          if incomingCommand!=nil
            if is_numeric(saveAsValue[1])
                valorNuevo=AnetValue.new(:arduino_id=>EchoServer.devicesLogged[@techname].id,:command_id=>incomingCommand.id,:value=>saveAsValue[1])
                valorNuevo.save
            else 
                generalLogs = Sys_Phrase.new(:phrase=>saveAsValue[1],:phone=>EchoServer.devicesLogged[@techname].token,:interface_executed=>11,:callid=>UUIDTools::UUID.random_create.to_s())
                generalLogs.save
                generalLogs.phrase=saveAsValue[1]
                generalLogs.save
            end
            puts "OK1"
            send_data "OK\n"
          else
            puts "CMD_ERROR"
            send_data "CMD_ERROR\n"
          end
        
        elsif @incomingString.index("nfcreadr")!=nil
          # este fix corrige los errores de lectura del nfc
          if @incomingString.index("nfcreadr")!=0
            @incomingString=@incomingString[@incomingString.index("nfcreadr")..-1]
          end
          # A0 76 51 44#1ABCDEF#10
          # compuesto por tres partes
          # UID del nfc que se esta leyendo
          # token de seguridad anticlon
          # tienda a la cual esta asociada este lector
          # UID del scanner
          # UID del cupon escogido para escanear 

          # prueba nfcreadr 60 6C 51 44#holakace#10
          # prueba nfcreadr 59b81ca774684248#c2FsbW85MQ==#23
          # primero separamos la informacion
          rawInfoNfc=@incomingString[9..-1].split("#")
          puts rawInfoNfc.inspect
          puts "THIS LECTOR:"+EchoServer.devicesLogged[@techname].token
          if EchoServer.devicesLogged[@techname].cliente==10
            
            urltoe= 'http://gymapp-cware.rhcloud.com/entradas/logWithCard?nfc='+rawInfoNfc[0].delete(' ')+'&place=1'

            uri = URI(urltoe)
            from_service=Net::HTTP.get(uri) # => String
            generalLogsa = Sys_Phrase.new(:phone=>EchoServer.devicesLogged[@techname].token,:interface_executed=>11,:callid=>UUIDTools::UUID.random_create.to_s())
            generalLogsa.save
            generalLogsa.phrase="id Tarjeta:"+rawInfoNfc[0].delete(' ')+"==>"+from_service.inspect
            generalLogsa.save
            puts from_service.inspect
            if from_service=="OK"
              # prender el suitch
              EchoServer.list.each{ |c| 
                puts "ANET_LIST:"+c.name
                if c.name == "anet_709e5c87"
                    
                    puts "enviando : GYM"
                    c.send_data "off\n"
                    
                    # c.send_data "off\n"
                    break
                end
            }
            end
          elsif rawInfoNfc.length>=3
              # primero buscamos el nfc a ver si lo encontramos
              identificadorNfc=NfcReader.where("last_token like '%"+rawInfoNfc[1]+"%' and static_token like '%"+rawInfoNfc[0]+"%'")
              puts identificadorNfc.inspect
              if identificadorNfc.length>0
                # guardamos el token de seguridad anticlon
                anticlon=UUIDTools::UUID.random_create.to_s()[0..7]
                # identificadorNfc[0].last_token=anticlon
                # resulta que si esta registrado ese nfc, hay que verificar si la tienda tiene productos marcados como reclamables por NFC
                rawTicketNFC=QiwoProduct.where(:nfcTicket=>1,:business_source=>rawInfoNfc[2].to_i)
                ticketNFCIDs=rawTicketNFC.index_by(&:id).keys
                # tomamos el ultimo y lo usamos
                sinReclamar=QiwoLogKart.where(:qiwo_product_id=>ticketNFCIDs,:user_id=>identificadorNfc[0].user_id,:kart_state=>[2,3]).where("nfcTicket IS NULL").last
                if sinReclamar!=nil
                  # tenemos uno sin reclamar vamos a marcarlo
                  sinReclamar.nfcTicket=1
                  sinReclamar.save
                  send_data "OK_TICKET\n"
                else
                  # no tenemos boletos sin reclamar
                  send_data "NO_TICKETS\n"
                end

                # ya cargamos los productos con nfc ahora cargamos las compras cerradas y no reclamadas
                # NfcRequest
                # lo regresamos
                send_data anticlon+"\n"
              else
                # aqui consultamos el servicio de AXESO para el nfc
                urltoe= 'http://servicio.axesovip.com.mx/ServiceAxeso.svc/ValidaNFC?store_uid=21&nfc_uid='+rawInfoNfc[0].delete(' ')
                uri = URI('http://servicio.axesovip.com.mx/ServiceAxeso.svc/ValidaNFC?store_uid=21&nfc_uid='+rawInfoNfc[0].delete(' '))
                from_service=Net::HTTP.get(uri) # => String
                generalLogsa = Sys_Phrase.new(:phone=>EchoServer.devicesLogged[@techname].token,:interface_executed=>11,:callid=>UUIDTools::UUID.random_create.to_s())
                generalLogsa.save
                generalLogsa.phrase=urltoe+"==>"+from_service.inspect
                generalLogsa.save
                puts from_service.inspect

                # send_data "NOT_FOUND_SORRY\n"
                send_data rawInfoNfc[1]+"\n"
                send_data "PONG\n"
              end
        else
          puts " CELULAR NOT FOUND"
        end

        elsif @incomingString.index("fbwall")!=nil
          generalLogs = Sys_Phrase.new(:phone=>EchoServer.devicesLogged[@techname].token,:interface_executed=>11,:callid=>UUIDTools::UUID.random_create.to_s())
          generalLogs.save
          generalLogs.phrase=@incomingString.to_s
          generalLogs.save
          txtToSend=@incomingString[6..-1]
          send_data accessLine.runApp("facebook.post",txtToSend)+"\n"
        elsif @incomingString.index("inboxfb")!=nil
          generalLogs = Sys_Phrase.new(:phone=>EchoServer.devicesLogged[@techname].token,:interface_executed=>11,:callid=>UUIDTools::UUID.random_create.to_s())
          generalLogs.save
          generalLogs.phrase=@incomingString.to_s
          generalLogs.save
          send_data accessLine.runApp("facebook.post",txtToSend)+"\n"
        elsif @incomingString.index("email")!=nil
          generalLogs = Sys_Phrase.new(:phone=>EchoServer.devicesLogged[@techname].token,:interface_executed=>11,:callid=>UUIDTools::UUID.random_create.to_s())
          generalLogs.save
          generalLogs.phrase=@incomingString.to_s
          generalLogs.save
          txtToSend=@incomingString[6..-1]
          #ahora sacar el mail.
          sendToMail=txtToSend[0..txtToSend.index(" ")]
          txtToSend=txtToSend[txtToSend.index(" ")..-1]
          send_data accessLine.runApp("email.address",sendToMail+","+txtToSend)+"\n"
        elsif @incomingString.index("evernote")!=nil

        elsif @incomingString.index("ping")!=nil
           puts "intentando hacer ping desde:"+EchoServer.devicesLogged[@techname].token
           if EchoServer.devicesLogged[@techname].token.index("709e5c87")!=nil
              puts "PING"
             send_data "PING\n"
           else
            puts "PONG"
            send_data "PONG\n"
          end
          
        elsif @incomingString.index("twitter")!=nil
          generalLogs = Sys_Phrase.new(:phone=>EchoServer.devicesLogged[@techname].token,:interface_executed=>11,:callid=>UUIDTools::UUID.random_create.to_s())
          generalLogs.save
          generalLogs.phrase=@incomingString.to_s
          generalLogs.save
          txtToSend=@incomingString[7..-1]
          send_data accessLine.runApp("twitter.publish",txtToSend)+"\n"
        elsif @incomingString.index("sendto")!=nil
            divideCommand=@incomingString.split(" ")
            #el segundo es el dispositivo
            if divideCommand[1]!=nil
            anetToExecute="anet_"+divideCommand[1]
            divideCommand.shift
            divideCommand.shift
            divideCommand=divideCommand.join(" ")
            wasSent=false
            EchoServer.list.each{ |c| 
                if c.name == anetToExecute
                    wasSent=true
                    puts "enviando :"+divideCommand.inspect
                    c.send_data divideCommand+"\n"
                    send_data "OK\n"
                    break
                end
            }
              if wasSent==false
                  send_data "NOT_ONLINE\n"
              end
            end
#        elsif data.index("=")!=nil
#            #significa que tenemos un valor que esta entrando
#            valores=data.split("=")
#            #buscar el valor si existe dentro de los comandos
#            if AnetCommand.where(:command_data=>valores[0],:arduino_id=>EchoServer.devicesLogged[@techname].id).last!=nil and valores[0]!=nil and valores[1]!=nil
#                #insertamos valor en el registro
#                generalLogs = Sys_Phrase.new(:phone=>EchoServer.devicesLogged[@techname].token,:interface_executed=>11,:callid=>UUIDTools::UUID.random_create.to_s())
#                generalLogs.save
#              generalLogs.phrase=valores[1]
#              generalLogs.save
#            end
        else
          send_data "CMD_ERROR\n"
        end
        @incomingString=""  
      end
    (@buf ||= '') << @incomingString
      while line = @buf.slice!(/(.+)\r?\n/)
        if line =~ %r|^/nick (.+)|
          new_name = $1.strip
          (EchoServer.list - [self]).each{ |c| c.send_data "#{@name} is now known as #{new_name}\n" }
          @name = new_name
        elsif line =~ %r|^/quit|
          close_connection
        else
          (EchoServer.list - [self]).each{ |c| c.send_data "#{@name}: #{line}" }
        end
      end
      
    end
  end

  


EM.run do
  EM.open_datagram_socket('0.0.0.0', 3000, EchoServer)
  
end
