#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright © Burak Arslan <burak at arskom dot com dot tr>,
#             Arskom Ltd. http://www.arskom.com.tr
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#    2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#    3. Neither the name of the owner nor the names of its contributors may be
#       used to endorse or promote products derived from this software without
#       specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
# OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#


'''
This is a simple HelloWorld example to show the basics of writing
a webservice using spyne, starting a server, and creating a service
client.

Here's how to call it using suds:

>>> from suds.client import Client
>>> c = Client('http://localhost:8000/?wsdl')
>>> c.service.say_hello('punk', 5)
(stringArray){
   string[] =
      "Hello, punk",
      "Hello, punk",
      "Hello, punk",
      "Hello, punk",
      "Hello, punk",
 }

 29741d9a-234b-4104-9bf6-0515f2b48cab.mp3
>>>
'''


import logging
import json
import wget

from spyne.application import Application
from spyne.protocol.soap import Soap11
from spyne.server.wsgi import WsgiApplication

from spyne.decorator import srpc
from spyne.service import ServiceBase
from spyne.model.complex import Iterable
from spyne.model.primitive import Integer
from spyne.model.primitive import Unicode
from spyne.model.primitive import String


import os
import urllib2
import urllib



class HelloWorldService(ServiceBase):
    @srpc(String, Integer, _returns=Iterable(String))
    def say_hello(name):
        '''
        Docstrings for service methods appear as documentation in the wsdl.
        <b>What fun!</b>

        @param name the name to say hello to
        @param the number of times to say hello
        @return the completed array
        '''
        # filename = wget.download('http://lineaccess.mx/calls/'+name+'.wav')
        # os.system('/usr/bin/flac -8 --totally-silent --channels=1 --endian=little --sign=signed --bps=16 --force-raw-format --sample-rate=16000 /usr/src/lineAccess/public/calls/'+name+'.wav')
        f = open('/usr/src/lineAccess/public/calls/'+name+'.wav','rb')
        flac_cont = f.read()
        f.close()
        # f2 = open('/usr/src/lineAccess/public/calls/94dd68dd-e2d5-404a-93f7-180be4ce1e57.wav','rb')
        # flac_cont2 = f2.read()
        # f2.close()
        lang_code='es'
        # 'https://www.google.com/speech-api/v2/recognize?output=json&lang=en-us&key=AIzaSyBwdKhkBS0VwUhiX8beHePaHmjV6WIuVTs'
        # https://www.google.com/speech-api/v2/recognize?output=json&lang=en-us&key=AIzaSyBOti4mM-6x9WDnZIjIeyEU21OpBXqWBgw&client=chromium&maxresults=6&pfilter=2
        # googl_speech_url = 'https://www.google.com/speech-api/v1/recognize?xjerr=1&client=chromium&pfilter=2&lang=%s&maxresults=6'%(lang_code)
        googl_speech_url='https://www.google.com/speech-api/v2/recognize?output=json&lang='+lang_code+'&key=AIzaSyBOti4mM-6x9WDnZIjIeyEU21OpBXqWBgw&client=chromium&maxresults=6&pfilter=2'
        # hrs = {"User-Agent": "Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.63 Safari/535.7",'Content-type': 'audio/x-flac; rate=8000'}
        hrs={'Content-Type': 'audio/l16; rate=8000'}
        req = urllib2.Request(googl_speech_url, data=flac_cont, headers=hrs)
        p = urllib2.urlopen(req)

        # res = eval(p.read())['hypotheses'][0]['utterance'].decode('utf-8')
        
        res=p.read().decode('utf-8')
        
        
        # os.system(FLAC_CONV+ filename+'.wav')
        # f = open(filename+'.flac','rb')
        # flac_cont = f.read()
        # f.close()
        
        for i in range(1):
            yield '%s' % res


if __name__=='__main__':
    from wsgiref.simple_server import make_server

    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger('spyne.protocol.xml').setLevel(logging.DEBUG)

    logging.info("listening to http://127.0.0.1:8000")
    logging.info("wsdl is at: http://localhost:8000/?wsdl")

    application = Application([HelloWorldService], 'spyne.examples.hello.soap',
                in_protocol=Soap11(validator='lxml'),
                out_protocol=Soap11()
            )
    wsgi_application = WsgiApplication(application)

    server = make_server('127.0.0.1', 8000, wsgi_application)
    server.serve_forever()