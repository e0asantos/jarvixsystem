require 'net/http'
require 'net/https'
require 'rubygems'
require 'eventmachine'
require 'yaml'
require 'active_record'
require 'logger'
require 'json'
require 'uuidtools'
require '/usr/src/jarvixServer/db/model/api_token_user_interface.rb'
require '/usr/src/jarvixServer/db/model/anet_log.rb'
require '/usr/src/jarvixServer/db/model/sys_log.rb'
require '/usr/src/jarvixServer/db/model/anet_command.rb'
require '/usr/src/jarvixServer/db/model/anet_value.rb'
require '/usr/src/jarvixServer/jarvixSystem/ILineAccess.rb'
require '/usr/src/jarvixServer/db/model/Jarvix_Custom_Property.rb'
require '/usr/src/jarvixServer/db/model/anet_device.rb'
require '/usr/src/jarvixServer/db/model/JarvixPersonality.rb'
require '/usr/src/jarvixServer/db/model/sys_user.rb'
require '/usr/src/jarvixServer/db/model/sys_user_phone.rb'
require '/usr/src/jarvixServer/lib/twitter/JTwitter.rb'
require '/usr/src/jarvixServer/lib/google/GtalkHelper.rb'
require '/usr/src/jarvixServer/db/model/sys_phrases.rb'
require '/usr/src/jarvixServer/db/model/qiwo_enterprise.rb'
require '/usr/src/jarvixServer/db/model/qiwo_product.rb'
require '/usr/src/jarvixServer/db/model/nfc_reader.rb'
require '/usr/src/jarvixServer/db/model/qiwo_log_kart.rb'
require '/usr/src/jarvixServer/db/model/sys_dhcp.rb'
require '/usr/src/jarvixServer/db/model/sys_video.rb'
require '/usr/src/jarvixServer/db/model/system_activity.rb'
require "uuidtools"
require 'pusher'
require "base64"
require 'uri'
require 'json'
require 'telegram/bot'
# sendgrid-ruby (4.0.8)
require 'sendgrid-ruby'
include SendGrid
#Aquiring the needed models from the app




  class EchoServer < EM::Connection
    attr_accessor :techname
    attr_accessor :name
    attr_accessor :anet_id
    attr_accessor :isclosed
    attr_accessor :incomingString
    attr_accessor :incomingStringCopy
    attr_accessor :utimer
    attr_accessor :receiving_file
    attr_accessor :receiving_img
    attr_accessor :receiving_contents
    attr_accessor :ping
    attr_accessor :streaming_channel


    def initialize 
      comm_inactivity_timeout = 20
      pending_connect_timeout = 20
    end
    
    def self.list
      @list ||= []
    end

    def self.offlineTimer
      if @offline == nil
        puts "\u001b[31m>>>>offlineTimer::start\u001b[0m"
        @offline = EventMachine::PeriodicTimer.new(15) do
          # verify if the device has been connected
          EchoServer.list.each{ |c| 
          if c.ping != nil
            if (c.ping+60.seconds)<DateTime.now
              puts "\u001b[31m>>OFFLINE detected"
              puts c.techname
              puts "\u001b[0m"
            end
          end
        }
        end
      end
    end

    def self.devicesLogged
      if @devicesLogged==nil
        @devicesLogged=Hash.new
      end
      return @devicesLogged
    end
      
    def is_numeric(obj) 
      obj.to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/) == nil ? false : true
  end

    def lineAccessToJarvix(laUser)
      if laUser == nil
        return nil
      end
      userPhone=SysUserPhone.where(:sys_user=>laUser,:sys_phone_type=>1)
      # si no hay telefonos usar el id del usuario de suitch como telefono
      if userPhone.length == 0
        fakePhone = SysUserPhone.new
        fakePhone.sys_phone_type = 1
        fakePhone.number = "00000"+laUser.to_s
        fakePhone.sys_user = laUser
        fakePhone.save
        userPhone=SysUserPhone.where(:sys_user=>laUser,:sys_phone_type=>1)
      end
      allPhones=[]
      # puts userPhone.inspect
      userPhone.each do |singlePhone|
        allPhones.push(singlePhone.number)
      end
      loadOntosEquals=Jarvix_Custom_Property.where(:property_name=>"number",:property_value=>allPhones).first
      # si es nil no tiene telefonos, o no los ha puesto, entonces ahora buscarlo por personalidad ligada con el id
      # de suitch, eso evitarÃ­a crear multiples personalidades
      # look_for_personality = JarvixPersonality.where(:suitch_user_id => laUser).last
      # if look_for_personality != nil
      #   loadOntosEquals 
      # end
      if loadOntosEquals==nil
        # creamos la personalidad
        personat=Jarvix_Personality.new
        personat.my_name="the network"
        personat.my_noun="it"
        personat.suitch_user_id = laUser
        personat.sex="m"
        personat.save
        
        # creamos una propiedad
        loadOntosEquals=Jarvix_Custom_Property.new
        loadOntosEquals.property_name="number"
        loadOntosEquals.property_value=allPhones[0]
        loadOntosEquals.id_user=personat.id
        loadOntosEquals.save
      end
      # puts loadOntosEquals.inspect
      # puts loadOntosEquals.inspect
      return loadOntosEquals.id_user
    end
    def sendInboxToId(iDList,mensaje)
      token=Api_Token_User_Interface.where(:sys_user=>11,:api_interface=>2).last

      iDList.each do |singleList|

        direccionString=Sys_User.find_by_id(singleList)
        if direccionString!=nil
          cuerpoMensaje='Hola '+direccionString.name.to_s+', '+mensaje
          # puts cuerpoMensaje.inspect
          # puts cuerpoMensaje.inspect
          id = '-100005654102627@chat.facebook.com'
          if direccionString.user_id!=nil
              to = '-'+direccionString.user_id+'@chat.facebook.com'
            #buscar producto
              body = cuerpoMensaje
              subject = 'no titulo'
              message = Jabber::Message.new to, body
              message.subject = subject
              
              client = Jabber::Client.new Jabber::JID.new(id)
              client.connect

              client.auth_sasl(Jabber::SASL::XFacebookPlatform.new(client, '357503864375536', token.token, 'e32f8dbdee5cb477333991302a1c78bf'), nil)
              
              client.send message
              client.close
            else
              #analizar si es un telefono de telegram
            end
          end
      end

    end

    def notificateOnChange(aDevice,msgToSend)
      if EchoServer.devicesLogged[@techname].id_owner == nil
        return nil
      end
      accessLine=ILineAccess.new(lineAccessToJarvix(EchoServer.devicesLogged[@techname].id_owner))
      # puts Sys_User.where(:id=>EchoServer.devicesLogged[@techname].id_owner).last.email
      # hay que enviar notificaciones en caso de que el dispositivo este marcado como para recibir notificaciones
      if aDevice.notify_hangout==1
        puts "TYRING_HANGOUT"
        # conectar con el servidor remoto
        # uri = URI.parse("http://zorrometro-cware.rhcloud.com/hangouts.json") 
        # response = Net::HTTP.post_form(uri, {"hangout"=>{"frommail"=>"asb.studios@gmail.com", "tomail"=>"amarramizapato@gmail.com", "token"=>"ya29.LgBIa6BfQViJCBoAAACCbIhCqPPQn_QIet38vDiWKi9LYyg9SPKLhBBIA_tZPw", "message"=>"123"}})
        # puts response.inspect
        # primero cargamos el usuario del zorro para que pueda enviar notificaciones
        # hangoutToken=Api_Token_User_Interface.where("sys_user=33 and api_interface=6")[0]
        # puts hangoutToken.inspect
        # hangoutToken.forceUpdateToken()
        # objeto=Hash.new
        # objeto[:frommail]="asb.studios@gmail.com"
        # objeto[:tomail]="amarramizapato@gmail.com"
        # objeto[:token]="ya29.LgAkTHVm6RrYsBoAAABrqY7E2Xf4rIPJNy0aGpENblAuYlVI7e8xW4gE2ujveA"
        # objeto[:message]="123"

        # puts objeto.to_json
        # # response = Net::HTTP.post_form(uri, {"hangout":{"frommail": "asb.studios@gmail.com", "tomail": "amarramizapato@gmail.com", "token":"ya29.LgBIa6BfQViJCBoAAACCbIhCqPPQn_QIet38vDiWKi9LYyg9SPKLhBBIA_tZPw", "message"=>"123"}})
        # # puts response


        # # postData = Net::HTTP.post_form(URI.parse('http://localhost:3000/hangouts.json'), {:hangout=>{"frommail"=>'asb.studios@gmail.com'}})
        # JSON.load `curl -H 'Content-Type:application/json' -H 'Accept:application/json' -X POST http://zorrometro-cware.rhcloud.com/hangouts.json -d '#{objeto.to_json}'`
      end
      if aDevice.notify_fb==1
        puts "TYRING_FB_POST"
        # accessLine.runApp("facebook.post","Device was connected to the network")+"\n"
      end
      if aDevice.notify_email==1
        puts "TYRING_EMAIL"
        # hay dos formas de enviar el mail, con el mail registrado del usuario
        # extraer los mail
        # clientMail = SendGrid::API.new(api_key: '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')
        # singleMail = SendGrid::Mail.new
        # singleMail.from = Email.new(email: 'noreply@consultware.mx')
        # singleMail.subject = 'Suitch - Connected'
        # personalization = Personalization.new
        #   finalArrayEmail=[]
        #   Sys_User.where(:id=>EchoServer.devicesLogged[@techname].id_owner).each do |singleEmail|
        #     personalization.to=Email.new(email: singleEmail.email, name: "Suitch usuario")
        #   end
        #   singleMail.personalizations=personalization
        #   content = SendGrid::Content.new(type: 'text/plain', value: msgToSend)
        #   singleMail.contents = content;
        #   response=clientMail.client.mail._('send').post(request_body: singleMail.to_json)
        #   puts response.status_code
        #   puts response.body
        #   puts response.headers
        begin
          uri = URI.parse("http://localhost:3002/mail-notification") 
          header = {'Content-Type'=> 'text/json'}
          https = Net::HTTP.new(uri.host,uri.port)
          # https.use_ssl = true
          req = Net::HTTP::Post.new(uri)
          req['Content-Type'] = 'application/json'
          finalArrayEmail=[]
          Sys_User.where(:id=>EchoServer.devicesLogged[@techname].id_owner).each do |singleEmail|
            finalArrayEmail.push({"email": singleEmail.email, "name": "Dear #{singleEmail.name}"})
          end
          # The body needs to be a JSON string, use whatever you know to parse Hash to JSON
          req.body = {recipients:finalArrayEmail,template:"connection_v1",device:EchoServer.devicesLogged[@techname].object}.to_json
          # Send the request
          response = https.request(req)
          puts response.body
        rescue

        end
      end
      if aDevice.notify_twitter==1
        send_data accessLine.runApp("twitter.publish",msgToSend)+"\n"
        puts "TYRING_TWITTER"
      end
      if aDevice.notify_fbinbox==1
        # sendInboxToId([EchoServer.devicesLogged[@techname].id_owner],msgToSend)
        fbtoken = Api_Token_User_Interface.where(:sys_user => EchoServer.devicesLogged[@techname].id_owner, :api_interface => 16)
        fbtoken.each do |singleInbox|
          messengerBot(msgToSend, singleInbox.uid)
        end
        puts "TYRING_FB_INBOX"
      end
      if aDevice.notify_telegram == 1
        puts "TYRING_TELEGRAM"
        tokenTelegram = Api_Token_User_Interface.where(:sys_user=>EchoServer.devicesLogged[@techname].id_owner,:api_interface=>15)
        tokenTelegram.each do |singleTelegram|
          Telegram::Bot::Client.run("982932162:AAHe5ja0swjNtu5pBQPzrDDgIcHyLg55QkI") do |bot|
            bot.api.send_message(chat_id: singleTelegram.uid, text: msgToSend)
          end
        end
      end
    end
  def messengerBot(message, user) 
    messageData = Hash.new
    messageData["recipient"]=Hash.new
    messageData["recipient"]["id"] = user
    messageData["message"]=Hash.new
    messageData["message"]["text"] = message
    messageData["messaging_type"] = "UPDATE"
    body = messageData.to_json
    uri = URI.parse("https://graph.facebook.com/v4.0/me/messages")
    https = Net::HTTP.new(uri.host,uri.port)
    https.use_ssl = true
    req = Net::HTTP::Post.new(uri.path+"?access_token=EAAFFJdrXiPABAFB3a9U1ZAqcZC7TxF3smIPGtAbN8ZBXgO5EQZA4BcdAacdFXNW8IrhUE6weDZA8rVnUHBLvnjeLa4Y4DWk0ASg7fUcRm6JWXPyR5E6SN2i5NVEHbRPWKPIaKTfkLhGlvg0KN19SQFwtFvKKOnwJl3x3ZAEhjVQrdt0YOu0pMl", initheader = {'Content-Type' =>'application/json'})
    req.body = messageData.to_json
    
    res = https.request(req)
    puts "Response #{res.code} #{res.message}: #{res.body}"
  end
   
    def post_init (*args)
      @isclosed=0
      @techname = "anet_#{rand(99999)}"
      @name = "anet_#{rand(99999)}"
      puts "entrando:"+@techname.to_s
      #EchoServer.list.each{ |c| c.send_data "#{@name} has joined.\n" }
      EchoServer.list << self
      EchoServer.devicesLogged[@techname]="."
      send_data "TOKEN?\n"
      EchoServer.offlineTimer()

      micro_verification = EventMachine::Timer.new(15) do
        # puts "VERIFY_CONNECTION"
        EchoServer.list.each{ |c| 
          if EchoServer.devicesLogged[c.techname] != "."
            # then this device is online
            # verify log DB and update accordingly
            lastOnlineVerification = AnetLog.where(:arduino_id => EchoServer.devicesLogged[c.techname].id).order("created_at ASC").last
            if lastOnlineVerification != nil
              if lastOnlineVerification.connection_type == 0
                # if it was a failed log, then correct that log session
                lastOnlineVerification.connection_type = 1
                lastOnlineVerification.save
              end
            end
          end
        }
      end
      silent_disconnection = EventMachine::Timer.new(120) do
        # puts "FORCE_DISCONNECTION"
        # puts "disconnection>>"
        EchoServer.list.each{ |c|             
            if EchoServer.devicesLogged[c.techname] == "."
              # then this device never got connected
              # let's kill it
              # puts c.name
              puts ">>closing2"
              puts c.inspect
              puts "<<closing2"
              c.isclosed = 1
              c.close_connection
              # puts "......"
            end
        }
        # puts "disconnection<<"
      end
      
    end

    def unbind
      puts "\u001b[33munbinding >>>"
      puts EchoServer.devicesLogged[@techname].inspect
        @isclosed=1 
        if @anet_id!=nil and @anet_id!=1
          begin
            uri = URI.parse("http://localhost:3002/mail-notification") 
            header = {'Content-Type'=> 'text/json'}
            https = Net::HTTP.new(uri.host,uri.port)
            # https.use_ssl = true
            req = Net::HTTP::Post.new(uri)
            req['Content-Type'] = 'application/json'
            finalArrayEmail=[]
            Sys_User.where(:id=>EchoServer.devicesLogged[@techname].id_owner).each do |singleEmail|
              finalArrayEmail.push({"email": singleEmail.email, "name": "Dear #{singleEmail.name}"})
            end
            # The body needs to be a JSON string, use whatever you know to parse Hash to JSON
            req.body = {recipients:finalArrayEmail,template:"connection_v1",device:EchoServer.devicesLogged[@techname].object}.to_json
            # Send the request
            response = https.request(req)
            puts response.body
          rescue
  
          end
            AnetLog.new(:arduino_id=>@anet_id,:connection_type=>0).save
        end
      puts "salio:"+@name.to_s
      puts "\u001b[0m"

        begin
          if EchoServer.devicesLogged[@techname] != "."
          uri = URI.parse("http://localhost:3002/update-data") 
          header = {'Content-Type'=> 'text/json'}
          https = Net::HTTP.new(uri.host,uri.port)
          # https.use_ssl = true
          req = Net::HTTP::Post.new(uri)
          req['Content-Type'] = 'application/json'
          # The body needs to be a JSON string, use whatever you know to parse Hash to JSON
          req.body = {user_channel:@streaming_channel,token: EchoServer.devicesLogged[@techname].token,type:'status'}.to_json
          # Send the request
          response = https.request(req)
          puts response.body
          end
        rescue

        end

      begin
        if EchoServer.devicesLogged[@techname] != "."
          notificateOnChange(EchoServer.devicesLogged[@techname],"Device "+EchoServer.devicesLogged[@techname].object+" just disconnected at "+EchoServer.devicesLogged[@techname].location+"! :o -@suitch_")  
        end
      rescue Exception => e
        
      end

      
      
      EchoServer.list.delete self
      #EchoServer.list.each{ |c| c.send_data "#{@name} has left.\n" }
    end


    def receive_data data
      begin
        get_peername
      rescue
        # all of a sudden the device disconnected
        return
      end
      begin
        port2, ip2 = Socket.unpack_sockaddr_in(get_peername)
      rescue
        # all of a sudden the device disconnected
        return
      end
      port, ip = Socket.unpack_sockaddr_in(get_peername)
      # puts "got from #{ip}:#{port}"
      
      if (data=="\n" or data=="\r") and (@incomingString==nil or @incomingString=="")
          return
      end
      if @incomingString==nil
        @incomingString=""
      end
      if data.index("<<<<JPG\nJPG>>>>") != nil
        # divide string when the last part of the image comes
        # puts "incoming JPG-end-start\n"
        stringDivision = data.split("JPG>>>>\n")
        if @receiving_contents == nil
          @receiving_contents = stringDivision[0]
        else
          @receiving_contents = @receiving_contents + stringDivision[0]
        end
        if EchoServer.devicesLogged[@techname].token != nil and @receiving_img != nil
          open('/home/suitch/public/recordings/'+EchoServer.devicesLogged[@techname].token+'/'+@receiving_img+'.jpg', 'wb') do |file|
            file << @receiving_contents
          end
          # channels_client = Pusher::Client.new(
          #       app_id: '826341',
          #       key: 'a41aed5f65a8c69c5f0a',
          #       secret: 'd95849ca33e6467c5b0e',
          #       cluster: 'us2',
          #       encrypted: true
          #       )
                capture = SysVideo.new
                capture.filename = @receiving_img+'.jpg'
                capture.token = EchoServer.devicesLogged[@techname].token
                capture.save
            begin
              # channels_client.trigger(EchoServer.devicesLogged[@techname].token, 'rec-picture', {
              #     message: @receiving_img+'.jpg'
              # })
              uri = URI.parse("http://localhost:3002/camera-stream") 
              header = {'Content-Type'=> 'text/json'}
              https = Net::HTTP.new(uri.host,uri.port)
              # https.use_ssl = true
              req = Net::HTTP::Post.new(uri)
              req['Content-Type'] = 'application/json'
              # The body needs to be a JSON string, use whatever you know to parse Hash to JSON
              req.body = {user_channel:@streaming_channel,token: EchoServer.devicesLogged[@techname].token,file:@receiving_img+'.jpg'}.to_json
              # Send the request
              response = https.request(req)
              puts response.body

            rescue
            end
          end
        # we have another image starting
        @receiving_img = Time.now.to_i.to_s
        open('/home/suitch/public/recordings/'+EchoServer.devicesLogged[@techname].token+'/'+@receiving_img+'.jpg', 'wb') do |file|
          # file << open('http://'+singleDHCP.ip.to_s+':8000/jpg').read
        end
        @receiving_contents = stringDivision[1]
        return
      elsif data.index("<<<<JPG") != nil and data.index("JPG>>>>") != nil
        # puts "incoming full-JPG\n"
        # maybe the image was sent completely
        stringDivision = data.split("JPG>>>>\n")
        @receiving_contents = stringDivision[1]
        @receiving_img = Time.now.to_i.to_s
       
        begin
          open('/home/suitch/public/recordings/'+EchoServer.devicesLogged[@techname].token+'/'+@receiving_img+'.jpg', 'wb') do |file|
            file << @receiving_contents
          end
          # channels_client = Pusher::Client.new(
          #     app_id: '826341',
          #     key: 'a41aed5f65a8c69c5f0a',
          #     secret: 'd95849ca33e6467c5b0e',
          #     cluster: 'us2',
          #     encrypted: true
          #     )
          capture = SysVideo.new
          capture.filename = @receiving_img+'.jpg'
          capture.token = EchoServer.devicesLogged[@techname].token
          capture.save
          # channels_client.trigger(EchoServer.devicesLogged[@techname].token, 'rec-picture', {
          #     message: @receiving_img+'.jpg'
          # })
          uri = URI.parse("http://localhost:3002/camera-stream") 
          header = {'Content-Type'=> 'text/json'}
          https = Net::HTTP.new(uri.host,uri.port)
          # https.use_ssl = true
          req = Net::HTTP::Post.new(uri)
          req['Content-Type'] = 'application/json'
          # The body needs to be a JSON string, use whatever you know to parse Hash to JSON
          req.body = {user_channel:@streaming_channel,token: EchoServer.devicesLogged[@techname].token,file:@receiving_img+'.jpg'}.to_json
          # Send the request
          response = https.request(req)
          puts response.body
        rescue
        end
        @receiving_img = nil
        @receiving_contents = nil
        return
      elsif data.index("<<<<JPG") != nil
        if @receiving_contents == nil
          @receiving_contents = data
        else
          @receiving_contents = @receiving_contents + data
        end
        if @receiving_img != nil
          open('/home/suitch/public/recordings/'+EchoServer.devicesLogged[@techname].token+'/'+@receiving_img+'.jpg', 'wb') do |file|
            file << @receiving_contents
          end
          # channels_client = Pusher::Client.new(
          #     app_id: '826341',
          #     key: 'a41aed5f65a8c69c5f0a',
          #     secret: 'd95849ca33e6467c5b0e',
          #     cluster: 'us2',
          #     encrypted: true
          #     )
              capture = SysVideo.new
              capture.filename = @receiving_img+'.jpg'
              capture.token = EchoServer.devicesLogged[@techname].token
              capture.save
          begin
            uri = URI.parse("http://localhost:3002/camera-stream") 
            header = {'Content-Type'=> 'text/json'}
            https = Net::HTTP.new(uri.host,uri.port)
            # https.use_ssl = true
            req = Net::HTTP::Post.new(uri)
            req['Content-Type'] = 'application/json'
            # The body needs to be a JSON string, use whatever you know to parse Hash to JSON
            req.body = {user_channel:@streaming_channel,token: EchoServer.devicesLogged[@techname].token,file:@receiving_img+'.jpg'}.to_json
            # Send the request
            response = https.request(req)
            puts response.body
          rescue

          end
        end
        @receiving_img = nil
        @receiving_contents = nil
        # send_data "pic\n"
        return
      elsif @receiving_img != nil
        # tenemos un archivo abierto, seguir depositando datos
        # open('/home/suitch/public/recordings/'+EchoServer.devicesLogged[@techname].token+'/'+@receiving_img+'.jpg', 'wb') do |file|
        #   file << data
        # end
        if @receiving_contents == nil
          @receiving_contents = data
        else
          @receiving_contents = @receiving_contents + data
        end
        return
      elsif data.index("JPG>>>>") != nil
        # creating folder
        # Dir.mkdir('/home/suitch/public/recordings/'+EchoServer.devicesLogged[@techname].token) unless File.exists?('/home/suitch/public/recordings/'+EchoServer.devicesLogged[@techname].token)
        # puts "incoming JPG>>>>\n"
        stringDivision = data.split("JPG>>>>\n")
        if stringDivision[1] != nil && stringDivision[1] != ''
          if @receiving_contents == nil
            @receiving_contents = stringDivision[1]
          else
            @receiving_contents = @receiving_contents + stringDivision[1]
          end
        end
        # puts "<<<<incoming JPG\n"
        @receiving_img = Time.now.to_i.to_s
        open('/home/suitch/public/recordings/'+EchoServer.devicesLogged[@techname].token+'/'+@receiving_img+'.jpg', 'wb') do |file|
          # file << open('http://'+singleDHCP.ip.to_s+':8000/jpg').read
        end
        return
      elsif data.index("<<<END\n\r") != nil
        @receiving_file = false
        @incomingString=@incomingString+data
      elsif data.index("openFile=<<<BEGIN") != nil or @receiving_file == true
        # a file was going to be transmitted
        @incomingString=@incomingString+data
        @receiving_file = true
        return
      else 
        @incomingString=@incomingString+data
      end
      if @utimer!=nil
        @utimer.cancel  
      end
      puts "incoming data:"+data.inspect
      
      
      @utimer = EventMachine::Timer.new(60) do
        puts "TIMEOUT---->"+@name
        send_data "PONG\n\r"  
        # unbind
      end
      
      if @incomingString.index("\n")!=nil or @incomingString.index("\r")!=nil
          # fix para lectores nfc
          # incoming data:"Read fa"
          # incoming data:"iled 6\r\nnfcreadr CD 91 4C 78#.......................#10\r\n"
          # EMPIEZA:"Read failed 6nfcreadr CD 91 4C 78#.......................#10"
          # [#<Sys_User_Phone id: 63, sys_user: 11, sys_phone_type: 1, number: "4448573200", removed: false>, #<Sys_User_Phone id: 79, sys_user: 11, sys_phone_type: 1, number: "4448573202", removed: false>]
          # #<Jarvix_Custom_Property id: 64, created_at: "2013-09-30 14:08:46", updated_at: "2013-09-30 14:08:46", property_name: "number", property_value: "4448573202", is_enabled: nil, id_user: 25, id_custom_object: 29>
          # ["ed 6nfcreadr CD 91 4C 78", ".......................", "10"]
          # THIS LECTOR:257f5204

          # incoming data:"R"
          # incoming data:"ead failed 11\r\nnfcreadr 04 C9 79 5A 54 3C 80#io.dartl....Y#10\r\n"
          # EMPIEZA:"Read failed 11nfcreadr 04 C9 79 5A 54 3C 80#io.dartl....Y#10"
          # [#<Sys_User_Phone id: 63, sys_user: 11, sys_phone_type: 1, number: "4448573200", removed: false>, #<Sys_User_Phone id: 79, sys_user: 11, sys_phone_type: 1, number: "4448573202", removed: false>]
          # #<Jarvix_Custom_Property id: 64, created_at: "2013-09-30 14:08:46", updated_at: "2013-09-30 14:08:46", property_name: "number", property_value: "4448573202", is_enabled: nil, id_user: 25, id_custom_object: 29>

          # incoming data:"\n"
          # incoming data:"Error"
          # incoming data:". Failed read page 4\r\nnfcreadr 04 C9 79 5A 54 3C 80##10\r\n"
          # EMPIEZA:"Error. Failed read page 4nfcreadr 04 C9 79 5A 54 3C 80##10"
          if @incomingString.index("Read failed ")!=nil
            @incomingString=@incomingString.delete "Read failed "
          end
          if @incomingString.index("Error. Failed read page ")!=nil
            @incomingString=@incomingString.delete "Error. Failed read page "
          end
          #data=data[0..-2].downcase
          @incomingStringCopy = @incomingString
          @incomingString=@incomingString.delete "\n"
          @incomingString=@incomingString.delete "\r"

      else
       return

      end
      puts "EMPIEZA:"+@incomingString.inspect
      # if data.index("\r")!=nil
      #     #data=data[0..-2].downcase
      #     data=data[0..-2]
      # end
      if EchoServer.devicesLogged[@techname]=="."
        #esta. recibiendo token
        #buscar si existe el dispositivo
        
        device=@incomingString.split("/")
        if device.length==2 
          puts "device[0]-->"
          puts device[0]
          puts "device[1]-->"
          puts device[1]
          aDevice=AnetDevice.where(:token=>device[0],:secret_token=>device[1]).last
          # puts aDevice.inspect
          if aDevice!=nil
              #buscar si ya esta loggeado, en ese caso desloggear
              # no puede haber varias conexiones con el mismo token
            alreadyLogged=false
            EchoServer.list.each{ |c| 
                if c.name == "anet_"+device[0] and c.isclosed==0
                  puts ">>closing_clone"
                  puts aDevice.inspect
                  puts "<<closing_clone"
                    c.isclosed=1
                    c.close_connection
                end
            }
            if alreadyLogged==false
                @name = "anet_"+device[0]
                EchoServer.devicesLogged[@techname]=aDevice
                @anet_id=aDevice.id
                if @anet_id!=1
                   port, ip = Socket.unpack_sockaddr_in(get_peername)
                   # detect when we are logging a camera to create the containing folder
                  end
                if aDevice.version == "camv1"
                  Dir.mkdir('/home/suitch/public/recordings/'+EchoServer.devicesLogged[@techname].token) unless File.exists?('/home/suitch/public/recordings/'+EchoServer.devicesLogged[@techname].token)
                end
                
                if aDevice.id_owner==nil
                  AnetLog.new(:arduino_id=>aDevice.id,:connection_type=>1,:created_at=>DateTime.now,:mip=>ip).save
                  # lets look for the device itself and update the latest IP that got connected
                  aDevice.mip = ip
                  aDevice.save
                  puts "WLCMEX"
                  send_data "WLCMEX\n"  
                else
                  AnetLog.new(:arduino_id=>aDevice.id,:connection_type=>1,:created_at=>DateTime.now,:mip=>ip).save
                  # lets look for the device itself and update the latest IP that got connected
                  aDevice.mip = ip
                  aDevice.save
                  # if the device has owner, then get the owner channel
                  prechannel=Sys_User.where(:id=>aDevice.id_owner).last
                  if prechannel!=nil
                    @streaming_channel=prechannel.user_id
                  end
                  begin
                    uri = URI.parse("http://localhost:3002/update-data") 
                    header = {'Content-Type'=> 'text/json'}
                    https = Net::HTTP.new(uri.host,uri.port)
                    # https.use_ssl = true
                    req = Net::HTTP::Post.new(uri)
                    req['Content-Type'] = 'application/json'
                    # The body needs to be a JSON string, use whatever you know to parse Hash to JSON
                    req.body = {user_channel:@streaming_channel,token: EchoServer.devicesLogged[@techname].token,type:'status'}.to_json
                    # Send the request
                    response = https.request(req)
                    puts response.body
                  rescue
        
                  end
                  puts "WLCME"
                  send_data "WLCME\n"
                end
               
                
                @incomingString=""
                begin
                  notificateOnChange(aDevice,"The network says that a Device "+aDevice.object+" connected at "+aDevice.location+"! :) -@suitch_")  
                rescue Exception => e
                  puts "ALGUN ERROR SUCEDIO"
                end
                
            else 
              puts "BUSY"
                send_data "BUSY\n"
                puts ">>closing-busy"
                close_connection
            end
          else
            puts "USR_ERROR"
            send_data "USR_ERROR\n"
            @incomingString=""
            # close_connection
          end  
        else

          last_detection=AnetLog.where(:arduino_id=>@anet_id).last
          if last_detection!= nil && last_detection.connection_type == 0
            # it was marked as disconnected, lets push a connection state
            AnetLog.new(:arduino_id=>@anet_id,:connection_type=>1).save
          end
          @ping = Time.now
          # if we get any information from the device and the last status was a disconnection,
          # lets -reconnect-
          # antes de marcarlo como erroneo
          if @incomingString.index("Lorem amp ipsum dolor sit amet")!=nil
            port, ip = Socket.unpack_sockaddr_in(get_peername)
            # suitch inicia el intercambio de generacion de token
            @anet_device = AnetDevice.new
            @anet_device.description="Suitch device v4"
            @anet_device.location="home"
            @anet_device.object="light"
            #creamos un token y un token secret
            # @anet_device.id_owner=session[:activeUser].id
            @anet_device.token=UUIDTools::UUID.random_create.to_s()
            @anet_device.mip=ip
            @anet_device.token=@anet_device.token[0..7]
            @anet_device.secret_token=UUIDTools::UUID.random_create.to_s()
            @anet_device.secret_token=@anet_device.secret_token[0..7]
            @anet_device.is_public=0
            @anet_device.save

            comandon=AnetCommand.new
            comandon.arduino_id=@anet_device.id
            comandon.command="Turn on"
            comandon.command_data="on"
            comandon.is_only_question=1
            comandon.save
            comandon2=AnetCommand.new
            comandon2.arduino_id=@anet_device.id
            comandon2.command="Turn off"
            comandon2.command_data="off"
            comandon2.is_only_question=1
            comandon2.save
            comandon3=AnetCommand.new
            comandon3.arduino_id=@anet_device.id
            comandon3.command="File browser"
            comandon3.command_data="fileBrowser"
            comandon3.is_only_question=1
            comandon3.save
            comandon4=AnetCommand.new
            comandon4.arduino_id=@anet_device.id
            comandon4.command="Open file"
            comandon4.command_data="openFile"
            comandon4.is_only_question=1
            comandon4.save
            comandon5=AnetCommand.new
            comandon5.arduino_id=@anet_device.id
            comandon5.command="Save file"
            comandon5.command_data="saveFile"
            comandon5.save
            comandon6=AnetCommand.new
            comandon6.arduino_id=@anet_device.id
            comandon6.command="Power"
            comandon6.command_data="pow"
            comandon6.is_only_question=0
            comandon6.save
            puts "DEVICE CREATED"
            @incomingString=""
            send_data @anet_device.token+"/"+@anet_device.secret_token
          elsif @incomingString.index("4940ab80-2cc4-11ee")!=nil
            port, ip = Socket.unpack_sockaddr_in(get_peername)
            # suitch inicia el intercambio de generacion de token
            @anet_device = AnetDevice.new
            @anet_device.description="Suitch Enermon"
            @anet_device.location="home"
            @anet_device.object="Energy monitor"
            @anet_device.version="enermonv1"
            #creamos un token y un token secret
            # @anet_device.id_owner=session[:activeUser].id
            @anet_device.token=UUIDTools::UUID.random_create.to_s()
            @anet_device.mip=ip
            @anet_device.token=@anet_device.token[0..7]
            @anet_device.secret_token=UUIDTools::UUID.random_create.to_s()
            @anet_device.secret_token=@anet_device.secret_token[0..7]
            @anet_device.is_public=0
            @anet_device.save

            # comandon=AnetCommand.new
            # comandon.arduino_id=@anet_device.id
            # comandon.command="Turn on"
            # comandon.command_data="on"
            # comandon.is_only_question=1
            # comandon.save
            # comandon2=AnetCommand.new
            # comandon2.arduino_id=@anet_device.id
            # comandon2.command="Turn off"
            # comandon2.command_data="off"
            # comandon2.is_only_question=1
            # comandon2.save
            # comandon3=AnetCommand.new
            # comandon3.arduino_id=@anet_device.id
            # comandon3.command="File browser"
            # comandon3.command_data="fileBrowser"
            # comandon3.is_only_question=1
            # comandon3.save
            # comandon4=AnetCommand.new
            # comandon4.arduino_id=@anet_device.id
            # comandon4.command="Open file"
            # comandon4.command_data="openFile"
            # comandon4.is_only_question=1
            # comandon4.save
            # comandon5=AnetCommand.new
            # comandon5.arduino_id=@anet_device.id
            # comandon5.command="Save file"
            # comandon5.command_data="saveFile"
            # comandon5.save
            comandon6=AnetCommand.new
            comandon6.arduino_id=@anet_device.id
            comandon6.command="Power"
            comandon6.command_data="pow"
            comandon6.is_only_question=0
            comandon6.unit="Watts"
            comandon6.save
            puts "DEVICE CREATED"
            @incomingString=""
            send_data @anet_device.token+"/"+@anet_device.secret_token
        elsif @incomingString.index("Lorem ipsum dolor sit amet")!=nil
          port, ip = Socket.unpack_sockaddr_in(get_peername)
          # suitch inicia el intercambio de generacion de token
          @anet_device = AnetDevice.new
          @anet_device.description="Suitch device v1"
          @anet_device.location="home"
          @anet_device.object="light"
          #creamos un token y un token secret
          # @anet_device.id_owner=session[:activeUser].id
          @anet_device.token=UUIDTools::UUID.random_create.to_s()
          @anet_device.mip=ip
          @anet_device.token=@anet_device.token[0..7]
          @anet_device.secret_token=UUIDTools::UUID.random_create.to_s()
          @anet_device.secret_token=@anet_device.secret_token[0..7]
          @anet_device.is_public=0
          @anet_device.save

          comandon=AnetCommand.new
          comandon.arduino_id=@anet_device.id
          comandon.command="Turn on"
          comandon.command_data="SUON"
          comandon.save
          comandon2=AnetCommand.new
          comandon2.arduino_id=@anet_device.id
          comandon2.command="Turn off"
          comandon2.command_data="SUOFF"
          comandon2.save
          comandon3=AnetCommand.new
          comandon3.arduino_id=@anet_device.id
          comandon3.command="File browser"
          comandon3.command_data="fileBrowser"
          comandon3.save
          comandon4=AnetCommand.new
          comandon4.arduino_id=@anet_device.id
          comandon4.command="Open file"
          comandon4.command_data="openFile"
          comandon4.save
          comandon5=AnetCommand.new
          comandon5.arduino_id=@anet_device.id
          comandon5.command="Save file"
          comandon5.command_data="saveFile"
          comandon5.save
          puts "DEVICE CREATED"
          @incomingString=""
          send_data @anet_device.token+"/"+@anet_device.secret_token
        elsif @incomingString.index("esp ipsum dolor sit amet")!=nil
          # this device represents the HOMEKIT accesories WITH temperature
            port, ip = Socket.unpack_sockaddr_in(get_peername)
            # suitch inicia el intercambio de generacion de token
            @anet_device = AnetDevice.new
            @anet_device.description="Suitch device v3 IR"
            @anet_device.location="home"
            @anet_device.object="IR"
            #creamos un token y un token secret
            # @anet_device.id_owner=session[:activeUser].id
            @anet_device.token=UUIDTools::UUID.random_create.to_s()
            @anet_device.mip=ip
            @anet_device.token=@anet_device.token[0..7]
            @anet_device.secret_token=UUIDTools::UUID.random_create.to_s()
            @anet_device.secret_token=@anet_device.secret_token[0..7]
            @anet_device.is_public=0
            @anet_device.save

            comandon=AnetCommand.new
            comandon.arduino_id=@anet_device.id
            comandon.command="Turn on"
            comandon.command_data="on"
            comandon.save
            comandon2=AnetCommand.new
            comandon2.arduino_id=@anet_device.id
            comandon2.command="Turn off"
            comandon2.command_data="off"
            comandon2.save
            comandon3=AnetCommand.new
            comandon3.arduino_id=@anet_device.id
            comandon3.command="Temperature"
            comandon3.command_data="temp"
            comandon3.unit="°C"
            comandon3.save
            comandon4=AnetCommand.new
            comandon4.arduino_id=@anet_device.id
            comandon4.command="Slot 1"
            comandon4.command_data="c1"
            comandon4.save
            comandon5=AnetCommand.new
            comandon5.arduino_id=@anet_device.id
            comandon5.command="Slot 2"
            comandon5.command_data="c2"
            comandon5.save
            puts "DEVICE CREATED"
            @incomingString=""
            send_data @anet_device.token+"/"+@anet_device.secret_token+"\n"
          elsif @incomingString.index("esp simple ipsum dolor sit amet")!=nil
            # this device represents the HOMEKIT accesories WITH temperature
              port, ip = Socket.unpack_sockaddr_in(get_peername)
              # suitch inicia el intercambio de generacion de token
              @anet_device = AnetDevice.new
              @anet_device.description="Suitch basic"
              @anet_device.location="home"
              @anet_device.object="simple"
              #creamos un token y un token secret
              # @anet_device.id_owner=session[:activeUser].id
              @anet_device.token=UUIDTools::UUID.random_create.to_s()
              @anet_device.mip=ip
              @anet_device.token=@anet_device.token[0..7]
              @anet_device.secret_token=UUIDTools::UUID.random_create.to_s()
              @anet_device.secret_token=@anet_device.secret_token[0..7]
              @anet_device.is_public=0
              @anet_device.save
  
              # comandon=AnetCommand.new
              # comandon.arduino_id=@anet_device.id
              # comandon.command="Turn on"
              # comandon.command_data="on"
              # comandon.save
              # comandon2=AnetCommand.new
              # comandon2.arduino_id=@anet_device.id
              # comandon2.command="Turn off"
              # comandon2.command_data="off"
              # comandon2.save
              # comandon3=AnetCommand.new
              # comandon3.arduino_id=@anet_device.id
              # comandon3.command="Temperature"
              # comandon3.command_data="temp"
              # comandon3.save
              # comandon4=AnetCommand.new
              # comandon4.arduino_id=@anet_device.id
              # comandon4.command="Open file"
              # comandon4.command_data="openFile"
              # comandon4.save
              # comandon5=AnetCommand.new
              # comandon5.arduino_id=@anet_device.id
              # comandon5.command="Save file"
              # comandon5.command_data="saveFile"
              # comandon5.save
              puts "DEVICE CREATED"
              @incomingString=""
              send_data @anet_device.token+"/"+@anet_device.secret_token+"\n"
          elsif @incomingString.index("3f744d56ebc3fa5267b1501be924e9f7")!=nil
                port, ip = Socket.unpack_sockaddr_in(get_peername)
                # suitch inicia el intercambio de generacion de token
                @anet_device = AnetDevice.new
                @anet_device.description="Suitch stone"
                @anet_device.location="home"
                @anet_device.object="bulb"
                #creamos un token y un token secret
                # @anet_device.id_owner=session[:activeUser].id
                @anet_device.token=UUIDTools::UUID.random_create.to_s()
                @anet_device.mip=ip
                @anet_device.token=@anet_device.token[0..7]
                @anet_device.secret_token=UUIDTools::UUID.random_create.to_s()
                @anet_device.secret_token=@anet_device.secret_token[0..7]
                @anet_device.is_public=0
                @anet_device.save
    
                comandon=AnetCommand.new
                comandon.arduino_id=@anet_device.id
                comandon.command="Turn on"
                comandon.command_data="on"
                comandon.is_only_question=1
                comandon.save
                comandon2=AnetCommand.new
                comandon2.arduino_id=@anet_device.id
                comandon2.command="Turn off"
                comandon2.command_data="off"
                comandon2.is_only_question=1
                comandon2.save
                comandon3=AnetCommand.new
                comandon3.arduino_id=@anet_device.id
                comandon3.command="File browser"
                comandon3.command_data="fileBrowser"
                comandon3.is_only_question=1
                comandon3.save
                comandon4=AnetCommand.new
                comandon4.arduino_id=@anet_device.id
                comandon4.command="Open file"
                comandon4.command_data="openFile"
                comandon4.is_only_question=1
                comandon4.save
                comandon5=AnetCommand.new
                comandon5.arduino_id=@anet_device.id
                comandon5.command="Save file"
                comandon5.command_data="saveFile"
                comandon5.is_only_question=1
                comandon5.save
                comandon6=AnetCommand.new
                comandon6.arduino_id=@anet_device.id
                comandon6.command="Power"
                comandon6.command_data="pow"
                comandon6.is_only_question=0
                comandon6.save
                puts "DEVICE CREATED"
                @incomingString=""
                send_data @anet_device.token+"/"+@anet_device.secret_token      
            elsif @incomingString.index("212664f3163076893cb0a1055af4")!=nil
              # this device represents the HOMEKIT accesories WITH temperature
                port, ip = Socket.unpack_sockaddr_in(get_peername)
                # suitch inicia el intercambio de generacion de token
                @anet_device = AnetDevice.new
                @anet_device.description="Suitch Sfir"
                @anet_device.location="home"
                @anet_device.object="Wall switch"
                @anet_device.version="sfir"
                #creamos un token y un token secret
                # @anet_device.id_owner=session[:activeUser].id
                @anet_device.token=UUIDTools::UUID.random_create.to_s()
                @anet_device.mip=ip
                @anet_device.token=@anet_device.token[0..7]
                @anet_device.secret_token=UUIDTools::UUID.random_create.to_s()
                @anet_device.secret_token=@anet_device.secret_token[0..7]
                @anet_device.is_public=0
                @anet_device.save
    
                comandon=AnetCommand.new
                comandon.arduino_id=@anet_device.id
                comandon.command="Turn on"
                comandon.command_data="on"
                comandon.is_only_question=1
                comandon.save
                comandon2=AnetCommand.new
                comandon2.arduino_id=@anet_device.id
                comandon2.command="Turn off"
                comandon2.command_data="off"
                comandon2.is_only_question=1
                comandon2.save
                comandon3=AnetCommand.new
                comandon3.arduino_id=@anet_device.id
                comandon3.command="Button 1"
                comandon3.command_data="bt1"
                comandon3.is_only_question=1
                comandon3.save
                comandon4=AnetCommand.new
                comandon4.arduino_id=@anet_device.id
                comandon4.command="Button 2"
                comandon4.command_data="bt2"
                comandon4.is_only_question=1
                comandon4.save
                comandon5=AnetCommand.new
                comandon5.arduino_id=@anet_device.id
                comandon5.command="Button 3"
                comandon5.command_data="bt3"
                comandon5.is_only_question=1
                comandon5.save
                comandon6=AnetCommand.new
                comandon6.arduino_id=@anet_device.id
                comandon6.command="Sync screensaver"
                comandon6.command_data="sync_screen"
                comandon6.is_only_question=1
                comandon6.save
                puts "DEVICE CREATED"
                @incomingString=""
                send_data @anet_device.token+"/"+@anet_device.secret_token+"\n"
            elsif @incomingString.index("0205EACC79BAF77A")!=nil
              # this device represents the HOMEKIT accesories WITH temperature
                port, ip = Socket.unpack_sockaddr_in(get_peername)
                # suitch inicia el intercambio de generacion de token
                @anet_device = AnetDevice.new
                @anet_device.description="Suitch Doorlock"
                @anet_device.location="home"
                @anet_device.object="lock"
                @anet_device.version="doorv1"
                #creamos un token y un token secret
                # @anet_device.id_owner=session[:activeUser].id
                @anet_device.token=UUIDTools::UUID.random_create.to_s()
                @anet_device.mip=ip
                @anet_device.token=@anet_device.token[0..7]
                @anet_device.secret_token=UUIDTools::UUID.random_create.to_s()
                @anet_device.secret_token=@anet_device.secret_token[0..7]
                @anet_device.is_public=0
                @anet_device.save
    
                comandon=AnetCommand.new
                comandon.arduino_id=@anet_device.id
                comandon.command="Open"
                comandon.command_data="unlock"
                comandon.is_only_question=1
                comandon.save
                comandon2=AnetCommand.new
                comandon2.arduino_id=@anet_device.id
                comandon2.command="Close"
                comandon2.command_data="lock"
                comandon2.is_only_question=1
                comandon2.save
                puts "DEVICE CREATED"
                @incomingString=""
                send_data @anet_device.token+"/"+@anet_device.secret_token+"\n"
            elsif @incomingString.index("49D39F80FAE8CC597")!=nil
              # this device represents the HOMEKIT accesories WITH temperature
                port, ip = Socket.unpack_sockaddr_in(get_peername)
                # suitch inicia el intercambio de generacion de token
                @anet_device = AnetDevice.new
                @anet_device.description="Suitch Thermostat"
                @anet_device.location="home"
                @anet_device.object="Thermostat"
                @anet_device.version="thermostatv1"
                #creamos un token y un token secret
                # @anet_device.id_owner=session[:activeUser].id
                @anet_device.token=UUIDTools::UUID.random_create.to_s()
                @anet_device.mip=ip
                @anet_device.token=@anet_device.token[0..7]
                @anet_device.secret_token=UUIDTools::UUID.random_create.to_s()
                @anet_device.secret_token=@anet_device.secret_token[0..7]
                @anet_device.is_public=0
                @anet_device.save
    
                comandon=AnetCommand.new
                comandon.arduino_id=@anet_device.id
                comandon.command="Cool"
                comandon.command_data="cool"
                comandon.is_only_question=1
                comandon.save
                comandon2=AnetCommand.new
                comandon2.arduino_id=@anet_device.id
                comandon2.command="Heat"
                comandon2.command_data="heat"
                comandon2.is_only_question=1
                comandon2.save
                comandon3=AnetCommand.new
                comandon3.arduino_id=@anet_device.id
                comandon3.command="Turn off"
                comandon3.command_data="off"
                comandon3.is_only_question=1
                comandon3.save
                comandon4=AnetCommand.new
                comandon4.arduino_id=@anet_device.id
                comandon4.command="Temperature"
                comandon4.command_data="temp"
                comandon4.is_only_question=0
                comandon4.unit="°C"
                comandon4.save
                comandon5=AnetCommand.new
                comandon5.arduino_id=@anet_device.id
                comandon5.command="Target temperature"
                comandon5.command_data="set_temp"
                comandon5.is_only_question=0
                comandon5.unit="°C"
                comandon5.save
                comandon6=AnetCommand.new
                comandon6.arduino_id=@anet_device.id
                comandon6.command="Mode"
                comandon6.command_data="mode"
                comandon6.is_only_question=0
                comandon6.save
                puts "DEVICE CREATED"
                @incomingString=""
                send_data @anet_device.token+"/"+@anet_device.secret_token+"\n"
        elsif @incomingString.index("6782c21de")!=nil
          # this device represents the HOMEKIT accesories WITH temperature
            port, ip = Socket.unpack_sockaddr_in(get_peername)
            # suitch inicia el intercambio de generacion de token
            @anet_device = AnetDevice.new
            @anet_device.description="Suitch Plug & hub"
            @anet_device.location="home"
            @anet_device.object="Suitch Plug & hub"
            @anet_device.version="hubv1"
            #creamos un token y un token secret
            # @anet_device.id_owner=session[:activeUser].id
            @anet_device.token=UUIDTools::UUID.random_create.to_s()
            @anet_device.mip=ip
            @anet_device.token=@anet_device.token[0..7]
            @anet_device.secret_token=UUIDTools::UUID.random_create.to_s()
            @anet_device.secret_token=@anet_device.secret_token[0..7]
            @anet_device.is_public=0
            @anet_device.save

            comandon=AnetCommand.new
            comandon.arduino_id=@anet_device.id
            comandon.command="Turn On"
            comandon.command_data="on"
            comandon.is_only_question=1
            comandon.save
            comandon2=AnetCommand.new
            comandon2.arduino_id=@anet_device.id
            comandon2.command="Turn Off"
            comandon2.command_data="off"
            comandon2.is_only_question=1
            comandon2.save
            comandon3=AnetCommand.new
            comandon3.arduino_id=@anet_device.id
            comandon3.command="List peers"
            comandon3.command_data="ls"
            comandon3.is_only_question=1
            comandon3.save
            comandon4=AnetCommand.new
            comandon4.arduino_id=@anet_device.id
            comandon4.command="Add peer"
            comandon4.command_data="mk"
            comandon4.is_only_question=1
            comandon4.save
            comandon5=AnetCommand.new
            comandon5.arduino_id=@anet_device.id
            comandon5.command="Remove peer"
            comandon5.command_data="rm"
            comandon5.is_only_question=1
            comandon5.save
            puts "DEVICE CREATED"
            @incomingString=""
            send_data @anet_device.token+"/"+@anet_device.secret_token+"\n"
          elsif @incomingString.index("995f5f38cd15f6a80")!=nil
            # this device represents the HOMEKIT accesories WITH temperature
              port, ip = Socket.unpack_sockaddr_in(get_peername)
              # suitch inicia el intercambio de generacion de token
              @anet_device = AnetDevice.new
              @anet_device.description="Suitch Seismograph"
              @anet_device.location="home"
              @anet_device.object="Seismograph"
              @anet_device.version="seismov1"
              #creamos un token y un token secret
              # @anet_device.id_owner=session[:activeUser].id
              @anet_device.token=UUIDTools::UUID.random_create.to_s()
              @anet_device.mip=ip
              @anet_device.token=@anet_device.token[0..7]
              @anet_device.secret_token=UUIDTools::UUID.random_create.to_s()
              @anet_device.secret_token=@anet_device.secret_token[0..7]
              @anet_device.is_public=0
              @anet_device.save
  
              comandon=AnetCommand.new
              comandon.arduino_id=@anet_device.id
              comandon.command="Temperature"
              comandon.command_data="temp"
              comandon.unit="°C"
              comandon.is_only_question=0
              comandon.save
              comandon2=AnetCommand.new
              comandon2.arduino_id=@anet_device.id
              comandon2.command="Humidity"
              comandon2.command_data="humi"
              comandon2.unit="%"
              comandon2.is_only_question=0
              comandon2.save
              comandon3=AnetCommand.new
              comandon3.arduino_id=@anet_device.id
              comandon3.command="Pressure"
              comandon3.command_data="press"
              comandon3.is_only_question=0
              comandon3.unit="Pa"
              comandon3.save
              comandon4=AnetCommand.new
              comandon4.arduino_id=@anet_device.id
              comandon4.command="Magnetometer"
              comandon4.command_data="gauss"
              comandon4.unit="gauss"
              comandon4.is_only_question=0
              comandon4.save
              comandon5=AnetCommand.new
              comandon5.arduino_id=@anet_device.id
              comandon5.command="Accelaration"
              comandon5.command_data="accel"
              comandon5.is_only_question=0
              comandon5.unit="g"
              comandon5.save
              comandon6=AnetCommand.new
              comandon6.arduino_id=@anet_device.id
              comandon6.command="Gyroscope"
              comandon6.command_data="gyro"
              comandon6.is_only_question=0
              comandon6.unit="mV/deg/s"
              comandon6.save
              puts "DEVICE CREATED"
              @incomingString=""
              send_data @anet_device.token+"/"+@anet_device.secret_token+"\n"
            elsif @incomingString.index("DF894E463F0B7672F4F")!=nil
              # this device represents the HOMEKIT accesories WITH temperature
                port, ip = Socket.unpack_sockaddr_in(get_peername)
                # suitch inicia el intercambio de generacion de token
                @anet_device = AnetDevice.new
                @anet_device.description="Suitch plug"
                @anet_device.location="home"
                @anet_device.object="Plug"
                @anet_device.version="plugv1"
                #creamos un token y un token secret
                # @anet_device.id_owner=session[:activeUser].id
                @anet_device.token=UUIDTools::UUID.random_create.to_s()
                @anet_device.mip=ip
                @anet_device.token=@anet_device.token[0..7]
                @anet_device.secret_token=UUIDTools::UUID.random_create.to_s()
                @anet_device.secret_token=@anet_device.secret_token[0..7]
                @anet_device.is_public=0
                @anet_device.save
    
                comandon=AnetCommand.new
                comandon.arduino_id=@anet_device.id
                comandon.command="Turn on"
                comandon.command_data="on"
                comandon.is_only_question=1
                comandon.save
                comandon2=AnetCommand.new
                comandon2.arduino_id=@anet_device.id
                comandon2.command="Turn off"
                comandon2.command_data="off"
                comandon2.is_only_question=1
                comandon2.save
                @incomingString=""
                send_data @anet_device.token+"/"+@anet_device.secret_token+"\n"
              elsif @incomingString.index("fe52b6a7c04254e9d06c6")!=nil
                # this device represents the HOMEKIT accesories WITH temperature
                  port, ip = Socket.unpack_sockaddr_in(get_peername)
                  # suitch inicia el intercambio de generacion de token
                  @anet_device = AnetDevice.new
                  @anet_device.description="Suitch Welcome"
                  @anet_device.location="home"
                  @anet_device.object="Welcome screeen"
                  @anet_device.version="welcommev1"
                  #creamos un token y un token secret
                  # @anet_device.id_owner=session[:activeUser].id
                  @anet_device.token=UUIDTools::UUID.random_create.to_s()
                  @anet_device.mip=ip
                  @anet_device.token=@anet_device.token[0..7]
                  @anet_device.secret_token=UUIDTools::UUID.random_create.to_s()
                  @anet_device.secret_token=@anet_device.secret_token[0..7]
                  @anet_device.is_public=0
                  @anet_device.save
      
                  comandon=AnetCommand.new
                  comandon.arduino_id=@anet_device.id
                  comandon.command="Show number"
                  comandon.command_data="num"
                  comandon.is_only_question=1
                  comandon.save
                  comandon2=AnetCommand.new
                  comandon2.arduino_id=@anet_device.id
                  comandon2.command="Show image"
                  comandon2.command_data="img"
                  comandon2.is_only_question=1
                  comandon2.save
                  @incomingString=""
                  send_data @anet_device.token+"/"+@anet_device.secret_token+"\n"
                elsif @incomingString.index("228cd211bfc7c2056d")!=nil
                  # this device represents the HOMEKIT accesories WITH temperature
                    port, ip = Socket.unpack_sockaddr_in(get_peername)
                    # suitch inicia el intercambio de generacion de token
                    @anet_device = AnetDevice.new
                    @anet_device.description="Suitch Camera"
                    @anet_device.location="home"
                    @anet_device.object="Camera"
                    @anet_device.version="camv1"
                    #creamos un token y un token secret
                    # @anet_device.id_owner=session[:activeUser].id
                    @anet_device.token=UUIDTools::UUID.random_create.to_s()
                    @anet_device.mip=ip
                    @anet_device.token=@anet_device.token[0..7]
                    @anet_device.secret_token=UUIDTools::UUID.random_create.to_s()
                    @anet_device.secret_token=@anet_device.secret_token[0..7]
                    @anet_device.is_public=0
                    @anet_device.save
        
                    comandon=AnetCommand.new
                    comandon.arduino_id=@anet_device.id
                    comandon.command="Light on"
                    comandon.command_data="on"
                    comandon.is_only_question=1
                    comandon.save
                    comandon2=AnetCommand.new
                    comandon2.arduino_id=@anet_device.id
                    comandon2.command="Light off"
                    comandon2.command_data="off"
                    comandon2.is_only_question=1
                    comandon2.save
                    comandon3=AnetCommand.new
                    comandon3.arduino_id=@anet_device.id
                    comandon3.command="Reboot"
                    comandon3.command_data="reboot"
                    comandon3.is_only_question=1
                    comandon3.save
                    comandon4=AnetCommand.new
                    comandon4.arduino_id=@anet_device.id
                    comandon4.command="Capture"
                    comandon4.command_data="pic"
                    comandon4.is_only_question=1
                    comandon4.save
                    @incomingString=""
                    send_data @anet_device.token+"/"+@anet_device.secret_token+"\n" 
            else
            puts "TOKEN_ERROR"
            send_data "TOKEN_ERROR\n"
            @incomingString=""
            close_connection
          end
        end
      else
        #aqui hay que poner los comandos
        #fbwall
        #inboxfb
        #mail
        #evernote
        #twitter
        #val=algo
        #creamos un objeto generico para la interface con lineaccess
        accessLine=ILineAccess.new(lineAccessToJarvix(EchoServer.devicesLogged[@techname].id_owner))
        if @incomingString.index("burst::")==nil and @incomingString.index("=")!=nil and (@incomingString.index("==")==nil or @incomingString.index("<<<BEGIN")!=nil) and @incomingString.index("webhook")==nil and @incomingString.index("hotspot")==nil and (@incomingString.index("sendto") == nil or @incomingString.index("sendto") > 5)
          saveAsValue=@incomingString.split("=")
          #buscar si existe el comando, sino, esta enviando pura shit
          incomingCommand=AnetCommand.where(:arduino_id=>EchoServer.devicesLogged[@techname].id,:command_data=>saveAsValue[0]).last
          puts "incomingCommand:"+incomingCommand.inspect
          if incomingCommand!=nil
            if is_numeric(saveAsValue[1]) or incomingCommand.is_only_question==0
                valorNuevo=AnetValue.new(:arduino_id=>EchoServer.devicesLogged[@techname].id,:command_id=>incomingCommand.id,:value=>saveAsValue[1])
                valorNuevo.save
                begin
                  uri = URI.parse("http://localhost:3002/update-data")
                  header = {'Content-Type'=> 'text/json'}
                  https = Net::HTTP.new(uri.host,uri.port)
                  # https.use_ssl = true
                  req = Net::HTTP::Post.new(uri)
                  req['Content-Type'] = 'application/json'
                  # The body needs to be a JSON string, use whatever you know to parse Hash to JSON
                  req.body = {user_channel:@streaming_channel,token: EchoServer.devicesLogged[@techname].token,type:'prop',id:incomingCommand.id,value:saveAsValue[1]}.to_json
                  # Send the request
                  response = https.request(req)
                  puts response.body
                rescue
      
                end
            else 
              new_activity = SystemActivity.new
              new_activity.device_token = EchoServer.devicesLogged[@techname].token
              new_activity.virtual_assistant = 0
              new_activity.command = saveAsValue[0]
              # generalLogs = Sys_Phrase.new(:phone=>EchoServer.devicesLogged[@techname].token,:interface_executed=>11,:callid=>UUIDTools::UUID.random_create.to_s())
              # generalLogs.save
              # generalLogs.phrase=saveAsValue[0]
              new_activity.save
              begin
                uri = URI.parse("http://localhost:3002/update-data") 
                header = {'Content-Type'=> 'text/json'}
                https = Net::HTTP.new(uri.host,uri.port)
                # https.use_ssl = true
                req = Net::HTTP::Post.new(uri)
                req['Content-Type'] = 'application/json'
                # The body needs to be a JSON string, use whatever you know to parse Hash to JSON
                req.body = {user_channel:@streaming_channel,token: EchoServer.devicesLogged[@techname].token,type:'prop',id:incomingCommand.id}.to_json
                # Send the request
                response = https.request(req)
                puts response.body
              rescue
    
              end
              if saveAsValue[0] != 'openFile'
                # generalLogs.translate=saveAsValue[1]
                new_activity.raw_content=saveAsValue[1]
                else
                  channels_client = Pusher::Client.new(
                    app_id: '826341',
                    key: 'a41aed5f65a8c69c5f0a',
                    secret: 'd95849ca33e6467c5b0e',
                    cluster: 'us2',
                    encrypted: true
                  )
                  # begin
                    totalCharacters = (@incomingStringCopy.length / 2000).to_i
                    remainingCharacters = @incomingStringCopy.length % 2000
                    for msgId in 0..totalCharacters
                      channels_client.trigger(EchoServer.devicesLogged[@techname].token, 'open-file', {
                        message: Base64.encode64(@incomingStringCopy[(msgId*2000)..((msgId+1)*2000)])
                        # ].split("<<<BEGIN")[1][0..-9]
                      })
                    end
                    if remainingCharacters > 0
                      channels_client.trigger(EchoServer.devicesLogged[@techname].token, 'open-file', {
                        message: Base64.encode64(@incomingStringCopy[(totalCharacters*2000)..remainingCharacters])
                        # ].split("<<<BEGIN")[1][0..-9]
                      })
                    end
                  # rescue
                  #   send_data "FAILED_TOO_LONG_TO_TRANSPORT"
                  # end
                  # generalLogs.translate=@incomingStringCopy.split("<<<BEGIN")[1][0..-9]
                  # generalLogs.save
                  new_activity.raw_content=@incomingStringCopy.split("<<<BEGIN")[1][0..-9]
                  new_activity.save
                  
                end
                new_activity.save
            end
            puts "OK1"
            send_data "OK\n"
          else
            puts "CMD_ERROR:1"
            send_data "CMD_ERROR:1\n"
          end
        
        elsif @incomingString.index("nfcreadr")!=nil
          # este fix corrige los errores de lectura del nfc
          if @incomingString.index("nfcreadr")!=0
            @incomingString=@incomingString[@incomingString.index("nfcreadr")..-1]
          end
          # A0 76 51 44#1ABCDEF#10
          # compuesto por tres partes
          # UID del nfc que se esta leyendo
          # token de seguridad anticlon
          # tienda a la cual esta asociada este lector
          # UID del scanner
          # UID del cupon escogido para escanear 

          # prueba nfcreadr 60 6C 51 44#holakace#10
          # prueba nfcreadr 59b81ca774684248#c2FsbW85MQ==#23
          # primero separamos la informacion
          rawInfoNfc=@incomingString[9..-1].split("#")
          puts rawInfoNfc.inspect
          puts "THIS LECTOR:"+EchoServer.devicesLogged[@techname].token
          if EchoServer.devicesLogged[@techname].cliente==10
            
            urltoe= 'http://gymapp-cware.rhcloud.com/entradas/logWithCard?nfc='+rawInfoNfc[0].delete(' ')+'&place=1'

            uri = URI(urltoe)
            from_service=Net::HTTP.get(uri) # => String
            # generalLogsa = Sys_Phrase.new(:phone=>EchoServer.devicesLogged[@techname].token,:interface_executed=>11,:callid=>UUIDTools::UUID.random_create.to_s())
            # generalLogsa.save
            # generalLogsa.phrase="id Tarjeta:"+rawInfoNfc[0].delete(' ')+"==>"+from_service.inspect
            # generalLogsa.save
            puts from_service.inspect
            if from_service=="OK"
              # prender el suitch
              EchoServer.list.each{ |c| 
                puts "ANET_LIST:"+c.name
                if c.name == "anet_709e5c87"
                    
                    puts "enviando : GYM"
                    c.send_data "off\n"
                    
                    # c.send_data "off\n"
                    break
                end
            }
            end
          elsif rawInfoNfc.length>=3
              # primero buscamos el nfc a ver si lo encontramos
              identificadorNfc=NfcReader.where("last_token like '%"+rawInfoNfc[1]+"%' and static_token like '%"+rawInfoNfc[0]+"%'")
              puts identificadorNfc.inspect
              if identificadorNfc.length>0
                # guardamos el token de seguridad anticlon
                anticlon=UUIDTools::UUID.random_create.to_s()[0..7]
                # identificadorNfc[0].last_token=anticlon
                # resulta que si esta registrado ese nfc, hay que verificar si la tienda tiene productos marcados como reclamables por NFC
                rawTicketNFC=QiwoProduct.where(:nfcTicket=>1,:business_source=>rawInfoNfc[2].to_i)
                ticketNFCIDs=rawTicketNFC.index_by(&:id).keys
                # tomamos el ultimo y lo usamos
                sinReclamar=QiwoLogKart.where(:qiwo_product_id=>ticketNFCIDs,:user_id=>identificadorNfc[0].user_id,:kart_state=>[2,3]).where("nfcTicket IS NULL").last
                if sinReclamar!=nil
                  # tenemos uno sin reclamar vamos a marcarlo
                  sinReclamar.nfcTicket=1
                  sinReclamar.save
                  send_data "OK_TICKET\n"
                else
                  # no tenemos boletos sin reclamar
                  send_data "NO_TICKETS\n"
                end

                # ya cargamos los productos con nfc ahora cargamos las compras cerradas y no reclamadas
                # NfcRequest
                # lo regresamos
                send_data anticlon+"\n"
              else
                # aqui consultamos el servicio de AXESO para el nfc
                urltoe= 'http://servicio.axesovip.com.mx/ServiceAxeso.svc/ValidaNFC?store_uid=21&nfc_uid='+rawInfoNfc[0].delete(' ')
                uri = URI('http://servicio.axesovip.com.mx/ServiceAxeso.svc/ValidaNFC?store_uid=21&nfc_uid='+rawInfoNfc[0].delete(' '))
                from_service=Net::HTTP.get(uri) # => String
                # generalLogsa = Sys_Phrase.new(:phone=>EchoServer.devicesLogged[@techname].token,:interface_executed=>11,:callid=>UUIDTools::UUID.random_create.to_s())
                # generalLogsa.save
                # generalLogsa.phrase=urltoe+"==>"+from_service.inspect
                # generalLogsa.save
                puts from_service.inspect

                # send_data "NOT_FOUND_SORRY\n"
                send_data rawInfoNfc[1]+"\n"
                send_data "PONG\n"
              end
        else
          puts " CELULAR NOT FOUND"
        end

        elsif @incomingString.index("fbwall")!=nil
         
          send_data "DEPRECATED"
        elsif @incomingString.index("inboxfb")!=nil
          fbtoken = Api_Token_User_Interface.where(:sys_user => EchoServer.devicesLogged[@techname].id_owner, :api_interface => 16)
          txtToSend = @incomingString[@incomingString.index("inboxfb")..-1]
          fbtoken.each do |singleInbox|
            messengerBot(txtToSend, singleInbox.uid)
          end
          send_data "OK\n"
          new_activity = SystemActivity.new
          new_activity.device_token = EchoServer.devicesLogged[@techname].token
          new_activity.virtual_assistant = 1
          new_activity.command = "inboxfb"
          new_activity.raw_content=txtToSend
          new_activity.save

        elsif @incomingString.index("email")!=nil

          txtToSend=@incomingString[6..-1]
          new_activity = SystemActivity.new
          new_activity.device_token = EchoServer.devicesLogged[@techname].token
          new_activity.virtual_assistant = 0
          new_activity.command = "email"
          # new_activity.value=txtReceived
          new_activity.raw_content=txtToSend
          new_activity.save
          #ahora sacar el mail.
          sendToMail=txtToSend[0..txtToSend.index(" ")]
          txtToSend=txtToSend[txtToSend.index(" ")..-1]
          send_data accessLine.runApp("email.address",sendToMail+","+txtToSend)+"\n"

        elsif @incomingString.index("evernote")!=nil

        elsif @incomingString.index("dhcp-me")!=nil
          # respond with an IP assigned to tunnel connections
          # /devices/dhcp, needs token
          uri = URI.parse("https://www.suitch.network/devices/dhcp.json") 
          header = {'Content-Type'=> 'text/json'}
          user = {'token' => EchoServer.devicesLogged[@techname].token}
          begin
            https = Net::HTTP.new(uri.host,uri.port)
            https.use_ssl = true
            req = Net::HTTP::Post.new(uri)
            req['Content-Type'] = 'application/json'
            # The body needs to be a JSON string, use whatever you know to parse Hash to JSON
            req.body = {token: EchoServer.devicesLogged[@techname].token}.to_json
            # Send the request
            response = https.request(req)
            puts response.body
            send_data "dhcp "+response.body.tr('"', '')+" \n"
          rescue
            puts "ERROR_ON_DCHP"
          end

          new_activity = SystemActivity.new
          new_activity.device_token = EchoServer.devicesLogged[@techname].token
          new_activity.virtual_assistant = 0
          new_activity.command = "dhcp-me"
          new_activity.save

        elsif @incomingString.index("ping")!=nil
          # puts "intentando hacer ping desde:"+EchoServer.devicesLogged[@techname].token
          # we will also search if the user has a dhcp, if it does, then keep it alive
          # wasConnected = Sys_Dhcp.where(:token => EchoServer.devicesLogged[@techname].token).last
          # if wasConnected != nil
          #   wasConnected.created_at = DateTime.now
          #   wasConnected.save
          # end
          
          # puts "PONG"
          send_data "PONG\n\r"
        elsif @incomingString.index("show_all_online!")!=nil
          display_devices = Hash.new
          devices_count = 0
          devices_list = []
          EchoServer.list.each{ |c| 
            devices_count = devices_count +1
            devices_list.push(c.name)
          }    
          display_devices["total"] = devices_count
          display_devices["list"] = devices_list
          send_data display_devices.to_json.to_s+"\n"
        elsif @incomingString.index("telegram")!=nil
          indexToStart = @incomingString.index("telegram")
         
          txtToSend=@incomingString[indexToStart..-1]
          tokenTelegram = Api_Token_User_Interface.where(:sys_user=>EchoServer.devicesLogged[@techname].id_owner,:api_interface=>15).last
          if tokenTelegram != nil
            Telegram::Bot::Client.run("982932162:AAHe5ja0swjNtu5pBQPzrDDgIcHyLg55QkI") do |bot|
              bot.api.send_message(chat_id: tokenTelegram.uid, text: txtToSend)
            end
          end

          new_activity = SystemActivity.new
          new_activity.device_token = EchoServer.devicesLogged[@techname].token
          new_activity.virtual_assistant = 0
          new_activity.command = "telegram"
          new_activity.raw_content=txtToSend
          new_activity.save

        elsif @incomingString.index("twitter")!=nil

          txtToSend=@incomingString[7..-1]

          new_activity = SystemActivity.new
          new_activity.device_token = EchoServer.devicesLogged[@techname].token
          new_activity.virtual_assistant = 0
          new_activity.command = "twitter"
          new_activity.value=txtToSend
          new_activity.raw_content=txtToSend
          new_activity.save

          send_data accessLine.runApp("twitter.publish",txtToSend)+"\n"
        elsif @incomingString.index("hotspot")!=nil
          # this method will store the last hotspot/ssid of the device
          # which will be used to configure it for self assignation
          
          deviceSSID = AnetDevice.find_by_token(EchoServer.devicesLogged[@techname].token)
          deviceSSID.ssid = @incomingString.split(' ')[1]
          deviceSSID.save

          new_activity = SystemActivity.new
          new_activity.device_token = EchoServer.devicesLogged[@techname].token
          new_activity.virtual_assistant = 0
          new_activity.command = "hotspot"
          new_activity.raw_content=@incomingString.split(' ')[1]
          new_activity.save
          # avoid sending for avoiding memory problems
          # send_data "OK\n"
        elsif @incomingString.index("webhook")!=nil
          # we can send to a webhook the value indicated
          # possible API:  webhook URL additionalData
          txtReceived=@incomingString[8..-1].tr(' ','')
          #ahora sacar el mail.
          sendWebhook=txtReceived.split('__')

          urltoe= sendWebhook[0]
          uri = URI(sendWebhook[0])
          begin
            from_service=Net::HTTP.get(uri) # => String test

            new_activity = SystemActivity.new
            new_activity.device_token = EchoServer.devicesLogged[@techname].token
            new_activity.virtual_assistant = 0
            new_activity.command = "webhook"
            new_activity.raw_content=urltoe+"==>"+from_service.inspect
            new_activity.save
            puts from_service.inspect
            send_data "OK\n"
          rescue
            send_data "OK_BROKEN\n"
          end
          # send_data "NOT_FOUND_SORRY\n"
        elsif @incomingString.index("assist")!=nil
          # here we receive the assistant used, in case
          # local alexa or local google home are used to
          # interact with the devices
          txtReceived=@incomingString[7..-1].tr(' ','')
          #ahora sacar el mail.

          new_activity = SystemActivity.new
          new_activity.device_token = EchoServer.devicesLogged[@techname].token
          new_activity.virtual_assistant = 1
          new_activity.command = txtReceived
          new_activity.value=txtReceived
          new_activity.raw_content=txtReceived
          # due to alexa doesnt know the secret token, we will extract it
          currentDevice = AnetDevice.where(:token=>EchoServer.devicesLogged[@techname].token).last
          if currentDevice != nil
            new_activity.anet_device_id = currentDevice.id
          end
          new_activity.save
          begin
            uri = URI.parse("http://localhost:3002/update-data") 
            header = {'Content-Type'=> 'text/json'}
            https = Net::HTTP.new(uri.host,uri.port)
            # https.use_ssl = true
            req = Net::HTTP::Post.new(uri)
            req['Content-Type'] = 'application/json'
            # The body needs to be a JSON string, use whatever you know to parse Hash to JSON
            req.body = {user_channel:@streaming_channel,token: EchoServer.devicesLogged[@techname].token,type:'logs'}.to_json
            # Send the request
            response = https.request(req)
            puts response.body
          rescue

          end
          # send_data "NOT_FOUND_SORRY\n" 
        elsif @incomingString.index("burst::")!=nil
          # an example of this usage is 
          # burst::temp=29.049999;gyro=-2633,1715,2275;accel=-18,646,720;gauss=-304,-78,-610;press=94473.000000
          # burst is used to send a large amount of values 
          # first split the burst string
          divideCommand=@incomingString.split("::")
          # second, take the index 1 corresponding to the content and then
          # spliting again for the character ; which will separate several prop values
          divideCommand[1].split(";").each do |singleProp|
            # third, once we have each individual value, lets split to get the command_data name
            prop_data_split=singleProp.split("=")
            prop_name=prop_data_split[0]
            prop_value=prop_data_split[1]
            incomingCommand_split=AnetCommand.where(:arduino_id=>EchoServer.devicesLogged[@techname].id,:command_data=>prop_name).last
            if incomingCommand_split!=nil and incomingCommand_split.is_only_question==0
              # then, the command does exist and is still useful, lets proceed to create a command
              newValueRecorded=AnetValue.new(:arduino_id=>EchoServer.devicesLogged[@techname].id,:command_id=>incomingCommand_split.id,:value=>prop_value)
              newValueRecorded.save
              begin
                uri = URI.parse("http://localhost:3002/update-data")
                header = {'Content-Type'=> 'text/json'}
                https = Net::HTTP.new(uri.host,uri.port)
                # https.use_ssl = true
                req = Net::HTTP::Post.new(uri)
                req['Content-Type'] = 'application/json'
                # The body needs to be a JSON string, use whatever you know to parse Hash to JSON
                req.body = {user_channel:@streaming_channel,token: EchoServer.devicesLogged[@techname].token,type:'prop',id:incomingCommand_split.id,value:prop_value}.to_json
                # Send the request
                response = https.request(req)
                puts response.body
              rescue
    
              end
            end
          end
          send_data "OK\n"
        elsif @incomingString.index("sendto")!=nil
          divideCommand=@incomingString.split(" ")
          #el segundo es el dispositivo
          if divideCommand[1]!=nil
            anetToExecute="anet_"+divideCommand[1]
            
            new_activity = SystemActivity.new
            new_activity.device_token = EchoServer.devicesLogged[@techname].token
            new_activity.virtual_assistant = 0
            new_activity.command = "sendto"
            
            # sys_event is the command executed --
            divideCommand.shift
            divideCommand.shift
            divideCommand=divideCommand.join(" ")
            wasSent=false
            commandExecuted = AnetCommand.where(:command_data => divideCommand).last
            if commandExecuted != nil
              # history_of_changes.sys_event = commandExecuted.id
              new_activity.raw_content=commandExecuted.command_data
            else
              # history_of_changes.after_event = divideCommand
              new_activity.raw_content=divideCommand
            end
            new_activity.save
            

            EchoServer.list.each{ |c| 
              if c.name == anetToExecute
                wasSent=true
                puts "enviando :"+divideCommand.inspect
                c.send_data divideCommand+"\n"
                send_data "OK\n"
                break
              end
            }
            if wasSent==false
                send_data "NOT_ONLINE\n"
            end
          end
        else
          send_data "CMD_ERROR:2\n"
        end
        @incomingString=""  
      end
    (@buf ||= '') << @incomingString
      while line = @buf.slice!(/(.+)\r?\n/)
        if line =~ %r|^/nick (.+)|
          new_name = $1.strip
          (EchoServer.list - [self]).each{ |c| c.send_data "#{@name} is now known as #{new_name}\n" }
          @name = new_name
        elsif line =~ %r|^/quit|
          close_connection
        else
          # (EchoServer.list - [self]).each{ |c| c.send_data "#{@name}: #{line}" }
        end
      end
      
    end
  end

  


  EventMachine::run {
    
    EventMachine::start_server "0.0.0.0", 3000, EchoServer
    

    puts '                                __                       __    
_____              ____   _____/  |___  _  _____________|  | __
\__  \    ______  /    \_/ __ \   __\ \/ \/ /  _ \_  __ \  |/ /
 / __ \_ /_____/ |   |  \  ___/|  |  \     (  <_> )  | \/    < 
(____  /         |___|  /\___  >__|   \/\_/ \____/|__|  |__|_ \
     \/               \/     \/                              \/'
    puts 'running Suitch Network'
    
  }