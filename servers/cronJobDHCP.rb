require 'clockwork'
require 'active_record'
require 'simple-rss'
require 'open-uri'
require 'xmlsimple'
require '/usr/src/jarvixServer/db/model/anet_command.rb'
require '/usr/src/jarvixServer/db/model/earthquake.rb'
require '/usr/src/jarvixServer/db/model/anet_device.rb'
require '/usr/src/jarvixServer/db/model/sys_user.rb'
require '/usr/src/jarvixServer/db/model/sys_dhcp.rb'
require '/usr/src/jarvixServer/db/model/geo_setting.rb'
require '/usr/src/jarvixServer/db/model/feed_reader.rb'
require '/usr/src/jarvixServer/db/model/qiwo_enterprise.rb'
require '/usr/src/jarvixServer/db/model/qiwo_product.rb'
require '/usr/src/jarvixServer/db/model/summary.rb'
require '/usr/src/jarvixServer/db/model/api_token_user_interface.rb'
require 'net/ping'


#Daemonization
#
#
#You need daemons gem to use clockworkd. It is not automatically installed, please install by yourself.
#
#Then,
#
#clockworkd -c YOUR_CLOCK.rb start
# clockworkd -c [your .rb file] start|stop|restart
#For more details, see help shown by clockworkd.

def up?(host)
    check = Net::Ping::External.new(host)
    check.ping?
end
module Clockwork
  handler do |job, time|
    puts "Running #{job}"
    #identificar cambio de dia para desconectar las cuentas de qiwo
    
    if job=="DHCPManager"
        puts "DHCP pseudo process"
        Sys_Dhcp.where("created_at <= ?", DateTime.now - 5.minute).each do |singleIP|
            puts singleIP.ip+":"+up?(singleIP.ip).to_s
            if up?(singleIP.ip) == false
                # remove ip
                singleIP.destroy
            end
        end
        
    end
    
    # commandsToExecute=AnetCommand.where(:on_time=>time) 
    # #ahora comprobar la ejecucion
    # s = TCPSocket.new 'localhost', 3000
    # while line = s.gets # Read lines from socket
    #     puts line.inspect
    #     if line.index("TOKEN?")!=nil
    #         puts "enviando"
    #        s.puts "025621de/d822339f" 
    #     elsif line.index("WLCME")!=nil
    #         puts "WLCME."
    #         commandsToExecute.each do |singleCommand|
    #             dispositivo=AnetDevice.find_by_id(singleCommand.arduino_id)
    #             if dispositivo!=nil
    #             	puts dispositivo.token
    #             	s.puts "sendto "+dispositivo.token+" "+singleCommand.command_data	
    #             end
    #         end
    #         break
    #     end
    # end
    # s.close
  end


    # we create a 5 minute life time for assigning IPs or removing them
    every(5.minutes, 'DHCPManager')
    #  every(1.hour, 'hourly.job')

    every(1.day, 'midnight.job1', :at => '00:00')
    every(1.day, 'midnight.testjob2', :at => '00:30')
    every(1.day, 'midnight.testjob3', :at => '01:00')
    every(1.day, 'midnight.testjob4', :at => '01:30')
    every(1.day, 'midnight.testjob41', :at => '01:34')
    every(1.day, 'midnight.testjob42', :at => '01:36')
    every(1.day, 'midnight.testjob43', :at => '01:40')
    every(1.day, 'midnight.testjob5', :at => '02:00')
    every(1.day, 'midnight.testjob6', :at => '02:30')
    every(1.day, 'midnight.testjob7', :at => '03:00')
    every(1.day, 'midnight.testjob8', :at => '03:30')
    every(1.day, 'midnight.testjob9', :at => '04:00')
    every(1.day, 'midnight.testjob10', :at => '04:30')
    every(1.day, 'midnight.testjob11', :at => '05:00')
    every(1.day, 'midnight.testjob12', :at => '05:30')
    every(1.day, 'midnight.testjob13', :at => '06:00')
    every(1.day, 'midnight.testjob14', :at => '06:30')
    every(1.day, 'midnight.testjob15', :at => '07:00')
    every(1.day, 'midnight.testjob16', :at => '07:30')
    every(1.day, 'midnight.testjob17', :at => '08:00')
    every(1.day, 'midnight.testjob18', :at => '08:30')
    every(1.day, 'midnight.testjob19', :at => '09:00')
    every(1.day, 'midnight.testjob20', :at => '09:30')
    every(1.day, 'midnight.testjob21', :at => '10:00')
    every(1.day, 'midnight.testjob22', :at => '10:30')
    every(1.day, 'midnight.testjob23', :at => '11:00')
    every(1.day, 'midnight.testjob24', :at => '11:30')
    every(1.day, 'midnight.testjob25', :at => '12:00')
    every(1.day, 'midnight.testjob26', :at => '12:30')
    every(1.day, 'midnight.testjob27', :at => '13:00')
    every(1.day, 'midnight.testjob28', :at => '13:30')
    every(1.day, 'midnight.testjob29', :at => '14:00')
    every(1.day, 'midnight.testjob30', :at => '14:30')
    every(1.day, 'midnight.testjob31', :at => '15:00')
    every(1.day, 'midnight.testjob32', :at => '15:30')
    every(1.day, 'midnight.testjob33', :at => '16:00')
    every(1.day, 'midnight.testjob34', :at => '16:30')
    every(1.day, 'midnight.testjob35', :at => '17:00')
    every(1.day, 'midnight.testjob36', :at => '17:30')
    every(1.day, 'midnight.testjob37', :at => '18:00')
    every(1.day, 'midnight.testjob38', :at => '18:30')
    every(1.day, 'midnight.testjob39', :at => '19:00')
    every(1.day, 'midnight.testjob40', :at => '19:30')
    every(1.day, 'midnight.testjob41', :at => '20:00')
    every(1.day, 'midnight.testjob42', :at => '20:30')
    every(1.day, 'midnight.testjob43', :at => '21:00')
    every(1.day, 'midnight.testjob44', :at => '21:30')
    every(1.day, 'midnight.testjob45', :at => '22:00')
    every(1.day, 'midnight.testjob46', :at => '22:30')
    every(1.day, 'midnight.testjob47', :at => '23:00')
    every(1.day, 'midnight.testjob48', :at => '23:30')
end