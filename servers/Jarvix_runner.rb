PROJECT_HOME = "/usr/pbx-ruby/"
require 'rubygems'
require 'eventmachine'
require 'yaml'
require 'active_record'
require 'logger'
require 'json'
require '/usr/src/jarvixServer/jarvixSystem/Jarvix.rb'

#Aquiring the needed models from the app

puts PROJECT_HOME
#connecting to database
ActiveRecord::Base.logger = Logger.new( STDERR )
db_config = YAML::load( File.open("#{PROJECT_HOME}/config/database.yml"))
ActiveRecord::Base.establish_connection( db_config["development"])



  module EchoServer
    attr_accessor:jarvixServer
    def post_init (*args)
      puts args
      puts "-- Client connected!"
      @jarvixServer=Jarvix.new(8180829367,"en")
      send_data "JX>"
      send_data @jarvixServer.openPort
      send_data @jarvixServer.openTransmission

    end

    def receive_data data
      #send_data ">>> you sent: #{data}"
      respuesta=@jarvixServer.payAttention(data)
      send_data respuesta+"\n"
    end
  end

  


  EventMachine::run {
    
    EventMachine::start_server "0.0.0.0", 4001, EchoServer
    puts '    ____.                  .__        
    |    |____ __________  _|__|__  ___
    |    \__  \\_  __ \  \/ /  \  \/  /
/\__|    |/ __ \|  | \/\   /|  |>    < 
\________(____  /__|    \_/ |__/__/\_ \
              \/                     \/'
    puts 'running echo server on 4001'
    
  }