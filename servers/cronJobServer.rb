require 'clockwork'
require 'active_record'
require 'simple-rss'
require 'open-uri'
require 'xmlsimple'
require 'xmpp4r_facebook'
require 'xmpp4r/roster'
require 'fb_graph'
require '/usr/src/jarvixServer/db/model/anet_command.rb'
require '/usr/src/jarvixServer/db/model/earthquake.rb'
require '/usr/src/jarvixServer/db/model/anet_device.rb'
require '/usr/src/jarvixServer/db/model/sys_user.rb'
require '/usr/src/jarvixServer/db/model/geo_setting.rb'
require '/usr/src/jarvixServer/db/model/feed_reader.rb'
require '/usr/src/jarvixServer/db/model/qiwo_enterprise.rb'
require '/usr/src/jarvixServer/db/model/qiwo_product.rb'
require '/usr/src/jarvixServer/db/model/summary.rb'
require '/usr/src/jarvixServer/db/model/api_token_user_interface.rb'
require '/usr/src/lineAccess/app/models/qiwo_subscription.rb'
require '/usr/src/lineAccess/app/models/qiwo_user_subscription.rb'

#Daemonization
#
#
#You need daemons gem to use clockworkd. It is not automatically installed, please install by yourself.
#
#Then,
#
#clockworkd -c YOUR_CLOCK.rb start
#For more details, see help shown by clockworkd.
module Clockwork
  handler do |job, time|
    puts "Running #{job}"
    #identificar cambio de dia para desconectar las cuentas de qiwo
    if job=="midnight.job1"
        #buscar todos los usuarios
        # usuarios=Sys_User.all
        # usuarios.each do |singleUser|
        #     #obtener suscripcion
        #     suscripcion=QiwoUserSubscription.where(:user_id=>singleUser.id,:touched=>0).where(['ends_on >= ?', DateTime.now]).last
        #     if suscripcion!=nil
        #         suscripcion.touched=1
        #         suscripcion.save
        #        #si no tiene suscripcion poner invisibles los productos
        #       #obtener las empresas del usuario
        #       empresas=QiwoEnterprise.where(:user_owner=>singleUser.id).index_by(&:id).keys
        #       #analizar productos
        #       productos=QiwoProduct.where(:business_source=>empresas)
        #       modoGratis=QiwoSubscription.where(:price=>["0","0.0","0.00"]).last
        #       contadorGratis=0
        #       productos.each do |singleProduct|
        #         #activar solo plan gratis
        #         if modoGratis.products_allowed<=contadorGratis
        #             #tomamos solo los que siguen despues del plan gratis
        #             singleProduct.search_enabled=0
        #             singleProduct.save
        #         end
        #         contadorGratis=contadorGratis+1
        #       end
        #     end
               
        # end
    end
    if job=="feedReader"
        puts "phase 1"
        # ver si tenemos mensajes globales por enviar
        globales=Summary.where(:is_active=>1).last
        if globales!=nil
            puts "phase 2"
            token=Api_Token_User_Interface.where(:sys_user=>33,:api_interface=>2).last
            globales.is_active=0
            globales.save
            usuarios=Sys_User.where("gps_location IS NOT NULL")
            puts "phase 3"
            # usuarios.each do |singleList|
            #     direccionString=singleList
            #     mensaje=globales.texto
            #     cuerpoMensaje='Hola '+direccionString.name.to_s+', '+mensaje
            #     id = '-100006392581606@chat.facebook.com'
            #     puts cuerpoMensaje
            #     if direccionString.user_id!=nil
            #         to = '-'+direccionString.user_id+'@chat.facebook.com'
            #       #buscar producto
            #         body = cuerpoMensaje
            #         subject = 'no titulo'
            #         message = Jabber::Message.new to, body
            #         message.subject = subject
                    
            #         client = Jabber::Client.new Jabber::JID.new(id)
            #         client.connect
            #         puts token.token
            #         client.auth_sasl(Jabber::SASL::XFacebookPlatform.new(client, '357503864375536', token.token, 'e32f8dbdee5cb477333991302a1c78bf'), nil)            
            #         client.send message
            #         client.close
            #       end
            #     end
                puts "phase 4"
        end
        # aqui leemos los feeds
        if ActiveRecord::Base.logger!=nil
            ActiveRecord::Base.logger.level = 1 
        end
        feedsactivosParaLeer=FeedReader.where(:is_active=>1)
        puts "phase 5"
        feedsactivosParaLeer.each do |singleFeed|
            cfeed=SimpleRSS.parse open(singleFeed.url_source)
            puts "phase 6"
            porNotificar=[]
            nuevoUltimo=nil
            cfeed.entries.each do |singleEntry|
                
                if nuevoUltimo==nil and singleEntry.id!=singleFeed.last_notification
                    nuevoUltimo=singleEntry.id
                    
                end
                puts "phase 7"
                if singleEntry.id==singleFeed.last_notification
                    break
                end
                puts "phase 8"
                @xml=XmlSimple.xml_in(singleEntry[:content])
                
                
                posicion=nil
                poligono=false
                puts "phase 9"
                # si no tiene area
                
                begin
                    posicion=@xml["info"][0]["area"][0]["circle"][0].split(" ")    
                rescue Exception => e
                    poligono=true
                end
                puts "phase 10"
                begin
                    posicion=@xml["info"][0]["area"][0]["polygon"][0].split(" ")

                rescue Exception => e
                    poligono=false
                end
                puts "phase 11"
                iDList=[]
                puts "IS_POLIGON:"+poligono.to_s
                if poligono==true
                    puts "POLIGONO"
                    puts posicion
                    tmpArray=[]
                    i=0
                    j=0
                    for i in 0..posicion.length-1
                        usuarios=Sys_User.where("((("+posicion[i].split(",")[0].to_s+"< lon and "+posicion[j].split(",")[0].to_s+">=lon) or ("+posicion[j].split(",")[0].to_s+"< lon and "+posicion[i].split(",")[0].to_s+">=lon)) and ("+posicion[i].split(",")[1].to_s+"<=lat or "+posicion[j].split(",")[1].to_s+"<=lat))  and ("+posicion[i].split(",")[1].to_s+"+(lon-"+posicion[i].split(",")[0].to_s+")/("+posicion[j].split(",")[0].to_s+"-"+posicion[i].split(",")[0].to_s+")*("+posicion[j].split(",")[1].to_s+"-"+posicion[i].split(",")[1].to_s+")<lat)").index_by(&:id).keys
                        # if (((coordsPol[i].split(",")[0].to_f< y && coordsPol[j].split(",")[0].to_f>=y or coordsPol[j].split(",")[0].to_f< y && coordsPol[i].split(",")[0].to_f>=y) and  (coordsPol[i].split(",")[1].to_f<=x || coordsPol[j].split(",")[1].to_f<=x))) and (coordsPol[i].split(",")[1].to_f+(y-coordsPol[i].split(",")[0].to_f)/(coordsPol[j].split(",")[0].to_f-coordsPol[i].split(",")[0].to_f)*(coordsPol[j].split(",")[1].to_f-coordsPol[i].split(",")[1].to_f)<x)
                        puts "phase 12"
                        usuarios.each do |singlePolygonID|
                            if iDList.index(singlePolygonID)==nil
                                iDList.push(singlePolygonID)
                            else
                                # quitarlo
                                iDList.delete(singlePolygonID)
                            end
                        end
                    j=i;
                    end
                    puts "polygon====>"+tmpArray.inspect
                end

                # ------------------>
                
                puts "phase 11.5"
                # aqui debemos poner la peticion
                newGeoLog=Earthquake.new
                puts posicion.inspect
                if @xml["info"][0]["area"]!=nil
                    newGeoLog.latitude=posicion[0].split(",")[1]
                    newGeoLog.longitude=posicion[0].split(",")[0]    
                end
                
                newGeoLog.rawData=singleEntry
                newGeoLog.gateway=singleFeed.event_typer
                newGeoLog.save
                 if @xml["info"][0]["area"]==nil
                    break
                 end

                puts "phase 13"
                # 
                if poligono==false
                    if singleFeed.event_typer==3
                        iDList=Sys_User.where("acos(sin(sys_users.lat * 0.0175) * sin("+posicion[0].split(",")[1]+" * 0.0175) + cos(sys_users.lat * 0.0175) * cos("+posicion[0].split(",")[1]+" * 0.0175) * cos(("+posicion[0].split(",")[0]+" * 0.0175) - (sys_users.lon * 0.0175))) * 3959 <= geo_settings.add_distancia*.62137").joins(:geo_settings).select("geo_settings.add_distancia,sys_users.id").where("geo_settings.water=1").index_by(&:id).keys    
                    elsif singleFeed.event_typer==2
                        iDList=Sys_User.where("acos(sin(sys_users.lat * 0.0175) * sin("+posicion[0].split(",")[1]+" * 0.0175) + cos(sys_users.lat * 0.0175) * cos("+posicion[0].split(",")[1]+" * 0.0175) * cos(("+posicion[0].split(",")[0]+" * 0.0175) - (sys_users.lon * 0.0175))) * 3959 <= geo_settings.add_distancia*.62137").joins(:geo_settings).select("geo_settings.add_distancia,sys_users.id").where("geo_settings.volcano=1").index_by(&:id).keys    
                    elsif singleFeed.event_typer==1
                        iDList=Sys_User.where("acos(sin(sys_users.lat * 0.0175) * sin("+posicion[0].split(",")[1]+" * 0.0175) + cos(sys_users.lat * 0.0175) * cos("+posicion[0].split(",")[1]+" * 0.0175) * cos(("+posicion[0].split(",")[0]+" * 0.0175) - (sys_users.lon * 0.0175))) * 3959 <= geo_settings.add_distancia*.62137").joins(:geo_settings).select("geo_settings.add_distancia,sys_users.id").where("geo_settings.earthquakes=1").index_by(&:id).keys    
                    end
                end
                puts "phase 14"
                # <------------------
                    
                
                puts "USUARIOS:"+iDList.inspect
                
                    
                    token=Api_Token_User_Interface.where(:sys_user=>33,:api_interface=>2).last
                    mensaje=nil
                   puts "phase 15" 
                    if singleFeed.event_typer==3 and @xml["info"][0]["description"][0]!={}
                        mensaje="Se detectó un evento pluvial de riesgo en tu zona, "+@xml["info"][0]["description"][0]
                    elsif singleFeed.event_typer==2 and @xml["info"][0]["description"][0]!={}
                        mensaje="Se detectó un evento volcanico de riesgo en tu zona, "+@xml["info"][0]["description"][0]
                    elsif singleFeed.event_typer==1 and @xml["info"][0]["description"][0]!={}
                        mensaje="Se detectó un terremoto de riesgo en tu zona, "+@xml["info"][0]["description"][0]
                    else
                        mensaje="Se detectó un evento geologico critico en tu zona, no hay comentarios oficiales aún, toma precauciones."
                    end
                    puts "phase 16"
                    iDList.each do |singleList|
                      direccionString=Sys_User.find_by_id(singleList)
                      if direccionString!=nil
                        cuerpoMensaje='Hola '+direccionString.name.to_s+', '+mensaje
                        id = '-100006392581606@chat.facebook.com'
                        puts cuerpoMensaje
                        if direccionString.user_id!=nil
                            to = '-'+direccionString.user_id+'@chat.facebook.com'
                          #buscar producto
                            body = cuerpoMensaje
                            subject = 'no titulo'
                            message = Jabber::Message.new to, body
                            message.subject = subject
                            
                            client = Jabber::Client.new Jabber::JID.new(id)
                            client.connect
                            puts token.token
                            client.auth_sasl(Jabber::SASL::XFacebookPlatform.new(client, '357503864375536', token.token, 'e32f8dbdee5cb477333991302a1c78bf'), nil)            
                            client.send message
                            client.close
                          end
                        end
                    end
                
            end
            # guardando ultimo a notificar
            if nuevoUltimo!=nil
             singleFeed.last_notification=nuevoUltimo
             singleFeed.save   
            end
             
        end
    end
    commandsToExecute=AnetCommand.where(:on_time=>time)
    #ahora comprobar la ejecucion
      s = TCPSocket.new 'localhost', 3000
    while line = s.gets # Read lines from socket
        puts line.inspect
        if line.index("TOKEN?")!=nil
            puts "enviando"
           s.puts "025621de/d822339f" 
        elsif line.index("WLCME")!=nil
            puts "WLCME."
            commandsToExecute.each do |singleCommand|
                dispositivo=AnetDevice.find_by_id(singleCommand.arduino_id)
                if dispositivo!=nil
                	puts dispositivo.token
                	s.puts "sendto "+dispositivo.token+" "+singleCommand.command_data	
                end
            end
            break
        end
    end
    
    s.close
  end

  # handler receives the time when job is prepared to run in the 2nd argument
  # handler do |job, time|
  #   puts "Running #{job}, at #{time}"
  # end

#  every(10.seconds, 'frequent.job')
#  every(3.minutes, 'less.frequent.job')
    every(1.minutes, 'feedReader')
#  every(1.hour, 'hourly.job')

    every(1.day, 'midnight.job1', :at => '00:00')
    every(1.day, 'midnight.testjob2', :at => '00:30')
    every(1.day, 'midnight.testjob3', :at => '01:00')
    every(1.day, 'midnight.testjob4', :at => '01:30')
    every(1.day, 'midnight.testjob41', :at => '01:34')
    every(1.day, 'midnight.testjob42', :at => '01:36')
    every(1.day, 'midnight.testjob43', :at => '01:40')
    every(1.day, 'midnight.testjob5', :at => '02:00')
    every(1.day, 'midnight.testjob6', :at => '02:30')
    every(1.day, 'midnight.testjob7', :at => '03:00')
    every(1.day, 'midnight.testjob8', :at => '03:30')
    every(1.day, 'midnight.testjob9', :at => '04:00')
    every(1.day, 'midnight.testjob10', :at => '04:30')
    every(1.day, 'midnight.testjob11', :at => '05:00')
    every(1.day, 'midnight.testjob12', :at => '05:30')
    every(1.day, 'midnight.testjob13', :at => '06:00')
    every(1.day, 'midnight.testjob14', :at => '06:30')
    every(1.day, 'midnight.testjob15', :at => '07:00')
    every(1.day, 'midnight.testjob16', :at => '07:30')
    every(1.day, 'midnight.testjob17', :at => '08:00')
    every(1.day, 'midnight.testjob18', :at => '08:30')
    every(1.day, 'midnight.testjob19', :at => '09:00')
    every(1.day, 'midnight.testjob20', :at => '09:30')
    every(1.day, 'midnight.testjob21', :at => '10:00')
    every(1.day, 'midnight.testjob22', :at => '10:30')
    every(1.day, 'midnight.testjob23', :at => '11:00')
    every(1.day, 'midnight.testjob24', :at => '11:30')
    every(1.day, 'midnight.testjob25', :at => '12:00')
    every(1.day, 'midnight.testjob26', :at => '12:30')
    every(1.day, 'midnight.testjob27', :at => '13:00')
    every(1.day, 'midnight.testjob28', :at => '13:30')
    every(1.day, 'midnight.testjob29', :at => '14:00')
    every(1.day, 'midnight.testjob30', :at => '14:30')
    every(1.day, 'midnight.testjob31', :at => '15:00')
    every(1.day, 'midnight.testjob32', :at => '15:30')
    every(1.day, 'midnight.testjob33', :at => '16:00')
    every(1.day, 'midnight.testjob34', :at => '16:30')
    every(1.day, 'midnight.testjob35', :at => '17:00')
    every(1.day, 'midnight.testjob36', :at => '17:30')
    every(1.day, 'midnight.testjob37', :at => '18:00')
    every(1.day, 'midnight.testjob38', :at => '18:30')
    every(1.day, 'midnight.testjob39', :at => '19:00')
    every(1.day, 'midnight.testjob40', :at => '19:30')
    every(1.day, 'midnight.testjob41', :at => '20:00')
    every(1.day, 'midnight.testjob42', :at => '20:30')
    every(1.day, 'midnight.testjob43', :at => '21:00')
    every(1.day, 'midnight.testjob44', :at => '21:30')
    every(1.day, 'midnight.testjob45', :at => '22:00')
    every(1.day, 'midnight.testjob46', :at => '22:30')
    every(1.day, 'midnight.testjob47', :at => '23:00')
    every(1.day, 'midnight.testjob48', :at => '23:30')
end