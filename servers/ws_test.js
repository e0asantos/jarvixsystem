import { io } from "socket.io-client";

const socket = io("http://localhost:3002");

// connection
socket.on("connect", () => {
  console.log("connected");
});

// receiving an event
socket.on("foo", (value) => {
  // ...
  console.log(value);
});

// sending an event
socket.emit("bar", "abc");