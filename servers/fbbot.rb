# require "koala"

# @graph = Koala::Facebook::API.new("CAAFFJdrXiPABAGcTxJHktn8ZAEuHSe4l7fVS6cc7o5OQtRhn0m9IGYkT9WYIa4p2CgAZAWRyh7689hQla4hZCspGTykqSrAZCDB1TIFh9dq7zNKxrhYe4G0oRmjXlZB3J8oM4ZCntEkmFNj3ZBrwG8yiLvzmZAQvCkWZCHAsS1AD4yccoxLZCuyzNYj51cIB2yjZCqho4hjxTsOdb23UXY6AxX90JsAZAeqdXTYq2pl8LHh6iQZDZD")

# profile = @graph.get_object("me")
# friends = @graph.get_connections("me", "friends")
# # @graph.put_connections("me", "feed", :message => "I am writing on my wall!")
# puts friends.inspect
# @listaDePaginas=@graph.get_object("me/accounts")
# @yoAdministro=@graph.get_object("me/likes")

# @listaDePaginas.each do |singlePage|
# puts singlePage["name"]+"---"+singlePage["id"]
# end
# objeto=@graph.put_object("368487143263665", "feed", :message => "Qiwo")

# # You can specify version either globally:
# Koala.config.api_version = "v2.0"
# # or on a per-request basis
# @graph.get_object("me", {}, api_version: "v2.0")

require 'facebook/messenger'

include Facebook::Messenger

Bot.on :message do |message|
  puts "Received '#{message.inspect}' from #{message.sender}"

  case message.text
  when /hello/i
    message.reply(
      text: 'Hello, human!',
      quick_replies: [
        {
          content_type: 'text',
          title: 'Hello, bot!',
          payload: 'HELLO_BOT'
        }
      ]
    )
  when /something humans like/i
    message.reply(
      text: 'I found something humans seem to like:'
    )

    message.reply(
      attachment: {
        type: 'image',
        payload: {
          url: 'https://i.imgur.com/iMKrDQc.gif'
        }
      }
    )

    message.reply(
      attachment: {
        type: 'template',
        payload: {
          template_type: 'button',
          text: 'Did human like it?',
          buttons: [
            { type: 'postback', title: 'Yes', payload: 'HUMAN_LIKED' },
            { type: 'postback', title: 'No', payload: 'HUMAN_DISLIKED' }
          ]
        }
      }
    )
  else
    message.reply(
      text: 'You are now marked for extermination.'
    )

    message.reply(
      text: 'Have a nice day.'
    )
  end
end

Bot.on :postback do |postback|
  case postback.payload
  when 'HUMAN_LIKED'
    text = 'That makes bot happy!'
  when 'HUMAN_DISLIKED'
    text = 'Oh.'
  end

  postback.reply(
    text: text
  )
end

Bot.on :delivery do |delivery|
  puts "Delivered message(s) #{delivery.ids}"
end