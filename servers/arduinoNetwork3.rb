require 'net/http'
require 'net/https'
require 'rubygems'
require 'eventmachine'
require 'yaml'
require 'active_record'
require 'logger'
require 'json'
require 'uuidtools'
require '/usr/src/jarvixServer/db/model/api_token_user_interface.rb'
require '/usr/src/jarvixServer/db/model/anet_log.rb'
require '/usr/src/jarvixServer/db/model/sys_log.rb'
require '/usr/src/jarvixServer/db/model/anet_command.rb'
require '/usr/src/jarvixServer/db/model/anet_value.rb'
require '/usr/src/jarvixServer/jarvixSystem/ILineAccess.rb'
require '/usr/src/jarvixServer/db/model/Jarvix_Custom_Property.rb'
require '/usr/src/jarvixServer/db/model/anet_device.rb'
require '/usr/src/jarvixServer/db/model/JarvixPersonality.rb'
require '/usr/src/jarvixServer/db/model/sys_user.rb'
require '/usr/src/jarvixServer/db/model/sys_user_phone.rb'
require '/usr/src/jarvixServer/lib/twitter/JTwitter.rb'
require '/usr/src/jarvixServer/lib/google/GtalkHelper.rb'
require '/usr/src/jarvixServer/db/model/sys_phrases.rb'
require '/usr/src/jarvixServer/db/model/qiwo_enterprise.rb'
require '/usr/src/jarvixServer/db/model/qiwo_product.rb'
require '/usr/src/jarvixServer/db/model/nfc_reader.rb'
require '/usr/src/jarvixServer/db/model/qiwo_log_kart.rb'
require '/usr/src/jarvixServer/db/model/sys_dhcp.rb'
require '/usr/src/jarvixServer/db/model/sys_video.rb'
require '/usr/src/jarvixServer/db/model/system_activity.rb'
require "uuidtools"
require 'pusher'
require "base64"
require 'uri'
require 'json'
require 'telegram/bot'
# sendgrid-ruby (4.0.8)
require 'sendgrid-ruby'
include SendGrid
#Aquiring the needed models from the app




  class EchoServer < EM::Connection
    attr_accessor :techname
    attr_accessor :name
    attr_accessor :anet_id
    attr_accessor :isclosed
    attr_accessor :incomingString
    attr_accessor :incomingStringCopy
    attr_accessor :utimer
    attr_accessor :receiving_file
    attr_accessor :receiving_img
    attr_accessor :receiving_contents
    attr_accessor :ping
    attr_accessor :streaming_channel


    def initialize 
      comm_inactivity_timeout = 20
      pending_connect_timeout = 20
    end
    
    def self.list
      @list ||= []
    end

    def self.offlineTimer
      if @offline == nil
        puts "\u001b[31m>>>>offlineTimer::start\u001b[0m"
        @offline = EventMachine::PeriodicTimer.new(15) do
          # verify if the device has been connected
          EchoServer.list.each{ |c| 
          if c.ping != nil
            if (c.ping+60.seconds)<DateTime.now
              puts "\u001b[31m>>OFFLINE detected"
              puts c.techname
              puts "\u001b[0m"
            end
          end
        }
        end
      end
    end

    def self.devicesLogged
      if @devicesLogged==nil
        @devicesLogged=Hash.new
      end
      return @devicesLogged
    end
      
    def is_numeric(obj) 
      obj.to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/) == nil ? false : true
    end

    def lineAccessToJarvix(laUser)
      if laUser == nil
        return nil
      end
      userPhone=SysUserPhone.where(:sys_user=>laUser,:sys_phone_type=>1)
      # si no hay telefonos usar el id del usuario de suitch como telefono
      if userPhone.length == 0
        fakePhone = SysUserPhone.new
        fakePhone.sys_phone_type = 1
        fakePhone.number = "00000"+laUser.to_s
        fakePhone.sys_user = laUser
        fakePhone.save
        userPhone=SysUserPhone.where(:sys_user=>laUser,:sys_phone_type=>1)
      end
      allPhones=[]
      # puts userPhone.inspect
      userPhone.each do |singlePhone|
        allPhones.push(singlePhone.number)
      end
      loadOntosEquals=Jarvix_Custom_Property.where(:property_name=>"number",:property_value=>allPhones).first
      # si es nil no tiene telefonos, o no los ha puesto, entonces ahora buscarlo por personalidad ligada con el id
      # de suitch, eso evitarÃ­a crear multiples personalidades
      # look_for_personality = JarvixPersonality.where(:suitch_user_id => laUser).last
      # if look_for_personality != nil
      #   loadOntosEquals 
      # end
      if loadOntosEquals==nil
        # creamos la personalidad
        personat=Jarvix_Personality.new
        personat.my_name="the network"
        personat.my_noun="it"
        personat.suitch_user_id = laUser
        personat.sex="m"
        personat.save
        
        # creamos una propiedad
        loadOntosEquals=Jarvix_Custom_Property.new
        loadOntosEquals.property_name="number"
        loadOntosEquals.property_value=allPhones[0]
        loadOntosEquals.id_user=personat.id
        loadOntosEquals.save
      end
      # puts loadOntosEquals.inspect
      # puts loadOntosEquals.inspect
      return loadOntosEquals.id_user
    end
    

    def notificateOnChange(aDevice,msgToSend)
      if EchoServer.devicesLogged[@techname].id_owner == nil
        return nil
      end
      accessLine=ILineAccess.new(lineAccessToJarvix(EchoServer.devicesLogged[@techname].id_owner))
      # puts Sys_User.where(:id=>EchoServer.devicesLogged[@techname].id_owner).last.email
      # hay que enviar notificaciones en caso de que el dispositivo este marcado como para recibir notificaciones
      if aDevice.notify_hangout==1
        puts "TYRING_HANGOUT"
        # conectar con el servidor remoto
        # uri = URI.parse("http://zorrometro-cware.rhcloud.com/hangouts.json") 
        # response = Net::HTTP.post_form(uri, {"hangout"=>{"frommail"=>"asb.studios@gmail.com", "tomail"=>"amarramizapato@gmail.com", "token"=>"ya29.LgBIa6BfQViJCBoAAACCbIhCqPPQn_QIet38vDiWKi9LYyg9SPKLhBBIA_tZPw", "message"=>"123"}})
        # puts response.inspect
        # primero cargamos el usuario del zorro para que pueda enviar notificaciones
        # hangoutToken=Api_Token_User_Interface.where("sys_user=33 and api_interface=6")[0]
        # puts hangoutToken.inspect
        # hangoutToken.forceUpdateToken()
        # objeto=Hash.new
        # objeto[:frommail]="asb.studios@gmail.com"
        # objeto[:tomail]="amarramizapato@gmail.com"
        # objeto[:token]="ya29.LgAkTHVm6RrYsBoAAABrqY7E2Xf4rIPJNy0aGpENblAuYlVI7e8xW4gE2ujveA"
        # objeto[:message]="123"

        # puts objeto.to_json
        # # response = Net::HTTP.post_form(uri, {"hangout":{"frommail": "asb.studios@gmail.com", "tomail": "amarramizapato@gmail.com", "token":"ya29.LgBIa6BfQViJCBoAAACCbIhCqPPQn_QIet38vDiWKi9LYyg9SPKLhBBIA_tZPw", "message"=>"123"}})
        # # puts response


        # # postData = Net::HTTP.post_form(URI.parse('http://localhost:3000/hangouts.json'), {:hangout=>{"frommail"=>'asb.studios@gmail.com'}})
        # JSON.load `curl -H 'Content-Type:application/json' -H 'Accept:application/json' -X POST http://zorrometro-cware.rhcloud.com/hangouts.json -d '#{objeto.to_json}'`
      end
      if aDevice.notify_fb==1
        puts "TYRING_FB_POST"
        # accessLine.runApp("facebook.post","Device was connected to the network")+"\n"
      end
      if aDevice.notify_email==1
        puts "TYRING_EMAIL"
        # hay dos formas de enviar el mail, con el mail registrado del usuario
        # extraer los mail
        # clientMail = SendGrid::API.new(api_key: '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')
        # singleMail = SendGrid::Mail.new
        # singleMail.from = Email.new(email: 'noreply@consultware.mx')
        # singleMail.subject = 'Suitch - Connected'
        # personalization = Personalization.new
        #   finalArrayEmail=[]
        #   Sys_User.where(:id=>EchoServer.devicesLogged[@techname].id_owner).each do |singleEmail|
        #     personalization.to=Email.new(email: singleEmail.email, name: "Suitch usuario")
        #   end
        #   singleMail.personalizations=personalization
        #   content = SendGrid::Content.new(type: 'text/plain', value: msgToSend)
        #   singleMail.contents = content;
        #   response=clientMail.client.mail._('send').post(request_body: singleMail.to_json)
        #   puts response.status_code
        #   puts response.body
        #   puts response.headers
        begin
          uri = URI.parse("http://localhost:3002/mail-notification") 
          header = {'Content-Type'=> 'text/json'}
          https = Net::HTTP.new(uri.host,uri.port)
          # https.use_ssl = true
          req = Net::HTTP::Post.new(uri)
          req['Content-Type'] = 'application/json'
          finalArrayEmail=[]
          Sys_User.where(:id=>EchoServer.devicesLogged[@techname].id_owner).each do |singleEmail|
            finalArrayEmail.push({"email": singleEmail.email, "name": "Dear #{singleEmail.name}"})
          end
          # The body needs to be a JSON string, use whatever you know to parse Hash to JSON
          req.body = {recipients:finalArrayEmail,template:"connection_v1",device:EchoServer.devicesLogged[@techname].object}.to_json
          # Send the request
          response = https.request(req)
          puts response.body
        rescue

        end
      end
      if aDevice.notify_twitter==1
        send_data accessLine.runApp("twitter.publish",msgToSend)+"\n"
        puts "TYRING_TWITTER"
      end
      if aDevice.notify_fbinbox==1
        # sendInboxToId([EchoServer.devicesLogged[@techname].id_owner],msgToSend)
        fbtoken = Api_Token_User_Interface.where(:sys_user => EchoServer.devicesLogged[@techname].id_owner, :api_interface => 16)
        fbtoken.each do |singleInbox|
          messengerBot(msgToSend, singleInbox.uid)
        end
        puts "TYRING_FB_INBOX"
      end
      if aDevice.notify_telegram == 1
        puts "TYRING_TELEGRAM"
        tokenTelegram = Api_Token_User_Interface.where(:sys_user=>EchoServer.devicesLogged[@techname].id_owner,:api_interface=>15)
        tokenTelegram.each do |singleTelegram|
          Telegram::Bot::Client.run("982932162:AAHe5ja0swjNtu5pBQPzrDDgIcHyLg55QkI") do |bot|
            bot.api.send_message(chat_id: singleTelegram.uid, text: msgToSend)
          end
        end
      end
    end
    def messengerBot(message, user) 
      messageData = Hash.new
      messageData["recipient"]=Hash.new
      messageData["recipient"]["id"] = user
      messageData["message"]=Hash.new
      messageData["message"]["text"] = message
      messageData["messaging_type"] = "UPDATE"
      body = messageData.to_json
      uri = URI.parse("https://graph.facebook.com/v4.0/me/messages")
      https = Net::HTTP.new(uri.host,uri.port)
      https.use_ssl = true
      req = Net::HTTP::Post.new(uri.path+"?access_token=EAAFFJdrXiPABAFB3a9U1ZAqcZC7TxF3smIPGtAbN8ZBXgO5EQZA4BcdAacdFXNW8IrhUE6weDZA8rVnUHBLvnjeLa4Y4DWk0ASg7fUcRm6JWXPyR5E6SN2i5NVEHbRPWKPIaKTfkLhGlvg0KN19SQFwtFvKKOnwJl3x3ZAEhjVQrdt0YOu0pMl", initheader = {'Content-Type' =>'application/json'})
      req.body = messageData.to_json
      
      res = https.request(req)
      puts "Response #{res.code} #{res.message}: #{res.body}"
    end
   
    def post_init (*args)
      @isclosed=0
      @techname = "anet_#{rand(99999)}"
      @name = "anet_#{rand(99999)}"
      puts "entrando:"+@techname.to_s
      #EchoServer.list.each{ |c| c.send_data "#{@name} has joined.\n" }
      EchoServer.list << self
      EchoServer.devicesLogged[@techname]="."
      send_data "TOKEN?\n"
      # EchoServer.offlineTimer()

      micro_verification = EventMachine::Timer.new(15) do
        # puts "VERIFY_CONNECTION"
        EchoServer.list.each{ |c| 
          if EchoServer.devicesLogged[c.techname] != "."
            puts ">>>>>>verify"
            # then this device is online
            # verify log DB and update accordingly
            lastOnlineVerification = AnetLog.where(:arduino_id => EchoServer.devicesLogged[c.techname].id).order("created_at ASC").last
            if lastOnlineVerification != nil
              if lastOnlineVerification.connection_type == 0
                # if it was a failed log, then correct that log session
                lastOnlineVerification.connection_type = 1
                lastOnlineVerification.save
              end
            end
          end
        }
      end
      # silent_disconnection = EventMachine::Timer.new(120) do
      #   # puts "FORCE_DISCONNECTION"
      #   # puts "disconnection>>"
      #   EchoServer.list.each{ |c|             
      #       if EchoServer.devicesLogged[c.techname] == "."
      #         # then this device never got connected
      #         # let's kill it
      #         # puts c.name
      #         puts ">>closing2"
      #         puts c.inspect
      #         puts "<<closing2"
      #         c.isclosed = 1
      #         c.close_connection
      #         # puts "......"
      #       end
      #   }
      #   # puts "disconnection<<"
      # end
      
    end

    def unbind
      puts "\u001b[33munbinding >>>"
      puts EchoServer.devicesLogged[@techname].inspect
      @isclosed=1 
      if @anet_id!=nil and @anet_id!=1
        begin
          uri = URI.parse("http://localhost:3002/mail-notification") 
          header = {'Content-Type'=> 'text/json'}
          https = Net::HTTP.new(uri.host,uri.port)
          # https.use_ssl = true
          req = Net::HTTP::Post.new(uri)
          req['Content-Type'] = 'application/json'
          finalArrayEmail=[]
          Sys_User.where(:id=>EchoServer.devicesLogged[@techname].id_owner).each do |singleEmail|
            finalArrayEmail.push({"email": singleEmail.email, "name": "Dear #{singleEmail.name}"})
          end
          # The body needs to be a JSON string, use whatever you know to parse Hash to JSON
          req.body = {recipients:finalArrayEmail,template:"connection_v1",device:EchoServer.devicesLogged[@techname].object}.to_json
          # Send the request
          response = https.request(req)
          puts response.body
        rescue

        end
          AnetLog.new(:arduino_id=>@anet_id,:connection_type=>0).save
      end
      puts "salio:"+@name.to_s
      puts "\u001b[0m"

      begin
        if EchoServer.devicesLogged[@techname] != "."
        uri = URI.parse("http://localhost:3002/update-data") 
        header = {'Content-Type'=> 'text/json'}
        https = Net::HTTP.new(uri.host,uri.port)
        # https.use_ssl = true
        req = Net::HTTP::Post.new(uri)
        req['Content-Type'] = 'application/json'
        # The body needs to be a JSON string, use whatever you know to parse Hash to JSON
        req.body = {user_channel:@streaming_channel,token: EchoServer.devicesLogged[@techname].token,type:'status'}.to_json
        # Send the request
        response = https.request(req)
        puts response.body
        end
      rescue

      end

      begin
        if EchoServer.devicesLogged[@techname] != "."
          # notificateOnChange(EchoServer.devicesLogged[@techname],"Device "+EchoServer.devicesLogged[@techname].object+" just disconnected at "+EchoServer.devicesLogged[@techname].location+"! :o -@suitch_")  
        end
      rescue Exception => e
        
      end

      
      puts "UNBDING_x"
      EchoServer.list.delete self
      close_connection
      #EchoServer.list.each{ |c| c.send_data "#{@name} has left.\n" }
    end

    def receive_data(data)
      @buffer ||= '' # Initialize buffer if not already done
      @buffer += data # Append the incoming data to the buffer
    
      # Split the buffer into commands based on \r or \n delimiters
      commands = @buffer.split(/[\r\n]+/)
      if @buffer[-1] =~ /[\r\n]/ # Check if the last character is \r or \n
        # If true, all commands are complete, clear the buffer
        @buffer = ''
      else
        # If false, the last command is incomplete, keep it in the buffer for next time
        @buffer = commands.pop
      end
    
      commands.each do |command|
        process_command(command) unless command.empty?
      end
    end

    def process_command data
      begin
        get_peername
      rescue
        # all of a sudden the device disconnected
        return
      end
      begin
        port2, ip2 = Socket.unpack_sockaddr_in(get_peername)
      rescue
        # all of a sudden the device disconnected
        return
      end
      port, ip = Socket.unpack_sockaddr_in(get_peername)
      # puts "got from #{ip}:#{port}"
      
      if (data=="\n" or data=="\r") and (@incomingString==nil or @incomingString=="")
          return
      end
      if @incomingString==nil
        @incomingString=""
      end
      
      
      
      
      @incomingString=@incomingString+data

      # if @utimer!=nil
      #   @utimer.cancel  
      # end
      puts "incoming data:"+data.inspect
      
      
      # @utimer = EventMachine::Timer.new(60) do
      #   puts "TIMEOUT---->"+@name
      #   send_data "PONG\n\r"  
      #   # unbind
      # end
      # REMOVING END OF LINE CHARACTERS identification since it was already taken care of
      # if @incomingString.index("\n")!=nil or @incomingString.index("\r")!=nil
      #     if @incomingString.index("Read failed ")!=nil
      #       @incomingString=@incomingString.delete "Read failed "
      #     end
      #     if @incomingString.index("Error. Failed read page ")!=nil
      #       @incomingString=@incomingString.delete "Error. Failed read page "
      #     end
      #     #data=data[0..-2].downcase
      #     @incomingStringCopy = @incomingString
      #     @incomingString=@incomingString.delete "\n"
      #     @incomingString=@incomingString.delete "\r"

      # else
      #   puts "NO_END"
      #  return
      # end
      puts "EMPIEZA:"+@incomingString.inspect
      # if data.index("\r")!=nil
      #     #data=data[0..-2].downcase
      #     data=data[0..-2]
      # end
      if EchoServer.devicesLogged[@techname]=="."
        puts "tokenizing.."
        #esta. recibiendo token
        #buscar si existe el dispositivo
        
        device=@incomingString.split("/")
        if device.length==2 
          puts "device[0]-->"
          puts device[0]
          puts "device[1]-->"
          puts device[1]
          aDevice=AnetDevice.where(:token=>device[0],:secret_token=>device[1]).last
          # puts aDevice.inspect
          if aDevice!=nil
              #buscar si ya esta loggeado, en ese caso desloggear
              # no puede haber varias conexiones con el mismo token
            alreadyLogged=false
            EchoServer.list.each{ |c| 
                if c.name == "anet_"+device[0] and c.isclosed==0
                  puts ">>closing_clone"
                  puts aDevice.inspect
                  puts "<<closing_clone"
                    c.isclosed=1
                    c.close_connection
                end
            }
            if alreadyLogged==false
                @name = "anet_"+device[0]
                EchoServer.devicesLogged[@techname]=aDevice
                @anet_id=aDevice.id
                if @anet_id!=1
                   port, ip = Socket.unpack_sockaddr_in(get_peername)
                   # detect when we are logging a camera to create the containing folder
                  end
                if aDevice.version == "camv1"
                  Dir.mkdir('/home/suitch/public/recordings/'+EchoServer.devicesLogged[@techname].token) unless File.exists?('/home/suitch/public/recordings/'+EchoServer.devicesLogged[@techname].token)
                end
                
                if aDevice.id_owner==nil
                  AnetLog.new(:arduino_id=>aDevice.id,:connection_type=>1,:created_at=>DateTime.now,:mip=>ip).save
                  # lets look for the device itself and update the latest IP that got connected
                  aDevice.mip = ip
                  aDevice.save
                  puts "WLCMEX"
                  send_data "WLCMEX\n"  
                else
                  AnetLog.new(:arduino_id=>aDevice.id,:connection_type=>1,:created_at=>DateTime.now,:mip=>ip).save
                  # lets look for the device itself and update the latest IP that got connected
                  aDevice.mip = ip
                  aDevice.save
                  # if the device has owner, then get the owner channel
                  prechannel=Sys_User.where(:id=>aDevice.id_owner).last
                  if prechannel!=nil
                    @streaming_channel=prechannel.user_id
                  end
                  begin
                    uri = URI.parse("http://localhost:3002/update-data") 
                    header = {'Content-Type'=> 'text/json'}
                    https = Net::HTTP.new(uri.host,uri.port)
                    # https.use_ssl = true
                    req = Net::HTTP::Post.new(uri)
                    req['Content-Type'] = 'application/json'
                    # The body needs to be a JSON string, use whatever you know to parse Hash to JSON
                    req.body = {user_channel:@streaming_channel,token: EchoServer.devicesLogged[@techname].token,type:'status'}.to_json
                    # Send the request
                    response = https.request(req)
                    puts response.body
                  rescue
        
                  end
                  puts "WLCME"
                  send_data "WLCME\n"
                end
               
                
                @incomingString=""
                begin
                  # notificateOnChange(aDevice,"The network says that a Device "+aDevice.object+" connected at "+aDevice.location+"! :) -@suitch_")  
                  puts "notificateOnChange(x_x)"
                rescue Exception => e
                  puts "ERROR_NOTIFICATING"
                end
                
            else 
              puts "BUSY"
                send_data "BUSY\n"
                puts ">>closing-busy"
                close_connection
            end
          else
            puts "USR_ERROR"
            send_data "USR_ERROR\n"
            @incomingString=""
            unbind
            # close_connection
          end
      else
        puts "token not properly set...closing"
        @incomingString=""  
        unbind
      end
    else
      if @incomingString.index("ping")!=nil or @incomingString.index("PING") != nil
        puts "pinging..."
        send_data "PONG\n"
      end
    end


      
    (@buf ||= '') << @incomingString
      while line = @buf.slice!(/(.+)\r?\n/)
        if line =~ %r|^/nick (.+)|
          new_name = $1.strip
          (EchoServer.list - [self]).each{ |c| c.send_data "#{@name} is now known as #{new_name}\n" }
          @name = new_name
        elsif line =~ %r|^/quit|
          close_connection
        else
          # (EchoServer.list - [self]).each{ |c| c.send_data "#{@name}: #{line}" }
        end
      end
      
    end
  end

  


  EventMachine::run {
    
    EventMachine::start_server "0.0.0.0", 3010, EchoServer
    

    puts '                                __                       __    
_____              ____   _____/  |___  _  _____________|  | __
\__  \    ______  /    \_/ __ \   __\ \/ \/ /  _ \_  __ \  |/ /
 / __ \_ /_____/ |   |  \  ___/|  |  \     (  <_> )  | \/    < 
(____  /         |___|  /\___  >__|   \/\_/ \____/|__|  |__|_ \
     \/               \/     \/                              \/'
    puts 'running Suitch Network'
    
  }