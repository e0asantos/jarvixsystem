require 'rubygems'        # if you use RubyGems
require 'daemons'
#this server runs as ----> ruby arduinoNetworkDaemon.rb start

  #options = {
  #     :app_name           => "my_app",
  #     :ARGV               => ['start', '-f', '--', 'param_for_myscript']
  #     :dir_mode           => :script,
  #     :dir                => 'pids',
  #     :multiple           => true,
  #     :pid_delimiter      => '.n',
  #     :ontop              => true,
  #     :shush              => false,
  #     :mode               => :exec,
  #     :backtrace          => true,
  #     :monitor            => true,
  #     :logfilename        => 'custom_logfile.log',
  #     :output_logfilename => 'custom_outputfile.txt'
  #   }
  #
  #   Daemons.run(File.join(File.dirname(__FILE__), 'myscript.rb'), options)
  #
  options = {
      :app_name           => "my_app_cron",
      :backtrace          => true,
      :monitor            => true,
      :logfilename        => 'cron_custom_logfile.log',
      :output_logfilename => 'cron_custom_outputfile.txt'
    }
Daemons.run('cronJobCommandCustom.rb', options)