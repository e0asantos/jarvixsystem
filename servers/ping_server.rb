#!/usr/bin/env ruby

require 'net/ping'

def up?(host)
    check = Net::Ping::External.new(host)
    check.ping?
end

puts up?("10.0.1.4")