
require 'active_record'
require 'simple-rss'
require 'open-uri'
require 'xmlsimple'
# require 'xmpp4r_facebook'
# require 'xmpp4r/roster'
# require 'fb_graph'
require '/usr/src/jarvixServer/db/model/anet_command.rb'
require '/usr/src/jarvixServer/db/model/earthquake.rb'
require '/usr/src/jarvixServer/db/model/anet_device.rb'
require '/usr/src/jarvixServer/db/model/sys_user.rb'
require '/usr/src/jarvixServer/db/model/geo_setting.rb'
require '/usr/src/jarvixServer/db/model/feed_reader.rb'
# require '/usr/src/jarvixServer/db/model/qiwo_enterprise.rb'
# require '/usr/src/jarvixServer/db/model/qiwo_product.rb'
require '/usr/src/jarvixServer/db/model/summary.rb'
require '/usr/src/jarvixServer/db/model/api_token_user_interface.rb'
# require '/usr/src/lineAccess/app/models/qiwo_subscription.rb'
# require '/usr/src/lineAccess/app/models/qiwo_user_subscription.rb'
require '/usr/src/jarvixServer/db/model/sys_schedule.rb'
require 'clockwork'
require 'active_support/time' # Allow numeric durations (eg: 1.minutes)
#Daemonization
#
#
#You need daemons gem to use clockworkd. It is not automatically installed, please install by yourself.
#
#Then,
#
#clockworkd -c YOUR_CLOCK.rb start
#For more details, see help shown by clockworkd.
module Clockwork
    # configure do |config|
    #     config[:sleep_timeout] = 5
    #     config[:logger] = Logger.new('myclock.txt')
    #     config[:tz] = 'EST'
    #     config[:max_threads] = 2
    #     config[:thread] = true
    #   end
  handler do |job, time|
    puts "Running #{job}"
    #identificar cambio de dia para desconectar las cuentas de qiwo
    
    if job=="anetCommands"
        puts "phase 1"
        # ver si tenemos mensajes globales por enviar
        mtime = Time.now
        mtime = mtime.change(:usec => 0)
        mtime = mtime.change(:sec => 0)
        SysSchedule.all.each do |singleSchedule|
            # mtime.sec = 0
            # mtime.usec = 0
            # mtime = mtime – (mtime.sec).seconds
            puts mtime
            puts singleSchedule.schedule
            if singleSchedule.schedule == mtime
                s = TCPSocket.new 'localhost', 3000
                while line = s.gets # Read lines from socket
                    puts line.inspect
                    if line.index("TOKEN?")!=nil
                        puts "enviando"
                        s.puts "025621de/d822339f" 
                    elsif line.index("WLCME")!=nil
                        puts "WLCME."
                        # commandsToExecute.each do |singleCommand|
                        #     dispositivo=AnetDevice.find_by_id(singleCommand.arduino_id)
                        #     if dispositivo!=nil
                        #         puts dispositivo.token
                        #         s.puts "sendto "+dispositivo.token+" "+singleCommand.command_data	
                        #     end
                        # end
                        dispositivo = AnetDevice.find_by_id(singleSchedule.device_id)
                        mcommand = AnetCommand.find_by_id(singleSchedule.command)
                        if dispositivo != nil and mcommand != nil
                            s.puts "sendto "+dispositivo.token+" "+mcommand.command_data	
                        end
                        break
                    end
                end
                s.close
            end
        end
    end
  end

  # handler receives the time when job is prepared to run in the 2nd argument
  # handler do |job, time|
  #   puts "Running #{job}, at #{time}"
  # end

#  every(10.seconds, 'frequent.job')
#  every(3.minutes, 'less.frequent.job')
    every(1.minutes, 'anetCommands')
#  every(1.hour, 'hourly.job')

   
end