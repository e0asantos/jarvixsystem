#!/usr/bin/env perl
use warnings;
use strict;
use CGI::Util qw(escape);
use File::Copy qw(move);
use File::Temp qw(tempfile);
use LWP::UserAgent;
use DBI;
use utf8;
use Data::Dumper;


my $uaresponse;
my $audio;
my $language = "es";
my $intkey = "#";
my $grammar    = "builtin:dictation"; #"builtin:search";
my $ua_timeout = 10;
my $results    = 1;
my $pro_filter = 0;
my $ua;
my $url;
my $fh;
my $host       = "www.google.com/speech-api/v1/recognize";
my $atsresponse="NONE";

$url = "https://" . $host;
$ua  = LWP::UserAgent->new(ssl_opts => {verify_hostname => 1});
$language = escape($language);
$grammar  = escape($grammar);

$url .= "?xjerr=1&client=chromium&lang=$language&pfilter=$pro_filter&lm=$grammar&maxresults=$results";
$ua->agent("Mozilla/5.0 (X11; Linux) AppleWebKit/537.1 (KHTML, like Gecko)");
$ua->env_proxy;
$ua->timeout($ua_timeout);
open($fh, "<", $ARGV[0]) or die "Can't read file: $!";
$audio = do { local $/; <$fh> };
close($fh);
$uaresponse = $ua->post(
	"$url",
	Content_Type => "audio/x-flac; rate=8000",
	Content      => "$audio",
);
print $uaresponse->content