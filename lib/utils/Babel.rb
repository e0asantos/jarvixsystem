#!/usr/bin/env ruby
# encoding: utf-8
#Encoding.default_external = Encoding::UTF_8
#Encoding.default_internal = Encoding::UTF_8
Encoding.default_external = "utf-8"
Encoding.default_internal  = "utf-8"

require 'rubygems'
require 'bing_translator'
require '/usr/src/jarvixServer/db/model/Jarvix_Conversation_Log.rb'

class Babel
	attr_accessor:translator
	attr_accessor:fromPhone
	attr_accessor:call_controller
	attr_accessor:conversation_log
	
	def initialize
		@translator = BingTranslator.new('lineaccessmx', 'k+Q3xRzEgOImBpDLGf/h1Xus3Rbby6WcEid4Khbsiv4=')
		@call_controller="NONE"
	end
	def _translate(thingToTranslate)
		#obtener la ultima palabra que es el idioma
		words=thingToTranslate.split(" ")
		langToTranslate=words.pop()
		shortLang="en"
		if langToTranslate.downcase=="español"
			shortLang="es"
		elsif langToTranslate.downcase=="inglés"
			shortLang="en"
		elsif langToTranslate.downcase=="ingles"
			shortLang="en"
		elsif langToTranslate.downcase=="alemán"
			shortLang="de"
		elsif langToTranslate.downcase=="aleman"
			shortLang="de"
		elsif langToTranslate.downcase=="fránces"
			shortLang="fr"
		elsif langToTranslate.downcase=="frances"
			shortLang="fr"
		end
		
		spanisht = @translator.translate words.join(" "), :from => 'es', :to => shortLang
		# puts 'exec agi "googletts.agi,'+spanisht+','+shortLang+'"'
		if @conversation_log!=nil
			@conversation_log.response=spanisht
			@conversation_log.save	
		end
		
		if @call_controller!="NONE"
			@call_controller.execute 'agi', 'googletts.agi, "'+spanisht+'",'+shortLang
		end
		return spanisht
	end
end