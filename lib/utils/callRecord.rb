#!/usr/bin/env ruby
# encoding: utf-8
#Encoding.default_external = Encoding::UTF_8
#Encoding.default_internal = Encoding::UTF_8
Encoding.default_external = "utf-8"
Encoding.default_internal  = "utf-8"
require 'rubygems'
require 'savon'
require 'uri'
require 'net/http'
require 'json'
require 'active_record'
require 'adhearsion-asterisk'
require '/usr/src/jarvixServer/db/model/sys_phrases.rb'

class CallRecord

  attr_accessor:json_result
  attr_accessor:text
  attr_accessor:db_message
  attr_accessor:continue_conversation
  attr_accessor:call_id
  attr_accessor:callController

  def initialize(lang, userphone,callid=nil,ccontroller)
      puts "+++++++++++++ CALL RECORD +++ lang:"+lang.inspect+"  userphone:"+userphone.inspect+"    callid:"+callid.inspect+"     ccontroller:"+ccontroller.inspect
    @callController=ccontroller
    if callid!=nil
          @call_id=callid
    end

    @text="NONE"
    @continue_conversation=false
    userlang=lang
    uid=UUIDTools::UUID.random_create.to_s()
    tmpfile='/usr/src/lineAccess/public/calls/'+uid
    
    @callController.execute 'Record', tmpfile+'.wav', 3
    puts "converting"
    systemResult=false
    contador=0
    #se hace el grabado y se hace una peticion
    client = Savon.client(:wsdl=> "http://localhost:8000/?wsdl")
    response=client.call(:say_hello,message:{name:uid})

    if response.body==nil
      @text="NONE"
      @continue_conversation=true
    else
      # @text=response.body[:say_hello_response][:say_hello_result][:string] 
      mjson=JSON.parse(response.body[:say_hello_response][:say_hello_result][:string][13..-1])
      @text=mjson["result"][0]["alternative"][0]["transcript"]
      negativeText=". "+@text+" ."
      nuevaFrase=Sys_Phrase.new
      nuevaFrase.phrase=@text
      nuevaFrase.phone=userphone
      nuevaFrase.soundfile=uid
      nuevaFrase.callid=@call_id
      nuevaFrase.save
      @db_message=nuevaFrase
      if negativeText.downcase.index(" no ")!=nil or negativeText.downcase.index(" not ")!=nil or negativeText.downcase.index(" nada más ")!=nil or negativeText.downcase.index(" nothing else ")!=nil
        @continue_conversation=false
      else
        @continue_conversation=true
      end
    end
    
  end
end
