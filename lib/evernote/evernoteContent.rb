# require "digest/md5"

# # To include an attachment such as an image in a note, first create a Resource
# # for the attachment. At a minimum, the Resource contains the binary attachment
# # data, an MD5 hash of the binary data, and the attachment MIME type. It can also
# #/ include attributes such as filename and location.
# $filename = "/usr/src/apiManagerCore/resources/logo.png"
# $image = File.open($filename, "rb") { |io| io.read }
# $hashFunc = Digest::MD5.new

# def getResources()
# data = Evernote::EDAM::Type::Data.new
# data.size = $image.size
# data.bodyHash = $hashFunc.digest($image)
# data.body = $image

# resource = Evernote::EDAM::Type::Resource.new
# resource.mime = "image/png"
# resource.data = data
# resource.attributes = Evernote::EDAM::Type::ResourceAttributes.new
# resource.attributes.fileName = $filename

# return resource
# end

# def getContent(titleNote, bodyNote)
# # To display the Resource as part of the note's content, include an <en-media>
# # tag in the note's ENML content. The en-media tag identifies the corresponding
# # Resource using the MD5 hash.
# hashHex = $hashFunc.hexdigest($image)

# # The content of an Evernote note is represented using Evernote Markup Language
# # (ENML). The full ENML specification can be found in the Evernote API Overview
# # at http://dev.evernote.com/documentation/cloud/chapters/ENML.php

# 	#<table>
# 	#	<tr>
# 	#		<td><en-media width="60px"   type="image/png" hash="#{hashHex}"/></td> 
# 	#		<td> <span style="font-weight:bold;color:4FA22B;">#{titleNote}</span> </td>
# 	#	</tr>
# 	#</table>

# content = <<EOF
# <?xml version="1.0" encoding="UTF-8"?>
# <!DOCTYPE en-note SYSTEM "http://xml.evernote.com/pub/enml2.dtd">
# <en-note>
#     <en-media width="70px"   type="image/png" hash="#{hashHex}"/>
#     <br/>
#     #{bodyNote}<br/>
# </en-note>
# EOF

# return content
# end