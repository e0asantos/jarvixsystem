# require 'gmail'
# require 'gmail-contacts'
require 'active_support'
require 'rest_client'
require '/usr/src/jarvixServer/db/model/api_token_user_interface.rb'
require '/usr/src/jarvixServer/jarvixSystem/JarvixPerson.rb'
require '/usr/src/jarvixServer/jarvixSystem/Jarvix_Abstract_Object.rb'
require '/usr/src/jarvixServer/jarvixSystem/Jarvix_Custom_Object.rb'
require '/usr/src/jarvixServer/db/model/Jarvix_Custom_Property.rb'
require '/usr/src/jarvixServer/db/model/sys_user_phone.rb'
require '/usr/src/jarvixServer/db/model/sys_user.rb'
require '/usr/src/jarvixServer/lib/facebook/facebookClient.rb'
require '/usr/src/jarvixServer/lib/google/GtalkHelper.rb'

class JFacebook

	attr_accessor:fromIdUser
	attr_accessor:toIdUser
	attr_accessor:JObject
	attr_accessor:currentToken
	attr_accessor:fromPhone

	#lista de donde se debe escoger cuando hay varias opciones
	attr_accessor:optionsList
	#indice seleccionado cuando hay varias opciones
	attr_accessor:waitingSelectedIndex

	def _publish(msg)
		return finallySend(msg)
	end
	def _post(msg)
		return finallySend(msg)
	end
	def _post_wall(msg)
		return finallySend(msg)
	end
	def _create(msg)
		return finallySend(msg)
	end
	def _say(msg)
		return finallySend(msg)
	end
	def _write(msg)
		return finallySend(msg)
	end
	def _dictate(msg)
		return finallySend(msg)
	end
	
	def _chat(persona,msg)
		puts "---in_chat---"
		chatObject=ChatHelper.new
		chatObject.waitingSelectedIndex=waitingSelectedIndex
		chatObject.searchOnFB=optionsList
		selecciones=chatObject.start(@fromIdUser,persona,2)
		if selecciones==nil
			chatObject.sendMessage(msg)	
			return "You can write a new message once the other person responds"
		else
			puts "selections on chat"
			@optionsList=chatObject.searchOnFB
			return "-option"
		end
	end
	def finallySend(msg)
		# if @fromPhone==nil
		# 	@fromPhone=userPhone(@fromIdUser).number
		# end
		# @JObject=FacebookClient.new(@fromPhone)
		# @JObject.post_wall(msg)
		if @fromPhone==nil
			@fromPhone=userPhone(@fromIdUser)
		end
		if @fromPhone == nil
			return "FAILED"
		else 
			@JObject=FacebookClient.new(@fromPhone)
			@JObject.post_wall(msg)
			return "OK"
		end
		return "OK"
	end

	def jarvixToLineAccessUser(userID)
		#con el telefono podemos buscar el usuario
		lineAccessPhone=userPhone(userID)
		lineAccessUser=Sys_User.find_by_id(lineAccessPhone.sys_user)
	end

	def userPhone(userID)
		puts "---JFacebook-userPhone---"
		# person=JarvixPerson.new(userID)
		# #buscar telefono
		# personPhone=Jarvix_Custom_Object.where(:id_abstract_object=>1,:id_owner=>person.ontosEqualsArray)
		# personPhoneIds=idFromObjects(personPhone)
		# realPhone=Jarvix_Custom_Property.where(:property_name=>"number",:id_user=>person.ontosEqualsArray,:id_custom_object=>personPhoneIds).where("property_value!=1234567890").last
		# #con el telefono podemos buscar el usuario
		# lineAccessPhone=Sys_User_Phone.where(:number=>realPhone.property_value,:sys_phone_type=>1).last
		lineAccessPhone=Jarvix_Custom_Property.where(:property_name=>"number",:id_user=>userID).last
		if lineAccessPhone != nil 
			return lineAccessPhone.property_value
		end
		puts "---end JFacebook-userPhone---"
		return nil
	end
	def idFromObjects(objetos)
		returnIds=[]
		objetos.each do |objetoid|
			returnIds.push(objetoid.id)
		end
		return returnIds
	end
end