# autor :Héctor Acosta
# email :hacost@hotmail.com
# date  :2013-jul-17
# file  :core.rb  
# description: save notes, tweets, logs, words, metrics 

require 'date'  # Needed for Date
require 'time'
require '/usr/src/jarvixServer/lib/evernote/evernoteContent.rb'
require '/usr/src/jarvixServer/db/databaseConfig.rb'
require '/usr/src/jarvixServer/db/model/sys_user_phone.rb'
require '/usr/src/jarvixServer/db/model/api_token_user_interface.rb'
require '/usr/src/jarvixServer/db/model/sys_note.rb'
require '/usr/src/jarvixServer/db/model/sys_notes_log.rb'
require '/usr/src/jarvixServer/db/model/sys_event.rb'
require '/usr/src/jarvixServer/db/model/sys_log.rb'
require '/usr/src/jarvixServer/db/model/sys_ignored_word.rb'
require '/usr/src/jarvixServer/db/model/api_word.rb'
require '/usr/src/jarvixServer/db/model/api_metric_word.rb'

def save_note(api_id, u_id, message)
		sys_note=Sys_Note.new
		sys_note.api_interface=api_id
		sys_note.sys_user=u_id
		sys_note.phrase = message
		sys_note.save
		#save words and metrics
		save_word(u_id, message)
		#save logs (1 = create)
		log_id = save_log(u_id,1,"",message)
		#relations between notes and logs
		save_notes_log(sys_note.id, log_id)
end

def get_user_id(user_phone)
	return Sys_User_Phone.find_by_number(user_phone).sys_user
end

def get_token(api_id, user_id)
	puts "-----get_token------"
	result1=Api_Token_User_Interface.where("api_interface='" + api_id.to_s() + "' and sys_user='"+user_id.to_s()+"'")
	#puts result1.to_json
	#puts result1[0].token
	return result1[0].token
end

def get_token_secret(api_id, user_id)
	return Api_Token_User_Interface.where("api_interface='" + api_id.to_s() + "' and sys_user='"+user_id.to_s()+"'")[0].token_secret
end

private 

def save_word(u_id, words)
	words.split(" ").each do |w|  #recorro las palabras
		if w != nil  #si w tiene valor proceso
			# busco en palabras ignoradas 
			ignoredWord = Sys_Ignored_Word.find_by_word(w)
			if ignoredWord == nil # si no la encuentro proceso
				word = Api_Word.find_by_word(w)
				if word==nil
					api_word = Api_Word.new
					api_word.word = w
					api_word.save
					#save metrics
		  		  	save_metric_word(u_id,api_word.id)
				else
					#save metrics
				    save_metric_word(u_id,word.id)
				end					
			end	
		end
	end
end

def save_metric_word(u_id, w_id)
	api_metric_word = Api_Metric_Word.new
	api_metric_word.sys_user=u_id
	api_metric_word.api_word = w_id
	api_metric_word.save
end

def save_log(user_id, event_id, before_event, after_event)
	sys_log = Sys_Log.new
	sys_log.sys_user = user_id
	sys_log.sys_event = event_id
	sys_log.date_event = Date.today 
	sys_log.time_event = Time.now
	sys_log.before_event = before_event
	sys_log.after_event = after_event
	sys_log.save	

	return sys_log.id
end

def save_notes_log(note_id, log_id)
	sys_notes_log = Sys_Notes_Log.new
	sys_notes_log.sys_notes = note_id
	sys_notes_log.sys_log = log_id
	sys_notes_log.save		
end
