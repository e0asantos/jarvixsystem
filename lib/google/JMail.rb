#!/usr/bin/env ruby
# encoding: utf-8
#Encoding.default_external = Encoding::UTF_8
#Encoding.default_internal = Encoding::UTF_8
Encoding.default_external = "utf-8"
Encoding.default_internal  = "utf-8"
require 'gmail_xoauth'
require 'gmail'
require 'gmail-contacts'
require 'active_support'
require 'rest_client'
require '/usr/src/jarvixServer/db/model/api_token_user_interface.rb'
require '/usr/src/jarvixServer/jarvixSystem/JarvixPerson.rb'
require '/usr/src/jarvixServer/jarvixSystem/Jarvix_Abstract_Object.rb'
require '/usr/src/jarvixServer/jarvixSystem/Jarvix_Custom_Object.rb'
require '/usr/src/jarvixServer/jarvixSystem/Jarvix_Custom_Property.rb'
require '/usr/src/jarvixServer/db/model/sys_user_phone.rb'
require '/usr/src/jarvixServer/db/model/sys_user.rb'


class JMail

	attr_accessor:fromIdUser
	attr_accessor:toIdUser
	attr_accessor:JObject
	attr_accessor:currentToken
	attr_accessor:fromPhone
	#lista de donde se debe escoger cuando hay varias opciones
	attr_accessor:optionsList
	#indice seleccionado cuando hay varias opciones
	attr_accessor:waitingSelectedIndex
	attr_accessor:mailFrom
	attr_accessor:mailTo
    
    def _write(to,message)
        _send(to,message)    
    end
	
	def _send(to,message)
		#primero verificar que el usuario tenga mail
		vfrom=userHasMail(@fromIdUser,true)
		puts "vfrom PASSED:"+vfrom
		#necesitamos descartar si ya paso por aqui dependiendo de las selecciones
		vto=nil
		if waitingSelectedIndex==nil
			vto=userHasMail(to)	
		else
			vto="OK"
		end
		
		puts "vto PASSED:"+vto
		if vfrom!="OK"
			return vfrom
		elsif vto!="OK"
			return vto
		end
		puts "PASSED"
		#las direcciones se verificaron ahora crear objeto de mail..
		lineAccessUser=JarvixToLineAccessUser(@fromIdUser)
		#obtener el token del email
		#buscar token
		@currentToken=Api_Token_User_Interface.where(:api_interface=>6,:sys_user=>lineAccessUser.id).last
		@currentToken.forceUpdateToken()
		@JObject = Gmail.new(lineAccessUser.email, @currentToken.token)
		emailTo=nil
		if @waitingSelectedIndex==nil
			emailTo=getUserEmail(to)	
		else
			emailTo=@optionsList[@waitingSelectedIndex]
			@optionsList=nil
			@waitingSelectedIndex=nil
		end
		puts "eMail is from:"+lineAccessUser.email+" and goes to:"+emailTo
		@JObject.deliver do
		  to emailTo
		  subject "LineAccess"
		  text_part do
		    body message
		  end
		  # html_part do
		  #   content_type 'text/html; charset=UTF-8'
		  #   body "<p>Text of <em>html</em> message.</p>"
		  # end
		  ##add_file "/path/to/some_image.jpg"
		end
		return "OK"
	end
	def _address(to,message)
		#primero verificar que el usuario tenga mail
		vfrom=userHasMail(@fromIdUser,true)
		puts "vfrom PASSED:"+vfrom
		vto="OK"#userHasMail(to)
		puts "vto PASSED?:"+vto
		if vfrom!="OK"
			return vfrom
		elsif vto!="OK"
			return vto
		end
		puts "PASSED"
		#las direcciones se verificaron ahora crear objeto de mail
		lineAccessUser=JarvixToLineAccessUser(@fromIdUser)
		#obtener el token del email
		#buscar token
		@currentToken=Api_Token_User_Interface.where(:api_interface=>6,:sys_user=>lineAccessUser.id).last
		@currentToken.forceUpdateToken()
		@JObject = Gmail.new(lineAccessUser.email, @currentToken.token)
		emailTo=to
		puts "eMail is from:"+lineAccessUser.email+" and goes to:"+emailTo
		@JObject.deliver do
		  to emailTo
		  subject "LineAccess"
		  text_part do
		    body message
		  end
		  # html_part do
		  #   content_type 'text/html; charset=UTF-8'
		  #   body "<p>Text of <em>html</em> message.</p>"
		  # end
		  ##add_file "/path/to/some_image.jpg"
		end
		return "OK"
	end
	def createBody
		
	end
	def userHasMail(userID,verifyGmail=false)
		realEmail=getUserEmail(userID)
		if realEmail.index("E:")
			possibleMails=gmailInternalContacts(realEmail.split(":")[1])
			if possibleMails.length>0
				@optionsList=possibleMails
				return "-option"
			else
				return "the user "+realEmail.split(":")[1]+" doesnt have a mail to send"
			end
		end
		#verificar email si es gmail
		if (realEmail.index("gmail.com")!=nil and verifyGmail==true) or verifyGmail==false
			#si es gmail, ahora verificar la validez del mail
			if validate_email_domain(realEmail)==true
				return "OK"
			else
				#al parecer hasta este punto dentro de los datos de jarvix no hay informacion suficiente para creer
				#que podemos enviar un mail a quien se nos pidio enviarlo
				#por esa razon realizamos una busqueda en gmail
				
				return "the user "+person.ontosData.my_name+" doesnt have a valid mail to send"
			end
		else
			return "only gmail accounts can send mails"
		end
	end
	def getUserEmail(userID)
		#buscar ususario en jarvix y buscar su telefono
		#para poder buscarlo en lineaccess
		person=JarvixPerson.new(userID)
		emailWord=Jarvix_Abstract_Object.find_by_object_name("email")
		#buscar si lo tiene asignado
		personEmail=person.getObjectProperty("email")#Jarvix_Custom_Object.where(:id_abstract_object=>emailWord.id,:id_owner=>person.ontosData.id)
		if personEmail!=nil
			puts "personEmail:"+personEmail.inspect
			return personEmail
		else
			return "E:"+person.ontosData.my_name
		end
		
		# if personEmail.length<1
		# 	return "E:"+person.ontosData.my_name
		# end
		# #parece que tiene, ahora determinar si el email es correcto
		# realEmail=Jarvix_Custom_Property.where(:property_name=>"extra",:id_user=>person.ontosData.id,:id_custom_object=>personEmail[0].id).last
		# if realEmail.nil?
		# 	return "E:"+person.ontosData.my_name
		# end
		# return realEmail.property_value
	end
	def validate_email_domain(email)
      domain = email.match(/\@(.+)/)
      if domain!=nil
      	domain=domain[1]
      else
      	return false
      end
      Resolv::DNS.open do |dns|
          @mx = dns.getresources(domain, Resolv::DNS::Resource::IN::MX)
      end
      @mx.size > 0 ? true : false
	end
	def JarvixToLineAccessUser(userID)
		person=JarvixPerson.new(userID)
		#buscar telefono
		personPhone=Jarvix_Custom_Object.where(:id_abstract_object=>1,:id_owner=>person.ontosData.id).last
		realPhone=Jarvix_Custom_Property.where(:property_name=>"number",:id_user=>person.ontosData.id,:id_custom_object=>personPhone).last
		#con el telefono podemos buscar el usuario
		lineAccessPhone=Sys_User_Phone.where(:number=>realPhone.property_value,:sys_phone_type=>1).last
		lineAccessUser=Sys_User.find_by_id(lineAccessPhone.sys_user)

	end

	def gmailInternalContacts(busquedaOriginal)
		lineAccessUser=JarvixToLineAccessUser(@fromIdUser)
		#obtener el token del email
		#buscar token
		@currentToken=Api_Token_User_Interface.where(:api_interface=>6,:sys_user=>lineAccessUser.id).last
		if @currentToken==nil
			return
		end
		@currentToken.forceUpdateToken()
		gmail = Gmail.new(lineAccessUser.email, @currentToken.token)
		fecha = Date.today
		fecha=(fecha >> -3)
		externalCounter=0
		nombres=[]
		mails=[]
		gmail.inbox.emails(:all,:from=>busquedaOriginal,:after => fecha).each do |email|
		uno=email.header.raw_source.split("\r\n")
			uno.each do |suno|
				if suno.downcase.index("from") or suno.downcase.index("to") or suno.downcase.index("cc")
					if suno.encode('utf-8').downcase.index(busquedaOriginal)
						contacto=suno.encode('utf-8')
						nombre=contacto.split('"')
						if nombre[1]==nil
							#probablemente no tenga comillas
							#entonces idenfificarlo por <
							nombre=contacto.split("<")
							nombre=nombre[0][5..-2]
						else
							nombre=nombre[1]
						end
						 puts nombre
						umail=contacto.split("<")
						#en caso de que no tenga correo, nos saltamos este
						if umail.length==1
							next	
						end
						
						 puts umail[1][0..-2]
						if mails.index(umail[1][0..-2])==nil
							nombres.push(nombre)
							mails.push(umail[1][0..-2])	
						end
						if externalCounter>=5
							break
						end
					end
				end
			end
			if externalCounter>=5
				break
				#nombres.push(suno.encode('utf-8'))
			end
			externalCounter=externalCounter+1
		end
		puts mails.inspect
		puts nombres.inspect
		return mails
	end

end