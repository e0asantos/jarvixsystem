# encoding: utf-8

require 'adhearsion-asterisk'
require 'savon'
require 'rubygems'
require 'net/http'              #for http connections
require 'yaml'
require 'net/http'
require 'uri'
require 'logger'
require 'json'
require 'fb_graph'
require 'bing_translator'
require 'uuidtools'
require 'xmpp4r/roster'
require 'xmpp4r'

require '/usr/src/jarvixServer/lib/utils/IncomingCallAccessHelper.rb'
require '/usr/src/jarvixServer/jarvixSystem/Jarvix.rb'
require '/usr/src/jarvixServer/lib/utils/callRecord.rb'
require '/usr/src/jarvixServer/db/model/Jarvix_Conversation_Log.rb'
require '/usr/src/jarvixServer/db/model/sys_region_lang.rb'
require '/usr/src/jarvixServer/db/model/sys_message.rb'
require '/usr/src/jarvixServer/db/model/sys_start_command.rb'
require '/usr/src/jarvixServer/db/model/api_token_user_interface.rb'
require '/usr/src/jarvixServer/lib/twitter/JTwitter.rb'
require '/usr/src/jarvixServer/lib/google/GtalkHelper.rb'
require '/usr/src/jarvixServer/lib/utils/Babel.rb'
require '/usr/src/jarvixServer/lib/facebook/JFacebook.rb'
require '/usr/src/jarvixServer/db/model/sys_user_phone.rb'
class IncomingCall < Adhearsion::CallController

  attr_accessor :number, :attempt,:incomingMetric,:translator,:jarvixInstance,:sessionLang
  attr_accessor :incomingPhoneUser,:userAccount,:chatEngine,:texto,:textoc

  def run
    answer
    @sessionLang="es"
    call.on_end do |event|
      begin
        ChatHelper.isActiveChat["from_"+@userAccount.id.to_s]=nil
        ChatHelper.currentInstance["from_"+@userAccount.id.to_s]=nil
      rescue Exception => e
        
      end
      puts "++++++HANG:"+event.inspect+"++++++++++++++++++++++"
    end
    @texto=""
    @textoc=""
    @incomingMetric=IncomingCallAccessHelper.new(variables[:x_agi_callerid],variables[:x_agi_calleridname])
    @translator = BingTranslator.new('lineaccessmx', 'k+Q3xRzEgOImBpDLGf/h1Xus3Rbby6WcEid4Khbsiv4=')
    telefonoUsuario=getRealUserPhone(variables[:x_agi_callerid],variables[:x_agi_calleridname])
    puts "+++++++++++++++++++++"+telefonoUsuario+"+++++++++++++++++++++++++++"
    @incomingPhoneUser=Sys_User_Phone.find_by_number(telefonoUsuario)
    puts "----"+@incomingPhoneUser.inspect+"-----"
    if @incomingPhoneUser==nil 
      #si el usuario no existe.
      execute 'agi', 'googletts.agi, "'+Sys_Message.where("id_message=1 and lang='#{@sessionLang}'")[0].message+'",'+@sessionLang
      @incomingMetric.endCall("NOTR")
      hangup
    else
      @userAccount=Sys_User.find_by_id(@incomingPhoneUser.sys_user)
      if @userAccount.lang!=nil
        @sessionLang=@userAccount.lang  
      end

      mobileNumber=Sys_User_Phone.where("sys_user='"+@userAccount.id.to_s+"' and sys_phone_type='1'")[0].number
      @jarvixInstance=Jarvix.new(mobileNumber.to_i,@sessionLang)
      if @userAccount.is_technical==0
        execute 'agi', 'googletts.agi, "'+@userAccount.name+'. '+@jarvixInstance.openPort+'. '+@jarvixInstance.openTransmission+'",'+@sessionLang
      else 
        @jarvixInstance.openPort
        @jarvixInstance.openTransmission
      end

      @llamada=CallRecord.new(@sessionLang,telefonoUsuario,@jarvixInstance.currentConversationID,self)
    end
    #esta variable controla el numero de veces que intenta escuchar
    tryouts=0
    semaphoreNumber=1
    @chatEngine=ChatHelper.new
    while semaphoreNumber==1
      #primero vemos si tenemos mensajes
      incomingMessage=@llamada.text
      changesInExecution=0
      if ChatHelper.isActiveChat["from_"+@userAccount.id.to_s]!=nil
        if @texto==nil
          @texto=""
        end
        @textoc="_"+@texto+"_"
        myt=Thread.new do
          ChatHelper.isActiveChat["from_"+@userAccount.id.to_s].add_message_callback do |m|
             if m.body!=@texto
                @texto=m.body
                # execute 'agi', 'googletts.agi, "'+@userAccount.name+' mensaje nuevo de feisbuc '+m.body+'",'+@sessionLang
              end
            # if m.body==nil
            #   puts "is null"
            #   execute 'agi', 'googletts.agi, "'+usuarioAccount.name+' el contacto esta escribiendo'+'",'+@sessionLang
            # else
            #   puts "not null"
            #   execute 'agi', 'googletts.agi, "'+usuarioAccount.name+' mensaje nuevo de feisbuc '+m.body+'",'+@sessionLang
            # end
          end
        end
          myt.join
        myt.kill
        if @texto!=nil
          if @textoc!="_"+@texto+"_"
            if @texto!=nil
              puts "external --->"+@texto
              execute 'agi', 'googletts.agi, "'+@userAccount.name+' mensaje nuevo de feisbuc '+@texto+'",'+@sessionLang
              @llamada=CallRecord.new(@sessionLang,telefonoUsuario,@jarvixInstance.currentConversationID,self)
              ChatHelper.currentInstance["from_"+@userAccount.id.to_s].sendMessage(@llamada.text)
            end
          end
        end
        
      elsif incomingMessage=="NONE"
        tryouts=tryouts+1
        if tryouts==2
          semaphoreNumber=2
        end
        execute 'agi', 'googletts.agi, "'+@userAccount.name+'. ¿Puedes decirlo nuevamente?",'+@sessionLang
        @llamada=CallRecord.new(@sessionLang,telefonoUsuario,@jarvixInstance.currentConversationID,self)
      else
        #en caso de que si haya hablado algo esta parte sera la parte principal
        #----------------mainStart
        @llamada.db_message.interface_executed=4
        @llamada.db_message.save
        if @sessionLang!="en"
          incomingMessage=@llamada.db_message.phrase
        else
          incomingMessage=@llamada.db_message.translate
        end
        startCommands=Sys_Start_Command.find(:all)
        posicion=nil
        startCommands.each do |oneCommand|
            if incomingMessage.downcase.index(oneCommand.startCommand) !=nil
              if incomingMessage.length>(oneCommand.startCommand.length+3)
                
                # posicion=mensajeNuevo.downcase.index('i want to ')
                posicion=incomingMessage.downcase.index(oneCommand.startCommand)+oneCommand.startCommand.length+1  
                puts "----command was:"+oneCommand.startCommand+" with posicion:"+posicion.inspect
              end
            end
          end
          recortar=''
          if posicion!=nil
            #si encontramos que si tenemos comandos adentro
            recortar=incomingMessage[posicion..-1].downcase
            allOrals=Api_Method.find(:all)
            #recorrer todos los comandos orales
            allOrals.each do |oneOral|
              oralCommand=""
              if recortar.index(oneOral.oral)!=nil
                oralCommand=oneOral.oral
              elsif recortar.index(oneOral.oralSpanish)!=nil
                oralCommand=oneOral.oralSpanish
              end
              if (oralCommand!="" and Api_Token_User_Interface.where(:sys_user=>@incomingPhoneUser.sys_user,:api_interface=>oneOral.api_interface).length>0) or (oralCommand!="" and Api_Interface.find_by_id(oneOral.api_interface).usersNeedToken==0)#10 es el traductor

                  interfaceTxt=Api_Interface.find_by_id(oneOral.api_interface)
                  if recortar[oralCommand.length..-1].length>3
                  #si es mayor que 3 seguramente si tenemos un mensaje
                    if oneOral.ischat==0
                      puts "------GUARDANDO------"
                      changesInExecution=1
                      apiClient = eval(interfaceTxt.command+'()')
                      apiClient.fromPhone=telefonoUsuario
                      puts apiClient.instance_variables.inspect
                      if apiClient.instance_variables.index(:@call_controller)!=nil
                        puts "------has controller------"
                         apiClient.call_controller=self
                      end
                      nuevoRecorte=recortar[oralCommand.length..-1]
                      singleConversationLog=Jarvix_Conversation_Log.new
                      singleConversationLog.chat_id=@jarvixInstance.currentConversationID
                      singleConversationLog.user_id=@jarvixInstance.currentSubject.idPersonality
                      
                       if apiClient.instance_variables.index(:@conversation_log)!=nil
                        puts "------has conversation_log------"
                         apiClient.conversation_log=singleConversationLog
                      else
                        #singleConversationLog.save
                      end
                      apiClientResult=apiClient.send("_"+oneOral.name,nuevoRecorte)
                      singleConversationLog.response=apiClientResult
                      singleConversationLog.save
                      @llamada.db_message.interface_executed=oneOral.api_interface
                      @llamada.db_message.save
                      break
                    end
                  end
              end
            end
          end
          if changesInExecution==0
            @llamada.db_message.interface_executed=4
            @llamada.db_message.save
            messageFromJarvix=@jarvixInstance.payAttention(@llamada.db_message.translate)
            if messageFromJarvix!=nil
              if messageFromJarvix.index(">")==nil
                if sessionLang!="en"
                  messageTranslatedFromJarvix=@translator.translate messageFromJarvix, :from => 'en', :to => @sessionLang
                  messageTranslatedFromJarvix=messageTranslatedFromJarvix.gsub(","," ")
                  
                  execute 'agi', 'googletts.agi, "'+messageTranslatedFromJarvix+'",'+@sessionLang
                else
                  execute 'agi', 'googletts.agi, "'+messageFromJarvix+'",'+@sessionLang
                end
                # if messageFromJarvix.downcase=="ok"
                #   execute 'agi', 'googletts.agi, "'+@userAccount.name+'. ¿Algo más en que te pueda ayudar?",'+@sessionLang
                # end.
                # @llamada=CallRecord.new(@sessionLang,telefonoUsuario,@jarvixInstance.currentConversationID,self)
                # if @llamada.continue_conversation==true
                #   semaphoreNumber=1.
                # end
              else
                
                execute 'agi', 'googletts.agi, "'+@userAccount.name+'.'+Sys_Message.where("id_message=4 and lang='#{sessionLang}'")[0].message+'",'+@sessionLang
              end
            else
              execute 'agi', 'googletts.agi, "'+@userAccount.name+'.'+Sys_Message.where("id_message=4 and lang='#{sessionLang}'")[0].message+'",'+@sessionLang
            end
          end
          if @jarvixInstance.accessLine.waitingParam==nil and ChatHelper.isActiveChat["from_"+@userAccount.id.to_s]==nil
            execute 'agi', 'googletts.agi, "'+@userAccount.name+'. ¿Algo más en que te pueda ayudar?",'+@sessionLang  
          end
         if ChatHelper.isActiveChat["from_"+@userAccount.id.to_s]==nil
         @llamada=CallRecord.new(@sessionLang,telefonoUsuario,@jarvixInstance.currentConversationID,self)
         if @llamada.continue_conversation!=true
           semaphoreNumber=2
         end
       end
        #----------------mainEnd
      end
     
    end
    
  end

  def random_number
    rand(10).to_s
  end

  def update_number
    @number << random_number
  end

  def collect_attempt
    # result = ask @number, :limit => @number.length.
    execute 'agi', 'googletts.agi, "usando incoming",es'
    # @attempt = result.response
    # record_result = record start_beep: true, max_duration: 10,, final_timeout: 3
    uid=UUIDTools::UUID.random_create.to_s()
    # execute 'agi', 'speech-recog.agi','es'
   
    # execute 'verbose "HOLA QUE ACE"'
    # logger.info "Recording saved to #{record_result.recording_uri}"
  end

  def verify_attempt
    if attempt_correct?
      # speak 'good'
      # execute 'festival', 'Hello, this is Asterisk text to speech'
    else
      # speak "#{@number.length - 1} times wrong, try again smarty"
      # execute 'festival', 'Hello, this is Asterisk text to speech'
      reset
    end
  end

  def attempt_correct?
    @attempt == @number
  end

  def reset
    @attempt, @number = '', ''
  end
  def getRealUserPhone(callerid,calleridname)
    cutPhone=callerid[1..10]
    if callerid.length==12
      @sessionLang=Sys_Region_Lang.find_by_country_code(callerid[0..1]).lang
      cutPhone=callerid[2..11]
    end
    telefonoSkype=calleridname
    if callerid=="Anonymous"
      cutPhone=telefonoSkype
    end
    return cutPhone
  end
end
