require 'yaml'
require 'rubygems'
require 'active_record'

db_config = YAML::load( File.open("/usr/src/jarvixServer/db/database.yml"))
ActiveRecord::Base.establish_connection( db_config["lineAccess"])
#deactivate logger on DEBUG
